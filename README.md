[![MythXBadge](https://badgen.net/https/api.mythx.io/v1/projects/277bdf8e-4ee4-47aa-9506-d8c9a26a3d77/badge/data?cache=300&icon=https://raw.githubusercontent.com/ConsenSys/mythx-github-badge/main/logo_white.svg)](https://docs.mythx.io/dashboard/github-badges)
# Stake Protocol Contracts

Contracts and tests for stake protocol