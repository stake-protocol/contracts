from brownie import UpgradeToken, accounts, StakeToken, StakeVault


def main():
    main_deployer = accounts.load("stk_dep")
    token = StakeToken[-1]
    vault = StakeVault[-1]

    upg = UpgradeToken.deploy(token, vault, {"from": main_deployer})
    token.excludeAddress(upg, True, True, {"from": main_deployer})
