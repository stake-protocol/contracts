from brownie import (
    NerdFaucetV4,
    FundDistributor,
    TestToken,
    StakeTokenV2,
    POLv2,
    TokenLocker,
    UpgradeToken,
    FoundationV6,
    chain,
    accounts,
)
from web3 import Web3 as web3
import json


def main():
    dev = accounts.load("stk_dep")
    leaderDrops = "0x9CbF68b342B2fB993cEe3914bf80693DE2633552"
    if chain.id == 56:
        flm = TestToken.at("0x20D5BA6D5aa2A3dF3A632B493621D760E4c7965E")
        busd = TestToken.at("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
    else:
        flm = TestToken[-2]
        busd = TestToken[-1]

    stake = StakeTokenV2.deploy({"from": dev}, publish_source=True)
    fd = FundDistributor.at(
        "0xdECFeb7BE3C0d41d9927C8cF8c92f983b53b82d3"
    )  # .deploy(busd, {"from": dev}, publish_source=True)
    pol = POLv2.deploy(stake, busd, {"from": dev}, publish_source=True)
    faucet = NerdFaucetV4.deploy(
        stake, flm, leaderDrops, busd, fd, pol, {"from": dev}, publish_source=True
    )

    pol_lock = TokenLocker.deploy(pol, {"from": dev}, publish_source=True)

    fdt = FoundationV6.deploy(
        busd, stake, pol, pol_lock, {"from": dev}, publish_source=True
    )
    pol.setFoundation(fdt, {"from": dev})
    pol.setFundDistributor(fd, {"from": dev})
    pol.setWhitelistStatus(fdt, True, {"from": dev})
    stake.setFaucet(faucet, {"from": dev})
    # fd.setTreasury("0x651D8192a45A588E46aDF456A6e0F514E9D6f78a", {"from": dev})
    # fd.setPriceFloorProtection(
    #     "0x87ae4BC578Fb20879176377EC6ee4D7bF01D0329", {"from": dev}
    # )
    # # Semi Wallet
    # fd.setTeammate(SEMI_WALLET, 6, {"from": dev})
    # # BC WALLET
    # fd.setTeammate(BC_WALLET, 6, {"from": dev})
    # # SIFU WALLET TODO Swap Accounts[9] for a teammate wallet BPRO
    # fd.setTeammate(SIFUWALLET, 6, {"from": dev})
    # # Teammate wallet
    # fd.setTeammate(INVESTOR WALLET, 9, {"from": dev})

    pol.setLiquidityLock(pol_lock, {"from": dev})
    busd.approve(pol, web3.toWei(16_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(1_000_000, "ether"), {"from": dev})
    # Launch price is 0.15 BUSD per 1 STAKE
    pol.addLiquidity(
        web3.toWei(10_000, "ether"),  # Liquidity Amount
        web3.toWei(104078.51058196399, "ether"),  # Token
        web3.toWei(14194.105816902232000358, "ether"),  # Base
        {"from": dev},
    )
    # Foundation -> add permanent shares for people still participating in it.
    fd1_data = open("fd1_shares.json")
    fd1_data = json.load(fd1_data)

    for data in fd1_data["data"]:
        if data["shares"] == 0:
            continue
        fdt.setPermShares(data["user"], data["shares"], {"from": dev})

    # migrate from prev faucet to new faucet
    fc_data = open("all_v2_data.json")
    fc_data = json.load(fc_data)

    for i in range(0, len(fc_data["data"]), 20):
        transformed = {
            "users": list(),
            "accounting": list(),
            "team": list(),
            "leaders": list(),
            "claims": list(),
        }
        fragment = fc_data["data"][i : i + 20]
        for fg_data in fragment:
            transformed["users"].append(fg_data["user"])
            transformed["accounting"].append(fg_data["accounting"])
            transformed["team"].append(fg_data["team"])
            transformed["leaders"].append(fg_data["leaders"])
            transformed["claims"].append(fg_data["claims"])
        faucet.migrateMany(
            transformed["users"],
            transformed["accounting"],
            transformed["team"],
            transformed["leaders"],
            transformed["claims"],
            {"from": dev},
        )

    # airdrop tokens to users not gaming the system
    migrationContract = UpgradeToken.at("0xE920Ac343A112Cf8b81eA1dBF9d987d3D6eED217")
    stake.changeExcludeStatus(migrationContract, True, {"from": dev})
    migrationContract.setV2(stake, {"from": dev})
    total_stake = migrationContract.totalReceived()
    stake.transfer(migrationContract, total_stake, {"from": dev})

    pv2_wallets = open("v2_wallet_holders.json")
    pv2_wallets = json.load(pv2_wallets)
    pv2 = StakeTokenV2.at("0xd96E80443bF313a02f744e906E5c13753d7379e4")

    for wallet in pv2_wallets["wallets"]:
        balance = pv2.balanceOf(wallet)
        stake.transfer(wallet, balance, {"from": dev})
