from brownie import NerdFaucetV4
import json


def main():
    fc = NerdFaucetV4.at("0x1d5c8575e0479cfa7336c0e463d307a8ce676217")
    with open("all_v2_wallets.json") as input_file:
        v1_wallets = json.load(input_file)
    block = v1_wallets["snapshot_block"]

    faucet_data = list()
    for wallet in v1_wallets["wallets"]:
        req = {
            "accounting": fc.accounting(wallet, block_identifier=block),
            "team": fc.team(wallet, block_identifier=block),
            "leaders": fc.leaders(wallet, block_identifier=block),
            "claims": fc.claims(wallet, block_identifier=block),
        }
        transformed = {
            "user": wallet,
            "accounting": req["accounting"],
            "team": req["team"],
            "leaders": req["leaders"],
            "claims": req["claims"],
        }
        faucet_data.append(transformed)
        print(wallet, len(faucet_data))
    with open("all_v2_data.json", "w") as outputfile:
        json.dump({"data": faucet_data}, outputfile)
