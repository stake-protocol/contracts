from brownie import LotteryV2
from web3 import Web3, HTTPProvider


def main():
    w3 = Web3(
        HTTPProvider(
            "https://delicate-sparkling-meadow.bsc.quiknode.pro/99703c4a00aab1e437646facbf9583030b7fbf00/"
        )
    )
    newFilter = w3.eth.filter(
        {
            "fromBlock": 20670189,
            "toBlock": 20679521,
            "address": "0xd264ccac783c7CF11f1Dc1Cb445C6Ce7B5513268",
            "topics": [
                "0x26632edb369f47eb451a281fad73ca0a52cfa3109e457a64524535839486ac6d"
            ],
        }
    )

    entries = newFilter.get_all_entries()
    price_ = []
    for entry in entries:
        price_.append(
            {
                "price": Web3.fromWei(int(entry["data"], 16), "ether"),
                "block": entry["blockNumber"],
            }
        )
    print(price_)
