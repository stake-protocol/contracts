from brownie import Foundation
import json


def main():

    foundation = Foundation.at("0x943586bD4A72D4016EFBa3009e9D4C22851528B5")
    users = [
        "0xc528b36b9c7db0df961de167b84333c6ceff2a86",
        "0xf0d229550d48656b2ed10ab65b1f694c34e50228",
        "0x6f27fa0220d7aa14ba17d157b3b92b55400ec1e8",
        "0xe02aa280989d7948ded179212b68065abd6ae027",
        "0xabb7b3d86bf0cec577fdcd069cefeb8b5eedcb73",
        "0xb813a3f0fcff46dabb251cba75c31fcf78f4bf08",
        "0xadd87a6ea36d86b91476eb81f484018b8210ae50",
        "0x0b5121bc3c77f9844883843d8cd406b20d642d83",
        "0xb9b9b4318375daa2ab1897b73cb0d4fb27886008",
        "0x9a292b7c0d44ccabc0f87b311c5d853435a6c22e",
        "0x99c05181f3f06336ac8fe72f649bd9672f623bc8",
        "0xb33d7eadc3dd06ee6970853b82a06f190140801e",
        "0x1ee456994a481cba18fc5e3dc6d4ef8c8261c518",
        "0x6343ed94a8c516306ea199f960ebb7ae81a99635",
        "0xbb81b09c99a2b29d2c52060dc8dcae544c9e3cd9",
        "0xe7cc57e00e79d1e072de2fec9899c25b2936b791",
        "0x2c85ab6090253041e5c2a99f029e9318396c8a23",
        "0xeb03fbf5eddb657e64197d33c32bb1acd193cb37",
        "0x4001b796d515e8495aeb0673944e7271ceb87cd5",
        "0xba32f64a77884b53816027dc7b3bb1d3badc7ddc",
        "0xe81bc5df694cd4277834573a94bbb039190d96fa",
        "0x2b2e893f52cfa9eaac1c1b4437136f660a0471a1",
        "0xb4ece77650d63db53e59817b11c2ee69de945ebb",
        "0x885b449e4e3b64ddcd40e4a6f4ae7ac7a42b7eb0",
        "0xc6ea1576261ce6dea77ef150f2d51a3b5f49c60a",
        "0x01c1ea90a435bba87c879e2a950b762dd2ca1a27",
        "0xbe62f101a5915bf106373d9620915696a30be921",
        "0x0ad11a34008aa1d0a515c6e7ebf0d34b8905f354",
        "0x6a89bd345f787707e4fd8775cdcca8f52a86701b",
        "0xad61b2cc2fff61edbee92b17dbfcc592e207f18a",
        "0x7fe135050063845006da8b5002087b1a3ff5b87b",
        "0xb7c7e2b4b138781abb8bf33322a58cc6fef061d3",
        "0x59a7a5d00a18ae577f2f0d780be0e701b75b045d",
        "0xd91ae2f1c236e8c140d04c9e1b4fec6dd29e7b98",
        "0x97dccc329d98e5a44e934e53dbf9862f06962ac3",
        "0x065963df68ab6f33aa9470f333b83661aa060f20",
        "0x08ca9a35f96e818194212cb1d6b0cdb78416bb14",
        "0x636076b3bd8c89caa4198be171eb7eb061e043a7",
        "0x646eb6a26d61d62edbe16f1015589b4cacf7227c",
        "0x41eb4199903f3152c3778fd0cdca64d7f07ebba6",
        "0xed9a912182745fe101ecb4291d00b802d982ba0f",
        "0xdb642a5e1a78ae717dd594e688f8f6ca9fb4527c",
        "0xe7968816fa1bac9fe758012ac927afd2ba47a711",
        "0xcada8b8658c87092794ed879c8eb6d8274ec7ed9",
        "0xe5174ca87d270bce1c402f8b000ed7bd239c33d4",
        "0x2eab55eb9029cea0dc7c21cdea1c9656a63b9798",
        "0x0dba7ba6ccc2172061308e281e4e819bc1fc5c1d",
        "0x8b327824ea2ccb98acabb1cf453b3034963af129",
        "0x11bb14ece7d3709330cac2a3f4a96729ad8b873e",
        "0x238643d4106c7c6206f8dc31acb59cd24c979d12",
        "0xc844e702f33f82bae0a5c70d6942ffd067c4d3cc",
        "0xb6a18c10c7681662da841cdeb43bee2ec267e92b",
        "0xf2787bfd9525c5db0d8715785e85357838254a8b",
        "0xc5f8ffcc17200650cbcd7be6900407022038cf32",
        "0x67007016a6f1bb9ffd924b7914ea670d3f0d3889",
    ]

    data = list()

    for wallet in users:
        iv = foundation.investment(wallet)
        info = {
            "double": iv["lpDouble"],
            "single": iv["lpSingle"],
            "deposits": iv["busdDeposit"],
            "withdraws": iv["withdraws"],
            "wallet": wallet,
        }
        data.append(info)

    with open("ex_FD.json", "w") as out:
        json.dump({"info": data}, out)
