from brownie import (
    LotteryV2,
    accounts,
    StakeToken,
    interface,
    StakeLP2,
    chain,
    TestToken,
    TokenLocker,
    LotteryDataViewer,
)
from web3 import Web3 as web3


def main():
    if chain.id != 97:
        return
    tbusd = TestToken[-2]
    tstake = TestToken[-1]
    test_w = accounts.load("dev_deploy")

    tbusd.transfer(
        "0x51Ec0630E4145B174bF52a572ec9eC7d9509F672",
        web3.toWei(1000, "ether"),
        {"from": test_w},
    )
    tstake.transfer(
        "0x51Ec0630E4145B174bF52a572ec9eC7d9509F672",
        web3.toWei(1000, "ether"),
        {"from": test_w},
    )
    # test_w.transfer("0x51Ec0630E4145B174bF52a572ec9eC7d9509F672", "0.08 ether")
