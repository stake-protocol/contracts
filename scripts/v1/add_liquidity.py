from brownie import accounts, chain, StakeFountain, StakeToken, StakeVault, TokenLocker
from web3 import Web3


def main():
    owner = accounts.load("cmos1")
    token = StakeToken.at("0x9945FfBeA6cEDAc21B877ab03A378f17F0D4E910")
    fountain = StakeFountain.at(
        "0x14fF07585E778158139382C35cAF8D2293B19248"  # used dev's testnet address
    )
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": owner})
    fountain.addLiquidity(
        Web3.toWei(1, "ether"),
        Web3.toWei(350, "ether"),
        {"from": owner, "value": Web3.toWei(1, "ether")},
    )
