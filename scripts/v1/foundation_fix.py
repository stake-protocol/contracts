from brownie import (
    accounts,
    StakeLP2,
    StakeToken,
    StakeVault,
    TokenLocker,
    Foundation,
    NerdFaucetV2,
    interface,
)
from web3 import Web3
import json


def main():
    owner = accounts.load("stk_dep")

    busd = interface.IToken("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
    token = StakeToken.at("0xe55bd75d7cE7bfDe26A347A748d080D3ACdA7FFE")
    vault = StakeVault.at("0xcC75572a98B0562109eB87dBBFc5AE34E6bF6553")
    faucet = NerdFaucetV2.at("0x429d2591387d730379d2E2cdf3cD1BC4BD438CF4")
    paybackWallet = "0x9aDf9Eb89Ea989D291D1fE4460427979ed900A20"
    # Deploy LP
    lp = StakeLP2.at("0xd264ccac783c7CF11f1Dc1Cb445C6Ce7B5513268")
    lock = TokenLocker.at("0x422ebACD382473173C54654A19Efa88D6531A3d7")
    # Deploy Foundation
    foundation = Foundation.deploy(
        vault, token, busd, paybackWallet, faucet, {"from": owner}
    )
    foundation.setPolData(lp, lock, {"from": owner})
    # lp.setFoundation(foundation, {"from": owner})

    # Add liquidity
    vault.removeWhitelist("0x943586bD4A72D4016EFBa3009e9D4C22851528B5", {"from": owner})
    lp.removeAddressFromWhitelist(
        "0x943586bD4A72D4016EFBa3009e9D4C22851528B5", {"from": owner}
    )
    token.excludeAddress(
        "0x943586bD4A72D4016EFBa3009e9D4C22851528B5", False, False, {"from": owner}
    )
    lp.addAddressToWhitelist(foundation, {"from": owner})
    vault.addWhitelist(foundation, {"from": owner})
    token.excludeAddress(foundation, True, True, {"from": owner})

    #  Pay back people
    file = open("ex_FD.json")

    data = json.load(file)
    users = data["info"]

    lp.approve(foundation, Web3.toWei(1_000_000, "ether"), {"from": owner})

    for user in users:
        if user["single"] + user["double"] > 0:
            foundation.setUser(
                user["wallet"],
                user["single"],
                user["double"],
                user["deposits"],
                user["withdraws"],
                {"from": owner},
            )
    for user in users:
        if user["single"] + user["double"] > 0:
            foundation.setUserDebt(user["wallet"], {"from": owner})
