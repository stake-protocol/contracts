from brownie import (
    accounts,
    chain,
    StakeFountain,
    StakeToken,
    StakeVault,
    TokenLocker,
)


def main():
    # owner = accounts.load("cmos1")
    # treasury = (
    #     "0xd1F88aF8c340CD31e013B5121b7300e992C7200A"  # used dev's testnet address
    # )
    # foundation = (
    #     "0xd1F88aF8c340CD31e013B5121b7300e992C7200A"  # used dev's testnet address
    # )

    # token = StakeToken.deploy(treasury, {"from": owner})

    # vault = StakeVault.deploy(token.address, {"from": owner})

    # fountain = StakeFountain.deploy(
    #     token.address, treasury, vault.address, foundation, {"from": owner}
    # )

    # token.updateVault(vault.address, True, {"from": owner})

    # lock = TokenLocker.deploy(fountain.address, {"from": owner})
    # fountain.setLock(lock.address, {"from": owner})

    # StakeToken deployed at: 0x9945FfBeA6cEDAc21B877ab03A378f17F0D4E910
    # StakeVault deployed at: 0x2cC5501190Ea534F1262E37e452Ed0C386Db5EC7
    # StakeFountain deployed at: 0x14fF07585E778158139382C35cAF8D2293B19248
    # TokenLocker deployed at: 0x8a38379b3C82148cE7f0ef3272859A43d56aFA55

    # StakeToken.updateVault confirmed   Block: 19656599   Gas used: 50562 (90.91%)
    # StakeFountain.setLock confirmed   Block: 19656607   Gas used: 43554 (90.91%)
    return
