from brownie import (
    NerdFaucetV2,
    StakeToken,
    FlameToken,
    TokenLocker,
    accounts,
    StakeVault,
    chain,
    interface,
)
from web3 import Web3


def main():
    owner = accounts.load("dev_deploy")
    rec = accounts.load("receive")

    flame = FlameToken.deploy(owner, {"from": owner})
    lock = TokenLocker.deploy(flame, {"from": owner})
    flame.updateLocker(lock, {"from": owner})

    # Create PAIR
    router = interface.IPancakeSwapRouter("0xD99D1c33F9fC3444f8101754aBC46c52416550D1")
    flame.approve(router, Web3.toWei(1000, "ether"), {"from": owner})
    factory = interface.IPancakeSwapFactory(router.factory())
    router.addLiquidityETH(
        flame.address,
        Web3.toWei(1000, "ether"),
        Web3.toWei(1000, "ether"),
        Web3.toWei(0.01, "ether"),
        owner,
        chain.time() + 3600,
        {"from": owner, "value": Web3.toWei(0.01, "ether")},
    )
    lp = factory.getPair(flame, router.WETH())
    flame.setMainRouter(router, {"from": owner})
    flame.addLpPair(lp, {"from": owner})

    router.swapExactETHForTokens(
        0,
        [router.WETH(), flame.address],
        rec,
        chain.time() + 600,
        {"from": rec, "value": Web3.toWei(0.0001, "ether")},
    )

    flame.swapAndLiquify({"from": owner})
