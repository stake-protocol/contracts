from brownie import NerdFaucetV2, FlameToken, StakeVault, accounts, StakeToken
import json


def main():
    owner = accounts.load("stk_dep")

    token = StakeToken.at("0xe55bd75d7cE7bfDe26A347A748d080D3ACdA7FFE")
    vault = StakeVault.at("0xcC75572a98B0562109eB87dBBFc5AE34E6bF6553")
    flame = FlameToken.at("0x20D5BA6D5aa2A3dF3A632B493621D760E4c7965E")
    leaderDrop = "0x9CbF68b342B2fB993cEe3914bf80693DE2633552"
    prev = NerdFaucetV2.at("0x429d2591387d730379d2E2cdf3cD1BC4BD438CF4")
    lottery = "0x0B34CA2DE7d28877075D834524F2E35cD744450b"

    # token.excludeAddress(leaderDrop, False, True, {"from": owner})
    # faucet = NerdFaucetV2.deploy(
    #     token, vault, flame, leaderDrop, prev, {"from": owner}, publish_source=True
    # )
    faucet = NerdFaucetV2.at("0xB6169aE16CDf460228E51f57dd6d87F394475551")
    # faucet.migrate("0x26984bbB8db557538Bd02997aDE0Dc22cDC0b95D", {"from": owner})

    # with open("all_faucet_wallets.json", "r") as migration:
    #     allWallets = json.load(migration)
    #     wallets = allWallets["wallets"]
    #     migrated = 0
    #     current = list()
    #     for user in wallets:
    #         if len(current) == 50:
    #             print(f"Batch: ", migrated)
    #             print(current)
    #             faucet.migrateMany(current, {"from": owner})
    #             current = list()
    #         current.append(user)
    #         migrated = migrated + 1

    # faucet.setLottery(lottery, {"from": owner})
    vault.addWhitelist(faucet, {"from": owner})
    vault.removeWhitelist(prev, {"from": owner})
