from brownie import (
    NerdFaucetV2,
    Foundation,
    StakeFountain,
    TokenLocker,
    StakeToken,
    FlameToken,
    StakeVault,
    accounts,
)
from web3 import Web3


def main():
    owner = accounts.load("stk_dep")
    vault = StakeVault[-1]
    token = StakeToken[-1]
    faucet = NerdFaucetV2[-1]
    token.excludeAddress("0x000000000000000000000", False, True, {"from": owner})
    vault.withdraw(
        "0x000000000000000000000",
        Web3.toWei(43.9, "ether"),
        {"from": owner},
    )
    token.removeExclusions("0x000000000000000000000", True, True, {"from": owner})
