from brownie import NerdFaucetV2, StakeToken, FlameToken, accounts, StakeVault, chain


def main():
    owner = accounts.load("dev_deploy")
    # on mainnet STAKE has already been deployed... careful!!!
    token = (
        StakeToken.at("0xe55bd75d7cE7bfDe26A347A748d080D3ACdA7FFE")
        if chain.id == 56
        else StakeToken[-1]
    )
    leaderDrop = "0x34129699774B6fA7F9FD199161869E0EA4f6A782"
    govToken = FlameToken[-1]  # .deploy({"from": owner})
    vault = StakeVault[-1]  # .deploy(token, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    faucet = NerdFaucetV2.deploy(token, vault, govToken, leaderDrop, {"from": owner})
    token.excludeMultiple([vault, leaderDrop], False, False, {"from": owner})
    vault.addWhitelist(faucet, {"from": owner})
    token.setPool(faucet, {"from": owner})

    token.excludeAddress(owner, True, False, {"from": owner})
