from brownie import (
    accounts,
    StakeLP2,
    StakeToken,
    StakeVault,
    TokenLocker,
    Foundation,
    NerdFaucetV2,
    interface,
)
from web3 import Web3


def main():
    owner = accounts.load("stk_dep")

    busd = interface.IToken("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
    token = StakeToken[-1]
    vault = StakeVault[-1]
    faucet = NerdFaucetV2[-1]
    paybackWallet = "0x9aDf9Eb89Ea989D291D1fE4460427979ed900A20"
    # Deploy LP
    lp = StakeLP2[-1]
    lock = TokenLocker[-1]
    # Deploy Foundation
    foundation = Foundation.deploy(
        vault, token, busd, paybackWallet, faucet, {"from": owner}
    )
    foundation.setPolData(lp, lock, {"from": owner})
    lp.setFoundation(foundation, {"from": owner})

    # Add liquidity
    lp.addAddressToWhitelist(foundation, {"from": owner})
    vault.addWhitelist(foundation, {"from": owner})
    token.excludeAddress(foundation, True, True, {"from": owner})
