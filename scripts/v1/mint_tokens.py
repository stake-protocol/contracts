from brownie import accounts, chain, StakeFountain, StakeToken, StakeVault, TokenLocker
from web3 import Web3


def main():
    owner = accounts.load("cmos1")
    token = StakeToken.at("0x9945FfBeA6cEDAc21B877ab03A378f17F0D4E910")
    token.setPool(owner, {"from": owner})
    token.mint(Web3.toWei(100000, "ether"), {"from": owner})
