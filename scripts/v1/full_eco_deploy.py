from tokenize import Token
from brownie import (
    StakeToken,
    StakeVault,
    TestToken,
    StakeLP2,
    FlameToken,
    NerdFaucetV2,
    ClaimVault,
    accounts,
    chain,
    LotteryV2,
    LotteryDataViewer,
    TokenLocker,
)
from web3 import Web3 as web3


def main():
    dev = accounts.load("dev_deploy")

    stake = StakeToken.deploy(dev, {"from": dev}, publish_source=True)
    vault = StakeVault.deploy(stake, {"from": dev}, publish_source=True)
    stake.updateVault(vault, True, {"from": dev})

    flame = FlameToken.deploy(dev, {"from": dev}, publish_source=True)
    f_lock = TokenLocker.deploy(flame, {"from": dev}, publish_source=True)
    flame.updateLocker(f_lock, {"from": dev})

    busd = TestToken.deploy("TBUSD", "TestBUSD", {"from": dev}, publish_source=True)
    treasury = "0x6b230Af9527AF9d253Fd0B503a9D451239a9e2cE"
    pf = "0xc528B36B9c7DB0DF961de167B84333C6cefF2A86"

    faucet = NerdFaucetV2.deploy(
        stake, vault, flame, treasury, dev, {"from": dev}, publish_source=True
    )

    claimer = ClaimVault.deploy(
        [stake, busd, flame], {"from": dev}, publish_source=True
    )
    stake.excludeAddress(claimer, True, True, {"from": dev})
    flame.setTaxFreeAddress(claimer, True, True, {"from": dev})

    stake.transfer(claimer, web3.toWei(20_000, "ether"), {"from": dev})
    flame.transfer(claimer, web3.toWei(20_000, "ether"), {"from": dev})
    busd.transfer(claimer, web3.toWei(20_000, "ether"), {"from": dev})

    vault.addWhitelist(faucet, {"from": dev})
    lp = StakeLP2.deploy(
        stake, treasury, vault, pf, busd, {"from": dev}, publish_source=True
    )
    lock = TokenLocker.deploy(lp.address, {"from": dev}, publish_source=True)
    lp.setLock(lock, {"from": dev})
    stake.excludeMultiple([lp, vault, treasury], True, True, {"from": dev})
    busd.approve(lp, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(lp, web3.toWei(10_000, "ether"), {"from": dev})

    lp.addLiquidity(  # 1 to 1 ratio
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )

    vrf = "0x6A2AAd07396B36Fe02a22b33cf443582f682c82f"

    lottery = LotteryV2.deploy(
        stake, lp, busd, 1361, vrf, {"from": dev}, publish_source=True
    )
    viewer = LotteryDataViewer.deploy(lottery, {"from": dev}, publish_source=True)
    stake.excludeAddress(lottery, True, True, {"from": dev})
    flame.setTaxFreeAddress(lottery, True, True, {"from": dev})

    stake.approve(lottery, web3.toWei(1_000_000, "ether"), {"from": dev})
    lottery.addToPool(web3.toWei(100_000, "ether"), {"from": dev})
    lottery.firstStart({"from": dev})

    flame.approve(lottery, web3.toWei(1000, "ether"), {"from": dev})
    lottery.setBonusCoin(flame, web3.toWei(1000, "ether"), 2, 4, {"from": dev})
