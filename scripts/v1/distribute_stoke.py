from brownie import Foundation, interface, accounts
from web3 import Web3


def main():
    busd = interface.IToken("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
    stoke = interface.IToken("0xd264ccac783c7CF11f1Dc1Cb445C6Ce7B5513268")
    payback = accounts.load("payback")
    f1 = Foundation.at("0x3ef5B55f6d51DaDb8A2b8Ed45f867B87422f96b0")
    f2 = Foundation.at("0x8517575E21E4c7d310831b5d044434De5Ee89880")

    bal = stoke.balanceOf(payback)
    f1_bal = stoke.balanceOf(f1)
    totalStoke = f1_bal + stoke.balanceOf(f2)
    print(Web3.fromWei(bal, "ether"))
    to_f1 = int((bal * f1_bal) / totalStoke)

    to_f2 = bal - to_f1
    print(Web3.fromWei(to_f1, "ether"))
    print(Web3.fromWei(to_f2, "ether"))

    stoke.transfer(f1, to_f1, {"from": payback})
    stoke.transfer(f2, to_f2, {"from": payback})

    f1.distributePending({"from": payback})
    f2.distributePending({"from": payback})
