from brownie import DebtPool, accounts, interface
from web3 import Web3


def main():
    owner = accounts.load("payback")
    busd = interface.IToken("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
    pool = DebtPool[-1]  # .deploy(busd, {"from": owner}, publish_source=True)

    pool.addMultipleUsers(
        [
            "0x5df0afeaac28feffb22a08d736bec235db7c98fc",
            "0x6b702420836dd2b5bbc814740c9b1edbfb18b81c",
            "0x7fe135050063845006da8b5002087b1a3ff5b87b",
            "0x90920c4c16b889a6ac3880bb1a4a0426784b921b",
            "0xbe62f101a5915bf106373d9620915696a30be921",
        ],
        [
            Web3.toWei(503.601078597247, "ether"),
            Web3.toWei(101.132672813617, "ether"),
            Web3.toWei(2089.85904596058, "ether"),
            Web3.toWei(3700, "ether"),
            Web3.toWei(100, "ether"),
        ],
        {"from": owner},
    )

    currentBalance = busd.balanceOf(owner)
    busd.transfer(pool, currentBalance, {"from": owner})
