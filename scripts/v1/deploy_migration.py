import json
from brownie import NerdFaucetV2, StakeToken, FlameToken, StakeVault, accounts

# Although in this opportunity it was simple enough to collect the wallets of those who deposited
# if enough time passes, getting all the data might prove a bit more complex.
def main():
    owner = accounts.load("stk_dep")
    leaderDrop = "0x9CbF68b342B2fB993cEe3914bf80693DE2633552"
    lottery = "0x0B34CA2DE7d28877075D834524F2E35cD744450b"

    token = StakeToken.at("0xe55bd75d7cE7bfDe26A347A748d080D3ACdA7FFE")
    flame = FlameToken.at("0x20D5BA6D5aa2A3dF3A632B493621D760E4c7965E")
    vault = StakeVault.at("0xcC75572a98B0562109eB87dBBFc5AE34E6bF6553")

    pFaucet = NerdFaucetV2.at("0x90dd085c2B5a873ecC65E3A709dAc63E4CB917b6")

    faucet = NerdFaucetV2.deploy(
        token, vault, flame, leaderDrop, pFaucet, {"from": owner}
    )

    faucet.setLottery(lottery, {"from": owner})
    vault.addWhitelist(faucet, {"from": owner})

    NerdFaucetV2.publish_source(faucet)
    with open("migration_Wallets.json", "r") as migration_wallets:
        allWallets = json.load(migration_wallets)
        wallets = allWallets["wallets"]
        migrated = 0
        for user in wallets:
            faucet.migrate(user, {"from": owner})
            migrated += 1
            print(f"Migrated Wallets: ", migrated, " - ", user)
