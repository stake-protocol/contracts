import json
from brownie import NerdFaucetV2

# Although in this opportunity it was simple enough to collect the wallets of those who deposited
# if enough time passes, getting all the data might prove a bit more complex.
def main():
    faucet = NerdFaucetV2[-1]

    allWallets = json.load("snapshot_wallets.json")
    wallets = allWallets["wallets"]

    data = list()
    total = 0
    total_deposits = 0
    total_airdrops = 0
    total_compounds = 0
    for user in wallets:
        accData = faucet.accounting(user).dict()
        userData = {
            "wallet": user,
            "deposits": accData["deposits"],
            "compounds": accData["rolls"],
            "airdrops": accData["airdrops_rcv"],
            "netFaucet": accData["netFaucet"],
            "withTax": int(accData["netFaucet"] / 0.9),
        }
        total_deposits = total_deposits + accData["deposits"]
        total_compounds = total_compounds + accData["rolls"]
        total_airdrops = total_airdrops + accData["airdrops_rcv"]
        total = total + int(accData["netFaucet"] / 0.9)
        data.append(userData)
    jsonData = {
        "debts": data,
        "totalOwed": total,
        "deposits": total_deposits,
        "compounds": total_compounds,
        "airdrops": total_airdrops,
    }
    with open("snapshot.json", "w") as outpufile:
        json.dump(jsonData, outpufile)
