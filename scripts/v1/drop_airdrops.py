from brownie import NerdFaucetV2, accounts, StakeToken, StakeVault
from web3 import Web3
import json


def main():
    main_deployer = accounts.load("stk_dep")
    token = StakeToken[-1]
    vault = StakeVault[-1]

    f = open("snapshot.json")

    data = json.load(f)

    debts = data["debts"]
    # to prevent more headaches, will just make it excluded from tax on send
    # token.excludeAddress(vault, True, False, {"from": main_deployer})

    for debt in debts:
        vault.withdraw(
            debt["wallet"],
            debt["withTax"],
            {"from": main_deployer},
        )
        print(
            debt["wallet"],
        )

    # Place tax back in
    token.removeExclusions(vault, True, False, {"from": main_deployer})
