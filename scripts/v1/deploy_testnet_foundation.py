from brownie import (
    accounts,
    chain,
    StakeLP2,
    StakeToken,
    StakeVault,
    TokenLocker,
    Foundation,
    TestToken,
)
from web3 import Web3


def main():
    owner = accounts.load("dev_deploy")
    treasury = "0x651d8192a45a588e46adf456a6e0f514e9d6f78a"
    prefoundation = owner

    busd = TestToken.at("0x15c73D12242b3782B71E260844cEfE4F1aD01aB2")
    token = StakeToken.deploy(owner, {"from": owner})
    vault = StakeVault.deploy(token, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    # Add funds to vault
    token.transfer(vault, Web3.toWei(10000, "ether"), {"from": owner})
    # Deploy fountain LP
    fountain = StakeLP2.deploy(
        token.address, treasury, vault, prefoundation, busd, {"from": owner}
    )
    lock = TokenLocker.deploy(fountain.address, {"from": owner})
    fountain.setLock(lock.address, {"from": accounts[0]})
    # DEPLOY FOUNDATION
    foundation = Foundation.deploy(vault, token, busd, {"from": owner})
    foundation.setPolData(fountain, lock, {"from": owner})
    fountain.setFoundation(foundation, {"from": owner})

    busd.approve(foundation, Web3.toWei(1000, "ether"), {"from": owner})
    fountain.addAddressToWhitelist(foundation, {"from": owner})
    vault.addWhitelist(foundation, {"from": owner})
    token.excludeMultiple([fountain, foundation], True, True)
    busd.approve(fountain, Web3.toWei(100000, "ether"), {"from": owner})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": owner})
    # Actual values
    fountain.addLiquidity(
        Web3.toWei(12700, "ether"),  # Tokens to emit
        Web3.toWei(1072, "ether"),  # STAKE to send
        Web3.toWei(12700, "ether"),  # BUSD to pair with
        {"from": owner},
    )
