from brownie import (
    LotteryV2,
    accounts,
    StakeToken,
    interface,
    StakeLP2,
    chain,
    TestToken,
    TokenLocker,
    LotteryDataViewer,
    FlameToken,
    NerdFaucetV2,
)
from web3 import Web3 as web3


def main():
    if chain.id == 56:
        owner = accounts.load("stk_dep")
        vrf = "0xc587d9053cd1118f25F645F9E08BB98c9712A4EE"
        busd = interface.IToken("0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56")
        flame = FlameToken[-1]
        stake = StakeToken[-1]
        lp = StakeLP2[-1]
        faucet = NerdFaucetV2.at("0x429d2591387d730379d2E2cdf3cD1BC4BD438CF4")
        lottery = LotteryV2[-1]
        # .deploy(
        #     stake, lp, busd, 346, vrf, faucet, {"from": owner}, publish_source=True
        # )
        LotteryDataViewer.deploy(lottery, {"from": owner}, publish_source=True)
        stake.excludeAddress(lottery, True, True, {"from": owner})
        stake.approve(lottery, web3.toWei(75_000_000, "ether"), {"from": owner})
        lottery.addToPool(web3.toWei(27_835, "ether"), {"from": owner})

        lottery.editPartner(
            "0x5340B25449DCfF44eb6550E593d1EF466F1EaFd3", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x7fe135050063845006da8B5002087b1A3FF5b87b", 10, {"from": owner}
        )
        lottery.editPartner(
            "0xB4Ece77650d63DB53E59817b11C2EE69dE945eBB", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x26984bbB8db557538Bd02997aDE0Dc22cDC0b95D", 90, {"from": owner}
        )
        lottery.editPartner(
            "0x51Ec0630E4145B174bF52a572ec9eC7d9509F672", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x0B814322f746821266Dbe4a022E053DFb489F123", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x4B12435D43010972915cbf7dAafe0d1785160313", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x4c4338422ddcd2aad88b1a6d37dd14f469270a5a", 10, {"from": owner}
        )
        lottery.editPartner(
            "0x5b4184f5e307bec30c568fcca07377f8e6de88db", 10, {"from": owner}
        )
        flame.approve(lottery, web3.toWei(1000, "ether"), {"from": owner})
        lottery.setBonusCoin(flame, web3.toWei(1000, "ether"), 1, 5, {"from": owner})

    elif chain.id == 97:
        owner = accounts.load("dev_deploy")
        vrf = "0x6A2AAd07396B36Fe02a22b33cf443582f682c82f"
        busd = TestToken[-2]
        # .deploy(
        #     "TBUSD", "TestBUSD", {"from": owner}, publish_source=True
        # )
        stake = TestToken[-1]
        # .deploy(
        #     "TSTAKE", "TestSTAKE", {"from": owner}, publish_source=True
        # )
        lp = StakeLP2[-1]
        # lp = StakeLP2.deploy(
        #     stake, owner, owner, owner, busd, {"from": owner}, publish_source=True
        # )
        # locker = TokenLocker.deploy(lp, {"from": owner}, publish_source=True)
        # lp.setLock(locker, {"from": owner})

        # busd.approve(lp, web3.toWei(100000, "ether"), {"from": owner})
        # stake.approve(lp, web3.toWei(100000, "ether"), {"from": owner})
        # lp.addLiquidity(
        #     web3.toWei(10000, "ether"),  # Tokens to emit
        #     web3.toWei(20000, "ether"),  # STAKE to send 2 stake/1 busd
        #     web3.toWei(10000, "ether"),  # BUSD to pair with
        # )
        lottery = LotteryV2[-1]
        # LotteryV2.deploy(
        #     stake, lp, busd, 1361, vrf, {"from": owner}, publish_source=True
        # )
        LotteryDataViewer.deploy(lottery, {"from": owner}, publish_source=True)
        stake.approve(lottery, web3.toWei(100_000, "ether"), {"from": owner})
        lottery.addToPool(web3.toWei(100_000, "ether"), {"from": owner})
        # leaders = [
        #     "0xB4Ece77650d63DB53E59817b11C2EE69dE945eBB",
        #     "0x5340B25449DCfF44eb6550E593d1EF466F1EaFd3",
        #     "0xB813a3f0fcFF46DaBb251cbA75c31FCF78f4Bf08",
        #     "0x3533aC5EE83eb62bC7dA4B581786877b402A5416",
        #     "0x4C4338422dDCD2AaD88b1A6d37DD14f469270A5A",
        #     "0xf3Dc9f08bbDC790816aAB62be1D083fe978A1C90",
        # ]
        # for leader in leaders:
        #     busd.transfer(leader, web3.toWei(1_000, "ether"), {"from": owner})
        #     stake.transfer(leader, web3.toWei(1_000, "ether"), {"from": owner})
        #     owner.transfer(leader, "0.08 ether")
        # lottery.firstStart({"from": owner})
