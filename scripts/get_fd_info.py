from brownie import Foundation
import json


def main():
    wallets = wallets_import

    fd = Foundation.at("0x8517575E21E4c7d310831b5d044434De5Ee89880")

    data = list()
    total = 0
    for wallet in wallets:
        staked = fd.investment(wallet)
        shares = staked["lpDouble"] + 2 * staked["lpSingle"]
        data.append({"user": wallet, "shares": shares})
        total += shares

    with open("fd1_shares.json", "w") as outputfile:
        json.dump({"totalShares": total, "data": data}, outputfile)
