from brownie import NerdFaucetV2, accounts, NerdFaucetV4
import json


def main():
    fc = NerdFaucetV2.at("0xB6169aE16CDf460228E51f57dd6d87F394475551")
    with open("all_v2_wallets.json") as input_file:
        v1_wallets = json.load(input_file)
    block = v1_wallets["snapshot_block"]

    wallet = "0x9975c7d69F2f7cA3df3Cb8818cc2F4b36bD62836"
    req = {
        "accounting": fc.accounting(wallet, block_identifier=block),
        "team": fc.team(wallet, block_identifier=block),
        "leaders": fc.leaders(wallet, block_identifier=block),
    }
    transformed = {
        "user": wallet,
        "accounting": list(
            {
                "netFaucet": req["accounting"]["netFaucet"],
                "deposits": req["accounting"]["deposits"],
                "rolls": req["accounting"]["rolls"],
                "rebaseCompounded": req["accounting"]["rebaseCompounded"],
                "airdrops_rcv": req["accounting"]["airdrops_rcv"],
                "accFaucet": req["accounting"]["accFaucet"],
                "accRebase": req["accounting"]["accRebase"],
                "rebaseCount": req["accounting"]["rebaseCount"],
                "lastAction": req["accounting"]["lastAction"],
                "done": req["accounting"]["done"],
            }.values()
        ),
        "team": req["team"],
        "leaders": req["leaders"],
        "claims": list(
            {
                "faucetClaims": req["accounting"]["faucetClaims"],
                "rebaseClaims": req["accounting"]["rebaseClaims"],
                "faucetEffectiveClaims": req["accounting"]["faucetClaims"],
                "boostEnd": 0,
                "boostLvl": 0,
            }.values()
        ),
    }
    print(transformed)
    dev = accounts.load("stk_dep")
    newFaucet = NerdFaucetV4.at("0x93CC7D788fc350644eb3Bf993487184d5c418c2f")
    newFaucet.migrate(
        transformed["user"],
        transformed["accounting"],
        transformed["team"],
        transformed["leaders"],
        transformed["claims"],
        {"from": dev},
    )
