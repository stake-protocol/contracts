from brownie import NerdFaucetV2
import json


def main():
    fc = NerdFaucetV2.at("0xB6169aE16CDf460228E51f57dd6d87F394475551")
    with open("all_v2_wallets.json") as input_file:
        v1_wallets = json.load(input_file)
    block = v1_wallets["snapshot_block"]

    faucet_data = list()
    for wallet in v1_wallets["wallets"]:
        req = {
            "accounting": fc.accounting(wallet, block_identifier=block),
            "team": fc.team(wallet, block_identifier=block),
            "leaders": fc.leaders(wallet, block_identifier=block),
        }
        transformed = {
            "user": wallet,
            "accounting": {
                "netFaucet": req["accounting"]["netFaucet"],
                "deposits": req["accounting"]["deposits"],
                "rolls": req["accounting"]["rolls"],
                "rebaseCompounded": req["accounting"]["rebaseCompounded"],
                "airdrops_rcv": req["accounting"]["airdrops_rcv"],
                "accFaucet": req["accounting"]["accFaucet"],
                "accRebase": req["accounting"]["accRebase"],
                "rebaseCount": req["accounting"]["rebaseCount"],
                "lastAction": req["accounting"]["lastAction"],
            },
            "team": req["team"],
            "leaders": req["leaders"],
            "claims": {
                "faucetClaims": req["accounting"]["faucetClaims"],
                "rebaseClaims": req["accounting"]["rebaseClaims"],
                "faucetEffectiveClaims": req["accounting"]["faucetClaims"],
                "boostEnd": 0,
                "boostLvl": 0,
            },
        }
        faucet_data.append(transformed)
        print(wallet, len(faucet_data))
    with open("all_v2_data.json", "w") as outputfile:
        json.dump({"data": faucet_data}, outputfile)
