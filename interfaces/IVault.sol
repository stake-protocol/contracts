// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

interface IVault {
    /**
    * @param amount total amount of tokens to recevie
    * @param _type type of spread to execute.
      Stake Vault - Reservoir Collateral - Treasury
      0: do nothing
      1: Buy Spread 5 - 5 - 3
      2: Sell Spread 5 - 5 - 8
    * @param _customTaxSender the address where the tax is originated from.
      @return bool as successful spread op
    **/
    function spread(
        uint256 amount,
        uint8 _type,
        address _customTaxSender
    ) external returns (bool);

    function withdraw(address _address, uint256 amount) external;

    function withdraw(uint256 amount) external;
}
