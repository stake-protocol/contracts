// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface ITokenLocker {
    function updateLock() external;
}
