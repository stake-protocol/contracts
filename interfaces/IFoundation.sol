//SPDX-License-Identifier: MIT

pragma solidity 0.8.17;

interface IFoundation {
    function distribute(uint256 amount) external;
}
