//SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IFountain is IERC20 {
    function removeLiquidity(
        uint256 amount,
        uint256 min_bnb,
        uint256 min_tokens
    ) external returns (uint256, uint256);

    function addLiquidity(
        uint256 min_liquidity,
        uint256 max_tokens,
        uint256 busd_amount
    ) external returns (uint256);

    function txs(address owner) external view returns (uint256);

    function getLiquidityToReserveInputPrice(uint256 amount)
        external
        view
        returns (uint256, uint256);

    function getBnbToLiquidityInputPrice(uint256 bnb_sold)
        external
        view
        returns (uint256, uint256);

    function tokenBalance() external view returns (uint256);

    function bnbBalance() external view returns (uint256);

    function tokenAddress() external view returns (address);

    function getTokenToBnbOutputPrice(uint256 bnb_bought)
        external
        view
        returns (uint256);

    function getTokenToBnbInputPrice(uint256 tokens_sold)
        external
        view
        returns (uint256);

    function getBnbToTokenOutputPrice(uint256 tokens_bought)
        external
        view
        returns (uint256);

    function getBnbToTokenInputPrice(uint256 bnb_sold)
        external
        view
        returns (uint256);

    function tokenToBnbSwapOutput(uint256 bnb_bought, uint256 max_tokens)
        external
        returns (uint256);

    function tokenToBnbSwapInput(uint256 tokens_sold, uint256 min_bnb)
        external
        returns (uint256);

    function bnbToTokenSwapOutput(uint256 tokens_bought)
        external
        payable
        returns (uint256);

    function bnbToTokenSwapInput(uint256 min_tokens, uint256 busd_amount)
        external
        payable
        returns (uint256);

    function getOutputPrice(
        uint256 output_amount,
        uint256 input_reserve,
        uint256 output_reserve
    ) external view returns (uint256);

    function getInputPrice(
        uint256 input_amount,
        uint256 input_reserve,
        uint256 output_reserve
    ) external view returns (uint256);
}
