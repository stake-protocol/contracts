//SPDX-License-Identifier: MIT

pragma solidity 0.8.17;

interface IDistributor {
    function BUSD() external returns (address);

    function distributeFunds(
        uint256 amount,
        uint256 treasury,
        uint256 pricefloor,
        uint256 team
    ) external;
}
