// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

interface ILottery {
    function devAmount() external view returns (uint256);

    function currentRound() external returns (uint256);

    function partners(uint256 id) external view returns (address);

    function roundInfo(uint256 _roundId)
        external
        returns (
            uint256 totalTickets,
            uint256 ticketsClaimed,
            uint32 winnerNumber,
            uint256 pool,
            uint256 endTime,
            uint256 burn,
            uint256 totalWinners,
            uint256 ticketValue // in BUSD
        );
}
