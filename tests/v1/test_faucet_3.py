from datetime import timedelta
from brownie import (
    NerdFaucetV2,
    StakeToken,
    StakeVault,
    TestToken,
    accounts,
    reverts,
    chain,
)
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    pV = accounts[1]
    lottery = accounts[9]
    for i in range(20):
        accounts.add()  # Add 10 more accounts
    leadDrop = accounts[19]
    # Deploy contracts
    token = StakeToken.deploy(pV, {"from": owner})
    govToken = TestToken.deploy("GOV", "Governance", {"from": owner})
    vault = StakeVault.deploy(token, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    faucet = NerdFaucetV2.deploy(
        token, vault, govToken, leadDrop, owner, {"from": owner}
    )
    faucet.setLottery(lottery, {"from": owner})
    # Make necessary exclusions
    vault.addWhitelist(faucet, {"from": owner})
    token.excludeAddress(owner, True, False, {"from": owner})
    # token.excludeAddress(vault, False, False, {"from": owner})
    token.excludeMultiple([lottery, leadDrop], False, True, {"from": owner})
    token.excludeAddress(faucet, False, False, {"from": owner})
    token.setPool(faucet, {"from": owner})
    # Users have some tokens
    token.transfer(vault, Web3.toWei(11000, "ether"), {"from": owner})
    govToken.transfer(accounts[1], Web3.toWei(2, "ether"), {"from": owner})
    for i in range(20):
        if i == 9:
            continue
        token.transfer(accounts[i], Web3.toWei(1000, "ether"), {"from": owner})

    faucet.editLevelRequirements(1, 1, 0, 0, {"from": owner})
    faucet.editLevelRequirements(2, 2, 0, 0, {"from": owner})
    faucet.editLevelRequirements(3, 3, 1, 8, {"from": owner})

    return owner, faucet, token, vault, lottery, leadDrop, govToken


# Test that all the correct things happen on deposit
# 1. Amount deposited - 10% is credited to user
# 2. If Owner, then share amount is sent to leader(2nd) or lottery(1st)
# 3. 2nd user, deposit, Referral sends 10% to upline
# 4. 3rd user, deposit with user2 as main but since 2nd doesnt have gov, no airdrop is made.


# def test_deposit(setup):
#     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup

#     user1 = accounts[1]
#     user2 = accounts[2]
#     user3 = accounts[3]

#     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
#     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
#     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})

#     with reverts("Invalid upline address"):
#         faucet.deposit(
#             Web3.toWei(110, "ether"),
#             "0x0000000000000000000000000000000000000000",
#             {"from": user1},
#         )
#     with reverts("Invalid user for upline"):
#         faucet.deposit(
#             Web3.toWei("111.111111111111111111111", "ether"), user2, {"from": user1}
#         )

#     faucet.deposit(
#         Web3.toWei("111.111111111111111111111", "ether"), owner, {"from": user1}
#     )

#     u1_deposit_1 = faucet.accounting(user1)
#     u1_team_1 = faucet.team(user1)
#     # 1 ---
#     assert u1_deposit_1["deposits"] == Web3.toWei(100, "ether")
#     # 2------
#     assert token.balanceOf(lottery) == Web3.toWei("10", "ether")

#     faucet.deposit(
#         Web3.toWei("111.111111111111111111111", "ether"), owner, {"from": user1}
#     )
#     # assert token.balanceOf(leadDrop) == Web3.toWei("1010", "ether")
#     # 3 ----
#     faucet.deposit(
#         Web3.toWei("111.111111111111111111111", "ether"), user1, {"from": user2}
#     )
#     assert faucet.accounting(user1)["airdrops_rcv"] == Web3.toWei(10, "ether")
#     # 4 ----
#     faucet.deposit(
#         Web3.toWei("111.111111111111111111111", "ether"), user2, {"from": user3}
#     )
#     assert faucet.accounting(user2)["airdrops_rcv"] == Web3.toWei(0, "ether")

#     assert faucet.team(owner)["maxDownline"] == 3
#     assert faucet.team(user1)["maxDownline"] == 2
#     assert faucet.team(user2)["maxDownline"] == 1
#     assert faucet.team(user3)["maxDownline"] == 0


# # def test_claim_all_tests(setup):
# #     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup

# #     user1 = accounts[1]
# #     user2 = accounts[2]
# #     user3 = accounts[3]

# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})

# #     dep = chain.time()
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), owner, {"from": user1}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user1, {"from": user2}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user2, {"from": user3}
# #     )

# #     chain.mine(10, timedelta=24 * 3600)  # 24 hours pass
# #     # TEST CLAIM ALL
# #     u1_payout_calc = faucet.getNerdData(user1).dict()
# #     u1_status_acc = faucet.accounting(user1).dict()
# #     u1_balance = token.balanceOf(user1)

# #     assert (
# #         (u1_status_acc["netFaucet"] / 100 * 0.999999)
# #         <= u1_payout_calc["_faucetPayout"]
# #         <= u1_status_acc["netFaucet"] / 100 * 1.0001
# #     )
# #     assert u1_payout_calc["_rebasePayout"] == u1_status_acc["netFaucet"] * 2 / 100

# #     faucet.claim({"from": user1})
# #     assert (
# #         int(
# #             (u1_payout_calc["_faucetPayout"] + u1_payout_calc["_rebasePayout"])
# #             * 9
# #             * 0.9999
# #             / 10
# #         )
# #         <= token.balanceOf(user1) - u1_balance
# #         <= int(
# #             (u1_payout_calc["_faucetPayout"] + u1_payout_calc["_rebasePayout"])
# #             * 9
# #             * 1.0001
# #             / 10
# #         )
# #     )
# #     # TEST CLAIM REBASE
# #     u2_payout_calc = faucet.getNerdData(user2).dict()
# #     u2_status_acc = faucet.accounting(user2).dict()
# #     u2_balance = token.balanceOf(user2)

# #     faucet.rebaseClaim({"from": user2})
# #     assert (
# #         int((u2_payout_calc["_rebasePayout"]) * 9 * 0.9999 / 10)
# #         <= token.balanceOf(user2) - u2_balance
# #         <= int((u2_payout_calc["_rebasePayout"]) * 9 * 1.00001 / 10)
# #     )
# #     # TEST CLAIM FAUCET
# #     u3_payout_calc = faucet.getNerdData(user3).dict()
# #     u3_balance = token.balanceOf(user3)

# #     faucet.faucetClaim({"from": user3})
# #     u3_status_acc = faucet.accounting(user3).dict()
# #     assert (
# #         int((u3_payout_calc["_faucetPayout"]) * 9 * 0.9999 / 10)
# #         <= token.balanceOf(user3) - u3_balance
# #         <= int((u3_payout_calc["_faucetPayout"]) * 9 * 1.0001 / 10)
# #     )
# #     assert u3_status_acc["accRebase"] > 0
# #     assert u3_status_acc["accRebase"] == u3_payout_calc["_rebasePayout"]


# # def test_compound_all_tests(setup):
# #     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup

# #     user1 = accounts[1]
# #     user2 = accounts[2]
# #     user3 = accounts[3]

# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})

# #     dep = chain.time()
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), owner, {"from": user1}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user1, {"from": user2}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user2, {"from": user3}
# #     )

# #     chain.mine(10, timedelta=24 * 3600)  # 24 hours pass

# #     u1_payout_calc = faucet.getNerdData(user1).dict()
# #     u1_balance = token.balanceOf(user1)
# #     u2_payout_calc = faucet.getNerdData(user2).dict()
# #     u2_balance = token.balanceOf(user2)
# #     u3_payout_calc = faucet.getNerdData(user3).dict()
# #     u3_balance = token.balanceOf(user3)

# #     faucet.compoundAll({"from": user1})
# #     faucet.compoundRebase({"from": user2})
# #     faucet.compoundFaucet({"from": user3})

# #     u1_status_acc = faucet.accounting(user1).dict()
# #     u2_status_acc = faucet.accounting(user2).dict()
# #     u3_status_acc = faucet.accounting(user3).dict()

# #     assert (
# #         int(u1_status_acc["rolls"] * 0.99999)
# #         <= int(u1_payout_calc["_faucetPayout"] * 95 / 100)
# #         <= int(u1_status_acc["rolls"] * 1.0001)
# #     )
# #     assert (
# #         int(u1_status_acc["rebaseCompounded"] * 0.99999)
# #         <= int(u1_payout_calc["_rebasePayout"] * 95 / 100)
# #         <= int(u1_status_acc["rebaseCompounded"] * 1.0001)
# #     )
# #     assert (
# #         int(u2_status_acc["rebaseCompounded"] * 0.99999)
# #         <= int(u2_payout_calc["_rebasePayout"] * 95 / 100)
# #         <= int(u2_status_acc["rebaseCompounded"] * 1.0001)
# #     )
# #     assert u2_status_acc["rolls"] == 0
# #     assert (
# #         int(u3_status_acc["rolls"] * 0.99999)
# #         <= int(u3_payout_calc["_faucetPayout"] * 95 / 100)
# #         <= int(u3_status_acc["rolls"] * 1.00001)
# #     )
# #     assert u3_status_acc["rebaseCompounded"] == 0


# # def test_switch_teams(setup):
# #     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup
# #     user1 = accounts[1]
# #     user2 = accounts[2]
# #     user3 = accounts[3]

# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})

# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), owner, {"from": user1}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user1, {"from": user2}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("111.111111111111111111111", "ether"), user2, {"from": user3}
# #     )

# #     faucet.airdrop(
# #         user2, Web3.toWei("11.111111111111111111111", "ether"), 0, {"from": user1}
# #     )
# #     assert faucet.accounting(user2)["airdrops_rcv"] == Web3.toWei(10, "ether")
# #     assert faucet.team(user1)["airdrops_sent"] == Web3.toWei(
# #         "11.111111111111111111111", "ether"
# #     )

# #     u2_bal = token.balanceOf(user2)

# #     assert faucet.team(user3)["structure"] == 0
# #     faucet.switchTeam(user3, {"from": user2})
# #     assert faucet.team(user3)["structure"] == 1

# #     assert faucet.team(user2)["prevUpline"] == user1
# #     assert faucet.team(user2)["upline"] == user3
# #     assert u2_bal - token.balanceOf(user2) == int(
# #         faucet.accounting(user2)["airdrops_rcv"] / 10
# #     )

# #     chain.mine(10, timedelta=24 * 3600)  # 24 hours pass
# #     assert faucet.team(user3)["upline"] == user1
# #     user3_compound = int(
# #         (
# #             faucet.getNerdData(user3)["_faucetPayout"]
# #             + faucet.getNerdData(user3)["_rebasePayout"]
# #         )
# #         * 0.05
# #         / 0.95
# #     )
# #     assert faucet.team(user3)["referrals"] == 1
# #     assert faucet.team(user3)["structure"] == 1
# #     faucet.compoundAll({"from": user3})
# #     assert faucet.team(user3)["upline"] == user1
# #     assert faucet.team(user1)["referrals"] == 1
# #     assert faucet.team(user1)["structure"] == 2
# #     assert faucet.accounting(user1)["airdrops_rcv"] == user3_compound + Web3.toWei(
# #         "10", "ether"
# #     )


# # def test_lvl_3_status(setup):
# #     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup
# #     # setup is ready
# #     user1 = accounts[1]
# #     user2 = accounts[2]
# #     user3 = accounts[3]
# #     user4 = accounts[4]
# #     user5 = accounts[5]
# #     user6 = accounts[6]
# #     user7 = accounts[7]
# #     user8 = accounts[8]
# #     user9 = accounts[10]
# #     user10 = accounts[11]

# #     govToken.transfer(user2, Web3.toWei(2, "ether"), {"from": owner})

# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user4})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user5})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user6})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user7})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user8})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user9})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user10})

# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), owner, {"from": user1}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user1, {"from": user2}
# #     )
# #     assert faucet.userLevel(user1) == 1
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user1, {"from": user3}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user1, {"from": user4}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user5}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user6}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user7}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user8}
# #     )
# #     assert faucet.userLevel(user1) == 2
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user9}
# #     )
# #     assert faucet.userLevel(user1) == 3
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user2, {"from": user10}
# #     )
# #     user2_acc = faucet.accounting(user2)
# #     # assert user2_acc["accFaucet"] == user2_acc["netFaucet"] * (
# #     #     chain[-1]["timestamp"] - user2_acc["lastAction"]
# #     # ) / (100 * 3600 * 24)
# #     assert faucet.userLevel(user1) == 3
# #     assert faucet.team(user1)["leaders"] == 1


# # def test_vault_check(setup):
# #     (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup
# #     # setup is ready
# #     user1 = accounts[1]
# #     user2 = accounts[2]

# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
# #     token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})

# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), owner, {"from": user1}
# #     )
# #     faucet.deposit(
# #         Web3.toWei("11.111111111111111111111", "ether"), user1, {"from": user2}
# #     )

# #     vault_stat = faucet.checkVault().dict()
# #     assert vault_stat["_status"] == Web3.toWei(
# #         20 * 21 * 3 / 100, "ether"
# #     )  # 20 receiving 3% staked for 21 days
# #     assert vault_stat["_vaultBalance"] == token.balanceOf(vault)
# #     assert (
# #         int((token.balanceOf(vault)) * 0.67 * 0.9999)
# #         <= vault_stat["_needs"]
# #         <= int((token.balanceOf(vault)) * 0.67)
# #     )
# #     assert vault_stat["_threshold"] == int(
# #         (token.balanceOf(vault)) * 0.5
# #     )  # 20 receiving 3% staked for 21 days


def test_over_cap(setup):
    (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup
    # setup is ready
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]
    user5 = accounts[5]
    user6 = accounts[6]
    user7 = accounts[7]
    user8 = accounts[8]
    user9 = accounts[10]
    user10 = accounts[11]

    govToken.transfer(user2, Web3.toWei(2, "ether"), {"from": owner})

    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user2})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user3})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user4})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user5})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user6})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user7})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user8})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user9})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user10})

    token.transfer(user1, Web3.toWei(33000, "ether"), {"from": owner})
    token.transfer(vault, Web3.toWei(200000, "ether"), {"from": owner})
    token.approve(faucet, Web3.toWei(750000, "ether"), {"from": user1})
    # BY EOY accumulate the 10% of cap
    faucet.deposit(Web3.toWei(33000, "ether"), owner, {"from": user1})

    chain.mine(100, timedelta=(3600 * 24 * 370))  # 100 days

    faucet.claim({"from": user1})

    with reverts("Min Level 3"):
        faucet.leaderReset(Web3.toWei(750, "ether"), {"from": user1})

    # Once cap reached try to reset
    faucet.deposit(Web3.toWei("11", "ether"), user1, {"from": user2})
    faucet.deposit(Web3.toWei("11", "ether"), user1, {"from": user3})
    faucet.deposit(Web3.toWei("11", "ether"), user1, {"from": user4})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user5})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user6})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user7})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user8})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user9})
    faucet.deposit(Web3.toWei("11", "ether"), user2, {"from": user10})

    faucet.leaderReset(Web3.toWei(761, "ether"), {"from": user1})
    u1_reset = faucet.accounting(user1)
    assert (
        Web3.toWei(9.9 * 0.9999999999999, "ether")
        <= u1_reset["deposits"]
        <= Web3.toWei(9.9 * 1.00000000000001, "ether")
    )
    assert u1_reset["airdrops_rcv"] == 0
    # assert False
    pass


def test_kickback(setup):

    (owner, faucet, token, vault, lottery, leadDrop, govToken) = setup

    assert faucet.kickback(1) == 1
    assert faucet.kickback(2) == 5
    assert faucet.kickback(3) == 10
    assert faucet.kickback(4) == 15
    assert faucet.kickback(5) == 20

    faucet.setKickbacks([1, 2, 3, 4, 5], {"from": owner})
    assert faucet.kickback(1) == 1
    assert faucet.kickback(2) == 2
    assert faucet.kickback(3) == 3
    assert faucet.kickback(4) == 4
    assert faucet.kickback(5) == 5
