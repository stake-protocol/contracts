from brownie import NerdFaucetV2, StakeToken, StakeVault, TestToken, accounts, chain
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    pV = accounts[1]
    lottery = accounts[9]
    for i in range(10):
        accounts.add()  # Add 10 more accounts
    leadDrop = accounts[19]
    # Deploy contracts
    token = StakeToken.deploy(pV, {"from": owner})
    govToken = TestToken.deploy({"from": owner})
    vault = StakeVault.deploy(token, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    faucet = NerdFaucetV2.deploy(token, vault, govToken, leadDrop, {"from": owner})
    faucet.setLottery(lottery, {"from": owner})
    # Make necessary exclusions
    vault.addWhitelist(faucet, {"from": owner})
    token.excludeAddress(owner, True, False, {"from": owner})
    token.excludeAddress(vault, False, False, {"from": owner})
    token.excludeAddress(faucet, False, False, {"from": owner})
    token.setPool(faucet, {"from": owner})
    # Users have some tokens
    token.transfer(vault, Web3.toWei(11000, "ether"), {"from": owner})
    for i in range(20):
        if i == 9:
            continue
        token.transfer(accounts[i], Web3.toWei(1000, "ether"), {"from": owner})

    return owner, faucet, token, vault


def test_deposit(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]
    lottery = accounts[9]
    # Make Deposit
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})

    userAccounting = faucet.accounting(user1).dict()
    userTeam = faucet.team(user1).dict()
    ownerTeam = faucet.team(owner).dict()

    assert token.balanceOf(vault) == Web3.toWei(
        11000 + (100 - (100 * 0.9 * 0.1 * 0.9)), "ether"
    )
    assert userAccounting.get("netFaucet") == Web3.toWei(90, "ether")
    assert userAccounting.get("deposits") == Web3.toWei(90, "ether")
    assert userTeam.get("upline") == owner
    assert ownerTeam.get("referrals") == 1
    assert faucet.accLiability() == Web3.toWei(90 * 0.03, "ether")


def test_rewards_on_single_user(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]

    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    chain.mine(blocks=10, timedelta=86400)

    userData = faucet.getNerdData(user1).dict()
    assert userData.get("_grossClaimed") == 0
    assert userData.get("_netDeposits") == Web3.toWei(90, "ether")
    assert userData.get("_netFaucetValue") == Web3.toWei(90, "ether")
    assert userData.get("_grossFaucetValue") == Web3.toWei(90, "ether")
    # payout is within range
    assert (
        Web3.toWei((90 * 0.01) - 0.001, "ether")
        <= userData.get("_faucetPayout")
        <= Web3.toWei(90 * 0.01 + 0.001, "ether")
    )
    assert userData.get("_rebasePayout") == Web3.toWei(90 * 0.02, "ether")


def test_faucet_claim(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    chain.mine(blocks=10, timedelta=86400)

    faucet.faucetClaim({"from": user1})

    userData = faucet.getNerdData(user1).dict()
    assert (
        Web3.toWei(900 * 0.999, "ether")
        <= token.balanceOf(user1)
        <= Web3.toWei(990 * 1.001, "ether")
    )
    assert faucet.accounting(user1).dict().get("accRebase") == Web3.toWei(1.8, "ether")


def test_rebase_claim(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    chain.mine(blocks=10, timedelta=86400)

    faucet.rebaseClaim({"from": user1})

    userData = faucet.getNerdData(user1).dict()
    assert Web3.toWei(900 + (1.8 * 0.9), "ether") == token.balanceOf(user1)
    assert userData.get("_grossClaimed") == Web3.toWei(1.8, "ether")


def test_compound_rebase(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    chain.mine(blocks=10, timedelta=86400)

    faucet.compoundRebase({"from": user1})
    userDataAfterCompound = faucet.accounting(user1).dict()

    chain.mine(blocks=10, timedelta=86400)
    userDataAfterTime = faucet.getNerdData(user1).dict()
    # 5% fee on compound
    assert userDataAfterCompound.get("rebaseCompounded") == Web3.toWei(1.71, "ether")
    assert userDataAfterTime.get("_rebasePayout") == Web3.toWei(91.71 * 0.02, "ether")


def test_compound_rebase(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user5 = accounts[5]
    lottery = accounts[9]
    leader = accounts[19]
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    chain.mine(blocks=10, timedelta=86400)
    predepData = faucet.getNerdData(user1)
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})
    # funds should go to lottery (10%)
    assert (
        Web3.toWei(
            90 * 0.9 * 0.1 - 0.00001,
            "ether",
        )
        <= token.balanceOf(lottery)
        <= Web3.toWei(
            90 * 0.9 * 0.1 + 0.00001,
            "ether",
        )
    )
    # 1000 is of initial token value in wallet
    assert (
        Web3.toWei(
            1000 + (90 + (1.8 * 0.95) + (0.9 * 0.95)) * 0.9 * 0.1 - 0.00001,
            "ether",
        )
        <= token.balanceOf(leader)
        <= Web3.toWei(
            1000 + (90 + (1.8 * 0.95) + (0.9 * 0.95)) * 0.9 * 0.1 + 0.00001,
            "ether",
        )
    )
    assert faucet.accLiability() == Web3.toWei(
        (90 + (1.8 * 0.95) + (0.9 * 0.95) + 90) * 0.03, "ether"
    )


def test_upline_counter(setup):
    owner, faucet, token, vault = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    user4 = accounts[5]
    user5 = accounts[6]
    user6 = accounts[7]
    lottery = accounts[9]

    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user1})

    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user2})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user2})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user3})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user3})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user4})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user4})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user5})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user5})

    assert faucet.getLeaderAmount(owner).dict().get("_directLeaders") == 1
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": user6})
    faucet.deposit(Web3.toWei(100, "ether"), owner, {"from": user6})
    assert faucet.getLeaderAmount(owner).dict().get("_directLeaders") == 1
    assert faucet.getLeaderAmount(owner).dict().get("_structureLeaders") == 0

    token.approve(faucet, Web3.toWei(100, "ether"), {"from": accounts[8]})
    faucet.deposit(Web3.toWei(100, "ether"), user1, {"from": accounts[8]})

    token.approve(faucet, Web3.toWei(100, "ether"), {"from": accounts[10]})
    faucet.deposit(Web3.toWei(100, "ether"), user1, {"from": accounts[10]})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": accounts[11]})
    faucet.deposit(Web3.toWei(100, "ether"), user1, {"from": accounts[11]})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": accounts[12]})
    faucet.deposit(Web3.toWei(100, "ether"), user1, {"from": accounts[12]})
    token.approve(faucet, Web3.toWei(100, "ether"), {"from": accounts[13]})
    faucet.deposit(Web3.toWei(100, "ether"), user1, {"from": accounts[13]})

    assert faucet.getLeaderAmount(owner).dict().get("_structureLeaders") == 1
    assert faucet.accLiability() == Web3.toWei(11 * 90 * 0.03, "ether")
