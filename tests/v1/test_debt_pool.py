from brownie import DebtPool, TestToken, accounts
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    token = TestToken.deploy({"from": owner})
    pool = DebtPool.deploy(token, {"from": owner})
    return token, pool, owner


def test_add_debt(setup):
    (token, pool, owner) = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    pool.setUser(user1, Web3.toWei(121, "ether"), {"from": owner})
    assert pool.totalOwed() == Web3.toWei(121, "ether")
    pool.setUser(user2, Web3.toWei(200, "ether"), {"from": owner})
    assert pool.totalOwed() == Web3.toWei(321, "ether")
    pool.setUser(user3, Web3.toWei(310, "ether"), {"from": owner})
    assert pool.totalOwed() == Web3.toWei(631, "ether")
    pool.setUser(user4, Web3.toWei(10, "ether"), {"from": owner})
    assert pool.totalOwed() == Web3.toWei(641, "ether")


def test_owed(setup):
    (token, pool, owner) = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    pool.setUser(user1, Web3.toWei(121, "ether"), {"from": owner})
    pool.setUser(user2, Web3.toWei(200, "ether"), {"from": owner})
    pool.setUser(user3, Web3.toWei(310, "ether"), {"from": owner})
    pool.setUser(user4, Web3.toWei(10, "ether"), {"from": owner})

    token.transfer(pool, Web3.toWei(100, "ether"), {"from": owner})

    pendingClaim = pool.pendingClaim(user1)
    pendingAlloc = pool.pendingAllocation()
    assert (
        abs(pendingClaim - Web3.toWei(121 * 100 / 641, "ether")) < 1000
    )  # Difference in least significant digits
    assert pendingAlloc == Web3.toWei(100, "ether")


def test_owed(setup):
    (token, pool, owner) = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    pool.setUser(user1, Web3.toWei(121, "ether"), {"from": owner})
    pool.setUser(user2, Web3.toWei(200, "ether"), {"from": owner})
    pool.setUser(user3, Web3.toWei(310, "ether"), {"from": owner})
    pool.setUser(user4, Web3.toWei(10, "ether"), {"from": owner})

    token.transfer(pool, Web3.toWei(100, "ether"), {"from": owner})

    pool.claim({"from": user1})

    assert pool.pendingAllocation() == 0
    assert pool.pendingClaim(user1) == 0
    assert abs(token.balanceOf(user1) - Web3.toWei(121 * 100 / 641, "ether")) < 100000
    assert (
        abs(
            pool.debt(user1).dict()["amountWithdrawn"]
            - Web3.toWei(121 * 100 / 641, "ether")
        )
        < 1000000
    )

    assert abs(pool.pendingClaim(user2) - Web3.toWei(200 * 100 / 641, "ether")) < 100000
    assert abs(pool.pendingClaim(user3) - Web3.toWei(310 * 100 / 641, "ether")) < 100000
    assert abs(pool.pendingClaim(user4) - Web3.toWei(10 * 100 / 641, "ether")) < 100000


def test_full_claim(setup):
    (token, pool, owner) = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    pool.setUser(user1, Web3.toWei(121, "ether"), {"from": owner})
    pool.setUser(user2, Web3.toWei(200, "ether"), {"from": owner})
    pool.setUser(user3, Web3.toWei(310, "ether"), {"from": owner})
    pool.setUser(user4, Web3.toWei(10, "ether"), {"from": owner})

    token.transfer(pool, Web3.toWei(1000, "ether"), {"from": owner})

    pool.claim({"from": user1})
    pool.claim({"from": user2})
    pool.claim({"from": user3})
    pool.claim({"from": user4})

    assert token.balanceOf(pool) == Web3.toWei(1000, "ether") - pool.totalWithdrawn()
    assert token.balanceOf(user1) == Web3.toWei(121, "ether")
    assert token.balanceOf(user2) == Web3.toWei(200, "ether")
    assert token.balanceOf(user3) == Web3.toWei(310, "ether")
    assert token.balanceOf(user4) == Web3.toWei(10, "ether")

    pool.extractBEP20(token, {"from": owner})


def test_add_debt_multiple(setup):
    (token, pool, owner) = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]
    users = [user1, user2, user3, user4]
    amounts = [
        Web3.toWei(121, "ether"),
        Web3.toWei(200, "ether"),
        Web3.toWei(310, "ether"),
        Web3.toWei(10, "ether"),
    ]

    pool.addMultipleUsers(users, amounts, {"from": owner})
    assert pool.totalOwed() == Web3.toWei(641, "ether")
    assert pool.usersOwed() == 4
