from brownie import (
    accounts,
    chain,
    reverts,
    StakeLP2,
    StakeToken,
    TokenLocker,
    TestToken,
)
import pytest
from web3 import Web3

# THESE TESTS WERE MODIFIED FOR THE StakeFountainV2 WHICH USES BUSD INSTEAD OF BNB. TREAT EVERY BNB INSTANCE AS BUSD.


@pytest.fixture(scope="function", autouse=True)
def setup():
    owner = accounts[0]
    treasury = accounts[1]
    vault = accounts[2]
    foundation = accounts[3]
    user1 = accounts[4]
    user2 = accounts[5]
    user3 = accounts[6]

    token = StakeToken.deploy(vault, {"from": owner})
    busd_token = TestToken.deploy({"from": owner})
    fountain = StakeLP2.deploy(
        token.address, treasury, vault, foundation, busd_token, {"from": owner}
    )
    # Adding liquidity as owner for the first time. 60 BNB and 21000 STAKE.
    lock = TokenLocker.deploy(fountain.address, {"from": owner})
    fountain.setLock(lock.address, {"from": accounts[0]})
    token.excludeAddress(fountain, True, True)
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": owner})
    fountain.addLiquidity(
        Web3.toWei(1, "ether"),
        Web3.toWei(21000, "ether"),
        Web3.toWei(60, "ether"),
        {"from": owner},
    )

    return (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    )


def test_add_liquidity(setup):

    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # OWNER TESTS
    # Checking bnb and stake balance in contract
    assert busd_token.balanceOf(fountain) == Web3.toWei(60, "ether")
    assert token.balanceOf(fountain) == Web3.toWei(21000, "ether")

    # Checking owner balance of LP token
    assert fountain.balanceOf(owner) == Web3.toWei(60, "ether")

    # USER TESTS
    # Owner transfers some STAKE to user
    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})

    # User adding liquidity
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    fountain.addLiquidity(
        Web3.toWei(1, "ether"),
        Web3.toWei(400, "ether"),
        Web3.toWei(1, "ether"),
        {"from": user1},
    )

    # Checking bnb and stake balance in contract
    lp_and_tokens_minted = fountain.getBnbToLiquidityInputPrice(Web3.toWei(1, "ether"))
    lp_minted = lp_and_tokens_minted[0]
    calc_reserve = fountain.getLiquidityToReserveInputPrice(lp_minted)
    calc_bnb = calc_reserve[0]
    calc_tokens = calc_reserve[1]

    assert busd_token.balanceOf(fountain) == Web3.toWei(60, "ether") + calc_bnb
    assert (
        abs(token.balanceOf(fountain) - (Web3.toWei(21000, "ether") + calc_tokens)) < 10
    )
    # Checking user1 LP balance
    assert fountain.balanceOf(user1) == lp_minted


def test_remove_liquidity(setup):
    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # USER TESTS
    # Owner transfers some STAKE to user
    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})

    # User adding liquidity
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    fountain.addLiquidity(
        1,
        Web3.toWei(1000, "ether"),
        Web3.toWei(2, "ether"),
        {"from": user1},
    )

    # User removes liquidity, checking if liq got burned and tokens got transferred
    original_user_bnb_balance = busd_token.balanceOf(user1)
    original_user_token_balance = token.balanceOf(user1)
    original_user_liq_balance = fountain.balanceOf(user1)
    liq_to_remove_and_tokens = fountain.getBnbToLiquidityInputPrice(
        Web3.toWei(1, "ether")
    )
    liq_to_remove = liq_to_remove_and_tokens[0]
    reserve_to_receive = fountain.getLiquidityToReserveInputPrice(liq_to_remove)
    bnb_to_receive = reserve_to_receive[0]
    tokens_to_receive = reserve_to_receive[1]

    fountain.removeLiquidity(Web3.toWei(1, "ether"), 1, 1, {"from": user1})

    new_user_bnb_balance = busd_token.balanceOf(user1)
    new_user_token_balance = token.balanceOf(user1)
    new_user_lp_balance = fountain.balanceOf(user1)

    assert original_user_liq_balance - liq_to_remove == new_user_lp_balance
    assert original_user_bnb_balance + bnb_to_receive == new_user_bnb_balance
    assert original_user_token_balance + tokens_to_receive == new_user_token_balance


def test_swap_bnb_to_stake_input(setup):
    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # Setup
    busd_token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})

    busd_token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})
    success = fountain.addAddressToWhitelist(user2, {"from": owner})

    # Testing  NOT WHITELISTED account to swap with tax
    original_user1_balance = user1.balance()
    original_user2_balance = user2.balance()
    original_treasury_balance = busd_token.balanceOf(treasury)

    calc_tokens_bought = fountain.getBnbToTokenInputPrice(Web3.toWei(10, "ether"))

    tokens_bought = fountain.bnbToTokenSwapInput(
        1, Web3.toWei(10, "ether"), {"from": user1}
    )
    # Checking user token balance
    assert token.balanceOf(user1) == 0.97 * calc_tokens_bought

    # Checking treasury BNB balance
    assert busd_token.balanceOf(
        treasury
    ) == original_treasury_balance + 0.03 * Web3.toWei(10, "ether")

    original_treasury_balance2 = busd_token.balanceOf(treasury)
    # Testing WHITELISTED account to swap without tax
    calc_tokens_bought2 = fountain.getBnbToTokenInputPrice(Web3.toWei(10, "ether"))
    tokens_bought2 = fountain.bnbToTokenSwapInput(
        1, Web3.toWei(10, "ether"), {"from": user2}
    )

    # Checking user token balance
    assert token.balanceOf(user2) == calc_tokens_bought2

    # Checking treasury BNB balance
    assert busd_token.balanceOf(treasury) == original_treasury_balance2


def test_swap_bnb_to_stake_output(setup):
    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # Setup
    busd_token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})

    busd_token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})
    success = fountain.addAddressToWhitelist(user2, {"from": owner})

    # Testing account to swap with tax
    original_user1_balance = busd_token.balanceOf(user1)
    original_treasury_balance = busd_token.balanceOf(treasury)
    max_bnb = fountain.getBnbToTokenOutputPrice(Web3.toWei(100 * 300 / 97, "ether"))

    fountain.bnbToTokenSwapOutput(Web3.toWei(300, "ether"), max_bnb, {"from": user1})
    # Checking user token and BNB balance
    assert busd_token.balanceOf(user1) - (original_user1_balance + max_bnb) < 10
    assert token.balanceOf(user1) == Web3.toWei(300, "ether")

    # Checking treasury BNB balance
    assert (
        busd_token.balanceOf(treasury) - (original_treasury_balance + 0.03 * max_bnb)
        < 10
    )

    # Testing account without swap tax
    original_user2_balance = busd_token.balanceOf(user2)
    original_treasury_balance2 = busd_token.balanceOf(treasury)
    max_bnb2 = fountain.getBnbToTokenOutputPrice(Web3.toWei(300, "ether"))

    fountain.bnbToTokenSwapOutput(Web3.toWei(300, "ether"), max_bnb2, {"from": user2})
    # Checking user token and BNB balance
    assert token.balanceOf(user2) == Web3.toWei(300, "ether")
    assert busd_token.balanceOf(user2) == original_user2_balance - max_bnb2

    # Checking that treasury balance hasn't changed
    assert busd_token.balanceOf(treasury) == original_treasury_balance2


def test_swap_stake_to_bnb_input(setup):
    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # Setup
    busd_token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})

    busd_token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})

    busd_token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})

    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})

    success = fountain.addAddressToWhitelist(user2, {"from": owner})

    # Swapping from stake to bnb
    original_bnb_balance = busd_token.balanceOf(user1)
    original_token_balance = token.balanceOf(user1)
    calc_bnb_bought = fountain.getTokenToBnbInputPrice(Web3.toWei(300, "ether"))

    original_treasury_balance = busd_token.balanceOf(treasury)
    original_vault_token_balance = token.balanceOf(vault)

    liquidity_and_tokens_for_lock_and_foundation = fountain.getBnbToLiquidityInputPrice(
        0.025 * calc_bnb_bought
    )
    liquidity_for_lock_and_foundation = liquidity_and_tokens_for_lock_and_foundation[0]
    liquidity_for_foundation = 0.1 * liquidity_for_lock_and_foundation
    liquidity_for_lock = liquidity_for_lock_and_foundation - liquidity_for_foundation

    fountain.tokenToBnbSwapInput(Web3.toWei(300, "ether"), 1, {"from": user1})

    # Checking user1 token and bnb balance
    assert token.balanceOf(user1) == original_token_balance - Web3.toWei(300, "ether")
    # Taking gas into account
    assert (
        busd_token.balanceOf(user1) - (original_bnb_balance + 0.82 * calc_bnb_bought)
    ) < 100

    # Checking treasury BNB balance, taking rounding into account
    assert (
        busd_token.balanceOf(treasury)
        - (original_treasury_balance + 0.08 * calc_bnb_bought)
    ) < 10

    assert token.balanceOf(vault) - original_vault_token_balance == 0.05 * Web3.toWei(
        300, "ether"
    )

    # Checking foundation's liquidity
    assert (fountain.balanceOf(foundation) - liquidity_for_foundation) < 10

    # Checking lock's liquidity
    assert (fountain.balanceOf(lock) - liquidity_for_lock) < 10

    # Testing account without swap tax
    original_token_balance2 = token.balanceOf(user2)
    original_bnb_balance2 = busd_token.balanceOf(user2)
    calc_bnb_bought2 = fountain.getTokenToBnbInputPrice(Web3.toWei(300, "ether"))
    fountain.tokenToBnbSwapInput(Web3.toWei(300, "ether"), 1, {"from": user2})
    # Checking user token and BNB balance
    assert token.balanceOf(user2) == original_token_balance2 - Web3.toWei(300, "ether")
    assert busd_token.balanceOf(user2) - (original_bnb_balance2 + calc_bnb_bought2) == 0


def test_swap_stake_to_bnb_output(setup):
    (
        owner,
        treasury,
        vault,
        foundation,
        user1,
        user2,
        user3,
        token,
        busd_token,
        fountain,
        lock,
    ) = setup

    # Setup
    success = fountain.addAddressToWhitelist(user2, {"from": owner})
    token.transfer(user1, Web3.toWei(10000, "ether"), {"from": owner})
    token.transfer(user2, Web3.toWei(10000, "ether"), {"from": owner})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user1})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": user2})

    # Testing account to swap with tax
    original_user1_balance = busd_token.balanceOf(user1)
    original_user1_token_balance = token.balanceOf(user1)
    original_fountain_token_balance = token.balanceOf(fountain)

    original_treasury_balance = busd_token.balanceOf(treasury)
    original_vault_token_balance = token.balanceOf(vault)

    exact_output_bnb = Web3.toWei(10, "ether")
    max_tokens = fountain.getTokenToBnbOutputPrice(exact_output_bnb) / 0.82
    token_reserve = token.balanceOf(fountain)
    bnb_reserve = busd_token.balanceOf(fountain)
    assert max_tokens - (
        (((token_reserve * exact_output_bnb) / (bnb_reserve - exact_output_bnb)) + 1)
        / 0.82
    ) < Web3.toWei(1, "gwei")

    fountain.tokenToBnbSwapOutput(exact_output_bnb, max_tokens, {"from": user1})
    # Checking user token and BNB balance
    assert busd_token.balanceOf(user1) == (original_user1_balance + exact_output_bnb)

    # Checking tokens charged from user and token balance from fountain
    assert abs(
        token.balanceOf(user1) - (original_user1_token_balance - max_tokens)
    ) < Web3.toWei(1, "gwei")
    assert (
        token.balanceOf(fountain) - (original_fountain_token_balance + max_tokens)
    ) < Web3.toWei(1, "gwei")

    # Checking that taxes have been charged properly
    assert busd_token.balanceOf(treasury) == original_treasury_balance + (
        exact_output_bnb * 0.08
    )
    assert token.balanceOf(vault) - (
        original_vault_token_balance + (0.05 * max_tokens)
    ) < Web3.toWei(1, "gwei")
