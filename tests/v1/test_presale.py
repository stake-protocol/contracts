from brownie import accounts, TestToken, TokenPresale, reverts, chain
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    token = TestToken.deploy({"from": owner})
    presale = TokenPresale.deploy({"from": owner})
    return owner, token, presale


def test_buy(setup):
    owner, token, presale = setup
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]

    chain.mine(10, 1651489200)

    presale.buyToken({"from": user1, "value": Web3.toWei(1, "ether")})
    presale.buyToken({"from": user2, "value": Web3.toWei(0.5, "ether")})

    assert presale.userInfo(user1).dict().get("bought") == Web3.toWei(1, "ether")
    assert presale.userInfo(user2).dict().get("bought") == Web3.toWei(0.5, "ether")
    with reverts("Only intervals of 0.01 BNB"):
        presale.buyToken({"from": user3, "value": Web3.toWei(0.511, "ether")})


def test_claim(setup):
    owner, token, presale = setup
    user1 = accounts[1]
    user2 = accounts[2]
    chain.mine(10, 1651489200)

    presale.buyToken({"from": user1, "value": Web3.toWei(1, "ether")})

    chain.mine(30, 1651489200 + (24 * 3600) + 10)

    token.transfer(presale, Web3.toWei(39325, "ether"), {"from": owner})

    with reverts("Token not yet available"):
        presale.claimToken({"from": user1})

    presale.setToken(token, {"from": owner})

    with reverts("Already claimed"):
        presale.claimToken({"from": user2})

    presale.claimToken({"from": user1})

    assert token.balanceOf(user1) == Web3.toWei(525, "ether")

    assert token.balanceOf(presale) == Web3.toWei(39325 - 525, "ether")

    # TEST WITHDRAW
    ownerBalance = owner.balance()

    presale.withdraw({"from": owner})
    assert owner.balance() == ownerBalance + Web3.toWei(1, "ether")
