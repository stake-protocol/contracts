from brownie import (
    accounts,
    chain,
    StakeLP2,
    StakeToken,
    StakeVault,
    TokenLocker,
    Foundation,
    TestToken,
    NerdFaucetV2,
    FlameToken,
)
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    treasury = accounts[1]
    vault = accounts[2]
    prefoundation = accounts[3]
    user1 = accounts[4]
    user2 = accounts[5]
    user3 = accounts[6]
    user4 = accounts[7]
    paybackWallet = accounts[8]

    busd = TestToken.deploy({"from": owner})
    token = StakeToken.deploy(vault, {"from": owner})
    vault = StakeVault.deploy(token, {"from": owner})
    flame = FlameToken.deploy(user1, {"from": owner})
    faucet = NerdFaucetV2.deploy(token, vault, flame, user4, owner, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    # Add funds to vault
    token.transfer(vault, Web3.toWei(10000, "ether"), {"from": owner})
    # ADD LIABILITY TO FAUCET
    token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": owner})
    faucet.deposit(Web3.toWei(1000, "ether"), owner, {"from": user1})
    # Deploy fountain LP
    fountain = StakeLP2.deploy(
        token.address, treasury, vault, prefoundation, busd, {"from": owner}
    )
    # Adding liquidity as owner for the first time. 60 BNB and 21000 STAKE.
    lock = TokenLocker.deploy(fountain.address, {"from": owner})
    fountain.setLock(lock.address, {"from": accounts[0]})
    # DEPLOY FOUNDATION
    foundation = Foundation.deploy(
        vault, token, busd, paybackWallet, faucet, {"from": owner}
    )
    foundation.setPolData(fountain, lock, {"from": owner})
    fountain.setFoundation(foundation, {"from": owner})
    for account in accounts:
        busd.transfer(account, Web3.toWei(100, "ether"), {"from": owner})
        busd.approve(foundation, Web3.toWei(1000, "ether"), {"from": account})

    fountain.addAddressToWhitelist(foundation, {"from": owner})
    vault.addWhitelist(foundation, {"from": owner})
    token.excludeMultiple([fountain, foundation], True, True)
    busd.approve(fountain, Web3.toWei(100000, "ether"), {"from": owner})
    token.approve(fountain.address, Web3.toWei(100000, "ether"), {"from": owner})
    fountain.addLiquidity(
        Web3.toWei(2000, "ether"),  # Tokens to emit
        Web3.toWei(21000, "ether"),  # STAKE to send
        Web3.toWei(1000, "ether"),  # BUSD to pair with
        {"from": owner},
    )

    return (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
        paybackWallet,
        faucet,
    )


def test_addLP(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
    ) = setup
    init_bal = busd.balanceOf(user1)
    init_lp = fountain.totalSupply()
    lock_time = lock.unlockTime() + 1
    init_lock_bal = fountain.balanceOf(lock)
    chain.mine(10, timedelta=3600)
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    created_lp = fountain.totalSupply() - init_lp
    # Extracted BUSD
    assert init_bal - Web3.toWei(1, "ether") == busd.balanceOf(user1)
    # Variables updated correctly remove the least significant digits
    assert int(foundation.totalStaked() / 1000) == int(created_lp * 92 / 95 / 1000)
    assert int(foundation.totalSingle() / 1000) == int(0)
    assert int(foundation.totalDouble() / 1000) == int(created_lp * 92 / 95 / 1000)
    assert int(fountain.balanceOf(lock) / 1000) == int(created_lp * 3 / 95 / 1000)
    assert lock.unlockTime() > lock_time
    # only added full LP
    user_data = foundation.investment(user1).dict()
    assert user_data.get("lpSingle") == 0
    # First deposit gets nothing
    assert user_data.get("debt") == 0
    assert foundation.accRewardsPerShare() == 0
    assert foundation.providers() == 1

    # SECOND DEPOSIT
    init_lp = fountain.totalSupply()

    foundation.deposit(Web3.toWei(1, "ether"), {"from": user2})

    created_lp = fountain.totalSupply() - init_lp

    assert foundation.accRewardsPerShare() == int(
        created_lp * 2 * foundation.PRECISION_FACTOR() / 95
    ) / (user_data.get("lpDouble"))

    assert (
        int(created_lp * 2 / 95) * 0.999999999
        <= foundation.pendingRewards(user1)
        <= int(created_lp * 2 / 95) * 1.0000001
    )


def test_correct_reward_distribution(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
    ) = setup
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    assert (
        fountain.balanceOf(foundation)
        == foundation.totalStaked() + foundation.pendingToDistribute()
    )

    assert foundation.pendingToDistribute() == 0
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user2})
    assert (
        fountain.balanceOf(foundation)
        == foundation.totalStaked() + foundation.pendingToDistribute()
    )

    assert foundation.pendingToDistribute() == 0
    assert foundation.providers() == 2

    init_lock = fountain.balanceOf(lock)

    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    assert (
        fountain.balanceOf(foundation)
        == foundation.totalStaked() + foundation.pendingToDistribute()
    )

    assert foundation.pendingToDistribute() == 0
    assert foundation.providers() == 2

    fountain.transfer(foundation, Web3.toWei(1, "ether"), {"from": owner})

    assert foundation.pendingToDistribute() == Web3.toWei(1, "ether")
    # tested locks as well here. All good


def test_external_compound(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
    ) = setup
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user2})

    user1_pending = foundation.pendingRewards(user1)
    assert foundation.providers() == 1
    user2_pending = foundation.pendingRewards(user2)
    assert foundation.providers() == 2
    foundation.compoundReward({"from": user1})
    foundation.compoundReward({"from": user2})
    user1_pending2 = foundation.pendingRewards(user1)
    user2_pending2 = foundation.pendingRewards(user1)
    assert user1_pending2 == 0
    assert user2_pending2 == 0
    assert (
        int(user1_pending * 0.95) * 0.99999999999
        <= foundation.investment(user1).dict().get("rewarded")
        <= int(user1_pending * 0.95) * 1.00000000001
    )
    assert (
        int(user2_pending * 0.95) * 0.99999999999
        <= foundation.investment(user2).dict().get("rewarded")
        <= int(user2_pending * 0.95) * 1.00000000001
    )


def test_external_claim(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
    ) = setup
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user2})

    user1_pending = foundation.pendingRewards(user1)
    user2_pending = foundation.pendingRewards(user2)
    foundation.claimReward({"from": user1})
    foundation.claimReward({"from": user2})
    user1_pending2 = foundation.pendingRewards(user1)
    user2_pending2 = foundation.pendingRewards(user1)
    assert user1_pending2 == 0
    assert user2_pending2 == 0
    assert (
        int(user1_pending * 0.90) * 0.99999999999
        <= foundation.investment(user1).dict().get("rewarded")
        <= int(user1_pending * 0.90) * 1.00000000001
    )
    assert (
        int(user2_pending * 0.90) * 0.99999999999
        <= foundation.investment(user2).dict().get("rewarded")
        <= int(user2_pending * 0.90) * 1.00000000001
    )


def test_withdraw(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        fountain,
        lock,
        busd,
    ) = setup
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user1})
    foundation.deposit(Web3.toWei(1, "ether"), {"from": user2})
    u1_balance = busd.balanceOf(user1)

    user1_data = foundation.investment(user1).dict()

    bnb_per_lp = busd.balanceOf(fountain) / fountain.totalSupply()
    user_current = user1_data.get("lpDouble") * bnb_per_lp

    foundation.withdraw(int(user1_data.get("lpDouble") / 2), {"from": user1})
    withdraw_data = foundation.investment(user1).dict()
    assert foundation.pendingToDistribute() == 0
    assert (
        int(busd.balanceOf(user1) * 0.99999)
        <= int(u1_balance + (user_current * 0.9 / 2) + withdraw_data.get("claimed"))
        <= int(busd.balanceOf(user1) * 1.00001)
    )
