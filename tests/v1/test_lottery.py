from multiprocessing import dummy
from brownie import (
    LotteryV2,
    TestToken,
    accounts,
    StakeLP2,
    TokenLocker,
    chain,
    MockVRFCoordinator,
    reverts,
    DummyFaucet,
)
import pytest
from web3 import Web3 as web3
from datetime import datetime


@pytest.fixture
def setup():
    owner = accounts[0]
    busd = TestToken.deploy("TBUSD", "TEST BUSD", {"from": owner})
    stake = TestToken.deploy("TSTAKE", "TEST STAKE", {"from": owner})
    # test in mainnet
    dummy_faucet = DummyFaucet.deploy(stake, {"from": owner})
    dummy_faucet.deposit(web3.toWei(100, "ether"), accounts[0], {"from": accounts[0]})
    dummy_faucet.deposit(web3.toWei(200, "ether"), accounts[0], {"from": accounts[4]})
    # DEPLOY LP to test with
    treasury = accounts[0]
    vault = accounts[1]
    prefoundation = accounts[2]
    lp = StakeLP2.deploy(
        stake.address, treasury, vault, prefoundation, busd, {"from": owner}
    )
    lock = TokenLocker.deploy(lp.address, {"from": owner})
    lp.setLock(lock.address, {"from": accounts[0]})

    busd.approve(lp, web3.toWei(100000, "ether"), {"from": owner})
    stake.approve(lp.address, web3.toWei(100000, "ether"), {"from": owner})
    lp.addLiquidity(
        web3.toWei(10000, "ether"),  # Tokens to emit
        web3.toWei(20000, "ether"),  # STAKE to send 2 stake/1 busd
        web3.toWei(10000, "ether"),  # BUSD to pair with
    )
    # Deploy Mock Coordinator
    coordinator = MockVRFCoordinator.deploy({"from": owner})
    # Deploy Lottery
    lottery = LotteryV2.deploy(
        stake, lp, busd, 1313, coordinator, dummy_faucet, {"from": owner}
    )
    stake.approve(lottery, web3.toWei(1000000, "ether"), {"from": owner})
    lottery.addToPool(web3.toWei(100, "ether"), {"from": owner})
    lottery.firstStart({"from": owner})
    return lottery, busd, stake, coordinator, dummy_faucet


def test_init_state(setup):
    (lottery, busd, stake, coordinator, dummy) = setup
    assert lottery.currentRound() == 1
    assert lottery.roundInfo(1)["pool"] == web3.toWei(100, "ether")


def test_buy_with_stake(setup):
    (lottery, busd, stake, coordinator, dummy) = setup
    owner = accounts[0]
    user1 = accounts[3]
    user2 = accounts[4]

    stake.transfer(user1, web3.toWei(100, "ether"), {"from": owner})
    stake.approve(lottery, web3.toWei(1000000, "ether"), {"from": user1})
    owner_busd = busd.balanceOf(owner)
    lottery.buyWithToken([1000000], 0, {"from": user1})
    assert stake.balanceOf(user1) < web3.toWei(100, "ether")
    assert (busd.balanceOf(owner) - owner_busd <= web3.toWei(0.5, "ether")) & (
        busd.balanceOf(owner) - owner_busd >= web3.toWei(0.4, "ether")
    )
    print(web3.fromWei(stake.balanceOf(user1), "ether"))
    print(web3.fromWei(busd.balanceOf(owner) - owner_busd, "ether"))
    assert len(lottery.getUserRoundTickets(1, user1)) == 1


def test_buy_with_busd(setup):
    (lottery, busd, stake, coordinator, dummy) = setup
    owner = accounts[0]
    user1 = accounts[3]
    user2 = accounts[4]
    busd.transfer(user1, web3.toWei(100, "ether"), {"from": owner})
    owner_bal = busd.balanceOf(owner)
    busd.approve(lottery, web3.toWei(1000000, "ether"), {"from": user1})
    lottery.buyWithBUSD([1568467, 67890], 0, {"from": user1})
    assert busd.balanceOf(user1) == web3.toWei(90, "ether")
    # The transfered BUSD also adds the 3% treasury fee that is sentfrom selling 4.5BUSD for STAKE
    assert busd.balanceOf(owner) - owner_bal == web3.toWei(
        1 + (4.5 * 2 * 0.03), "ether"
    )
    print(web3.fromWei(busd.balanceOf(user1), "ether"))
    assert len(lottery.getUserRoundTickets(1, user1)) == 2
    assert lottery.userNewTickets(user1, 1)[0] == 1568467
    assert lottery.userNewTickets(user1, 2)[0] == 1067890
    assert lottery.userNewTickets(user1, 0)[0] == 0


def test_next_hour(setup):
    (lottery, busd, stake, coordinator, dummy) = setup

    def t_(t):
        return datetime.fromtimestamp(t)

    currentEnd = lottery.roundEnd()
    # Testing with 2 hours
    # assert False
    # t_(lottery.roundEnd())
    # chain.mine(10, timedelta=24*3600)
    # lottery.calcNextHour()
    # t_(lottery.roundEnd())
    # chain.mine(10, timedelta=12*3600)
    # lottery.calcNextHour()
    # t_(lottery.roundEnd())
    # t_(chain.time())
    pass


def test_user_end(setup):
    (lottery, busd, stake, coordinator, dummy) = setup
    owner = accounts[0]
    user1 = accounts[3]
    user2 = accounts[4]

    busd.transfer(user1, web3.toWei(100, "ether"), {"from": owner})
    busd.transfer(user2, web3.toWei(100, "ether"), {"from": owner})

    busd.approve(lottery, web3.toWei(1000000, "ether"), {"from": user1})
    busd.approve(lottery, web3.toWei(1000000, "ether"), {"from": user2})

    lottery.buyWithBUSD(
        [1000000, 1234567, 1223344, 1987654, 1887766], 0, {"from": user1}
    )
    lottery.buyWithBUSD(
        [1000001, 1214567, 1223344, 1987654, 1887766], 0, {"from": user2}
    )
    r_info = lottery.roundInfo(1)
    assert r_info["pool"] > web3.toWei(110, "ether")
    assert r_info["totalTickets"] == 10
    with reverts("RA"):
        lottery.endRound({"from": user1})
    endTime = lottery.roundEnd()

    chain.mine(10, timestamp=endTime + 100)
    lottery.endRound({"from": user2})
    assert lottery.currentIsActive() == False
    coordinator.fulfill(9746000001, {"from": owner, "gas_limit": 700_000})
    assert lottery.currentIsActive()
    assert (
        lottery.roundInfo(1)["winnerNumber"] == 1214721
    )  # 9746000001 gets it's lower bits to form the uint24 value so the winner is dictated by bits, not actual number
    win_holders = lottery.getWinnerHolders(1)
    assert win_holders[0] == 3  # 1
    assert win_holders[1] == 0  # 2
    assert win_holders[2] == 1  # 3
    assert win_holders[3] == 0  # 4
    assert win_holders[4] == 0  # 5
    assert win_holders[5] == 0  # jackpot
    assert lottery.roundInfo(1)["totalWinners"] == 4
    assert lottery.roundInfo(1)["totalTickets"] == 10

    u1_stake = stake.balanceOf(user1)
    u2_stake = stake.balanceOf(user2)

    lottery.claimAllPendingTickets(
        [[1, 3, 2]],
        [2, 3],
        [1, 1],
        True,
        {"from": user1},
    )
    assert u1_stake < stake.balanceOf(user1)
    lottery.claimAllPendingTickets(
        [[1, 3, 2]],
        [2, 3],
        [3, 1],
        True,
        {"from": user2},
    )
    assert lottery.roundInfo(2)["pool"] / lottery.roundInfo(1)["pool"] == 0.824
    assert u2_stake - stake.balanceOf(user2) == 0
    assert dummy.userStat(user1)["airdrops_rcv"] == 0
    assert dummy.userStat(user2)["airdrops_rcv"] > 0
    assert stake.balanceOf(dummy) == dummy.userStat(user2)["airdrops_rcv"]


def test_buy_partner(setup):
    (lottery, busd, stake, coordinator, dummy) = setup
    owner = accounts[0]
    user1 = accounts[3]
    user2 = accounts[4]

    busd.transfer(user1, web3.toWei(100, "ether"), {"from": owner})
    busd.transfer(user2, web3.toWei(100, "ether"), {"from": owner})

    busd.approve(lottery, web3.toWei(1000000, "ether"), {"from": user1})
    busd.approve(lottery, web3.toWei(1000000, "ether"), {"from": user2})

    lottery.editPartner(user2, 10, {"from": owner})

    assert lottery.getProviderId(user2) == 1
    init_busd_bal = busd.balanceOf(user2)
    lottery.buyWithBUSD([1000000], 1, {"from": user1})
    lottery.buyWithBUSD([1000000], 2, {"from": user1})
    assert busd.balanceOf(user2) - init_busd_bal == web3.toWei(0.05, "ether")
    lottery.editPartner(user2, 0, {"from": owner})
    assert lottery.getProviderId(user2) == 0
    pass


# TODO test claim multiple rounds!!!
