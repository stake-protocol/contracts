from brownie import (
    accounts,
    chain,
    reverts,
    StakeLP2,
    StakeToken,
    StakeVault,
    TokenLocker,
    Foundation,
    TestToken,
    NerdFaucetV2,
    FlameToken,
)
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    treasury = accounts[1]
    vault = accounts[2]
    prefoundation = accounts[3]
    user1 = accounts[4]
    user2 = accounts[5]
    user3 = accounts[6]
    user4 = accounts[7]
    paybackWallet = accounts[8]

    busd = TestToken.deploy("TBUSD", "test busd", {"from": owner})
    token = StakeToken.deploy(vault, {"from": owner})
    vault = StakeVault.deploy(token, {"from": owner})
    flame = FlameToken.deploy(user1, {"from": owner})
    faucet = NerdFaucetV2.deploy(token, vault, flame, user4, owner, {"from": owner})
    token.updateVault(vault, True, {"from": owner})
    # Add funds to vault
    token.transfer(vault, Web3.toWei(1000, "ether"), {"from": owner})
    # ADD LIABILITY TO FAUCET
    token.transfer(user1, Web3.toWei(10000, "ether"), {"from": owner})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": owner})
    vault.addWhitelist(faucet, {"from": owner})
    token.approve(faucet, Web3.toWei(1000, "ether"), {"from": user1})
    faucet.deposit(Web3.toWei(1000, "ether"), owner, {"from": user1})
    # Deploy lp LP
    lp = StakeLP2.deploy(
        token.address, treasury, vault, prefoundation, busd, {"from": owner}
    )
    # Adding liquidity as owner for the first time. 60 BNB and 21000 STAKE.
    lock = TokenLocker.deploy(lp.address, {"from": owner})
    lp.setLock(lock.address, {"from": accounts[0]})
    # DEPLOY FOUNDATION
    foundation = Foundation.deploy(
        vault, token, busd, paybackWallet, faucet, {"from": owner}
    )
    foundation.setPolData(lp, lock, {"from": owner})
    lp.setFoundation(foundation, {"from": owner})
    # All accounts have approved the foundations for "BUSD" and posses 100 "BUSD"
    for account in accounts:
        if account == paybackWallet:
            continue
        busd.transfer(account, Web3.toWei(1000, "ether"), {"from": owner})
        busd.approve(foundation, Web3.toWei(2000, "ether"), {"from": account})

    lp.addAddressToWhitelist(foundation, {"from": owner})
    vault.addWhitelist(foundation, {"from": owner})
    token.excludeMultiple([lp, foundation], True, True)
    busd.approve(lp, Web3.toWei(100000, "ether"), {"from": owner})
    token.approve(lp.address, Web3.toWei(100000, "ether"), {"from": owner})
    lp.addLiquidity(
        Web3.toWei(12700, "ether"),  # Tokens to emit
        Web3.toWei(1072, "ether"),  # STAKE to send
        Web3.toWei(12700, "ether"),  # BUSD to pair with
        {"from": owner},
    )

    return (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    )


def test_payback(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    lp.approve(foundation, Web3.toWei(10_000, "ether"), {"from": owner})

    foundation.setUser(
        user1,
        Web3.toWei(200, "ether"),
        Web3.toWei(100, "ether"),
        Web3.toWei(250, "ether"),
        Web3.toWei(300, "ether"),
        {"from": owner},
    )
    with reverts("Set"):
        foundation.setUser(
            user1,
            Web3.toWei(200, "ether"),
            Web3.toWei(100, "ether"),
            Web3.toWei(250, "ether"),
            Web3.toWei(300, "ether"),
            {"from": owner},
        )
    with reverts("=0"):
        foundation.setUser(user2, 0, 0, 0, 0, {"from": owner})

    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})

    assert foundation.pendingRewards(user1) > 0
    foundation.compoundReward({"from": user1})
    foundation.setUser(
        user3,
        0,
        Web3.toWei(1000, "ether"),
        Web3.toWei(20, "ether"),
        Web3.toWei(0, "ether"),
        {"from": owner},
    )
    assert foundation.pendingRewards(user3) == 0
    assert foundation.pendingRewards(user1) == 0


def test_single_deposit(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    initLpSupply = lp.totalSupply()
    assert foundation.totalStaked() == lp.balanceOf(foundation)
    # USER1 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    # CHECK:
    supplyDiff = lp.totalSupply() - initLpSupply
    assert supplyDiff > 0
    # LP LOCKED IS 3/95 OF CREDITED TO USER1 since only 95% is passed as LP
    assert abs(supplyDiff * 3 / 95 - lp.balanceOf(lock)) < 1000
    # LP CREDITED TO USER1 IS THE AMOUNT THE FOUNDATION HAS
    assert foundation.totalStaked() == lp.balanceOf(foundation)
    assert supplyDiff - lp.balanceOf(lock) == lp.balanceOf(foundation)
    # We do 92/95 since the 2% is not rewarded since this user is the first to deposit
    assert (
        abs((supplyDiff * 92 / 95) - foundation.investment(user1).dict()["lpDouble"])
        < 100
    )
    # 1% OF BUSD DEPOSITED IS NOW HELD BY PAYBACKWALLET
    assert busd.balanceOf(paybackWallet) == Web3.toWei(1, "ether")
    # ACCREWARDS PER SHARE IS 0 (SINCE IT'S FIRST USER)
    assert foundation.pendingRewards(user1) == 0
    # PROVIDERS NOW == 1
    assert foundation.providers() == 1
    pass


def test_multiple_deposit_and_compound(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    # USER1 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    # USER2 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    # CHECK:
    # USER1 PENDING REWARDS = 2/90 OF USER2 AMOUNT
    user1_1st_deposit = foundation.investment(user1).dict()
    user1_reward = foundation.pendingRewards(user1)
    assert (
        abs(
            foundation.pendingRewards(user1)
            - foundation.investment(user2).dict()["lpDouble"] * 2 / 90
        )
        < 100000000
    )
    # PROVIDERS NOW == 2
    assert foundation.providers() == 2
    # USER1 DEPOSITS
    initLp = lp.totalSupply()
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    supplyAdded = lp.totalSupply() - initLp
    # USER1 COMPOUNDED
    user1_investment = foundation.investment(user1).dict()
    assert abs(user1_investment["rewarded"] - user1_reward * 95 / 100) < 1000000
    # USER1 PENDING REWARDS IS ZERO
    assert foundation.pendingRewards(user1) == 0
    # USER 1 CREDITED IS: PREVIOUS PENDING REWARD * 0.95 + SECOND DEPOSIT
    assert (
        abs(
            user1_investment["lpDouble"]
            - user1_1st_deposit["lpDouble"]
            - ((user1_reward * 95 / 100) + (supplyAdded * 90 / 95))
        )
        < 100000000
    )
    # USER 1 COMPOUNDS AGAIN, NOTHING HAPPENS
    tx = foundation.compoundReward({"from": user1})
    assert tx.events["AuditLog"][0]["_description"] == "Nothing to Compound"
    assert (
        foundation.investment(user1).dict()["lpDouble"] == user1_investment["lpDouble"]
    )
    pass


def test_claim(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    # USER1 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    # USER2 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    # CHECK:
    user1_busd = busd.balanceOf(user1)
    user1_pending = foundation.pendingRewards(user1)
    lock_balance = lp.balanceOf(lock)
    # USER1 CLAIMS
    claimTx = foundation.claimReward({"from": user1})
    assert foundation.totalStaked() == lp.balanceOf(foundation)

    assert len(claimTx.events["Claimed"]) == 1
    print(
        ""
        + str(Web3.fromWei(claimTx.events["Claimed"][0]["_lp_amount"], "ether"))
        + " - "
        + str(Web3.fromWei(claimTx.events["Claimed"][0]["_bnb"], "ether"))
    )
    # USER2 PENDING REWARDS  = 2% OF USER1 PENDING REWARDS
    user2_pending = foundation.pendingRewards(user2)
    assert abs(user2_pending - user1_pending * 2 / 100) < 100000000
    # LOCK LP AMOUNT IS INCREASED BY 3% OF USER1 PENDING REWARDS
    assert abs(lp.balanceOf(lock) - lock_balance - user1_pending * 3 / 100) < 1000000
    # USER1 BUSD AMOUNT INCREASES.
    user1_busd_2 = busd.balanceOf(user1)
    assert user1_busd_2 - user1_busd == claimTx.events["Claimed"][0]["_bnb"]
    # USER1 CLAIMS AGAIN, NOTHING HAPPENS
    claimTx = foundation.claimReward({"from": user1})
    assert busd.balanceOf(user1) == user1_busd_2
    assert claimTx.events["AuditLog"][0]["_description"] == "Nothing to Claim"
    pass


def test_withdraw_one_user(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    # USER 1 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    # USER 2 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    # USER 1 WITHDRAWS ALL
    user1_investment = foundation.investment(user1)
    withdraw_tx = foundation.withdraw(user1_investment["lpDouble"], {"from": user1})
    assert foundation.totalStaked() == lp.balanceOf(foundation)

    user1_w_investment = foundation.investment(user1)
    assert (user1_w_investment["lpDouble"] == 0) & (user1_w_investment["lpSingle"] == 0)

    claim_tx = foundation.claimReward({"from": user1})
    assert claim_tx.events["AuditLog"][0]["_description"] == "Nothing to Claim"
    compound_tx = foundation.compoundReward({"from": user1})
    assert compound_tx.events["AuditLog"][0]["_description"] == "Nothing to Compound"
    # PROVIDERS == 1
    assert foundation.providers() == 1
    # USER 1 DEPOSITS AGAIN
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    # PROVIDERS == 2
    assert foundation.providers() == 2
    # USER 1 DEPOSITS AGAIN
    foundation.deposit(Web3.toWei(130, "ether"), {"from": user1})
    assert foundation.pendingRewards(user1) == 0
    # PROVIDERS == 2
    assert foundation.providers() == 2
    pass


def test_distribute_rewards(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    # USER 1 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})
    assert foundation.totalStaked() == lp.balanceOf(foundation)
    # USER 2 DEPOSITS
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    assert foundation.totalStaked() == lp.balanceOf(foundation)
    # USER 1 COMPOUNDS
    foundation.compoundReward({"from": user1})
    assert (
        foundation.pendingToDistribute()
        == 0 & foundation.pendingRewards(user1)
        == 0 & foundation.pendingRewards(user2)
    )
    assert foundation.totalStaked() == lp.balanceOf(foundation)
    # LP IS SENT TO FOUNDATION
    # ADD LP
    prev_lp = lp.balanceOf(owner)
    lp.addLiquidity(
        1,  # Tokens to obtain
        Web3.toWei(130, "ether"),  # STAKE to send
        Web3.toWei(1200, "ether"),  # BUSD to pair with
        {"from": owner},
    )
    owner_created_supply = lp.balanceOf(owner) - prev_lp
    lp.transfer(foundation, owner_created_supply, {"from": owner})
    # PENDING TO DISTRIBUTE  = AMOUNT SENT
    assert foundation.pendingToDistribute() == owner_created_supply
    # DISTRIBUTE PENDING
    assert foundation.distributePending({"from": user3})

    total_double = foundation.totalDouble()
    total_single = foundation.totalSingle()
    # USER1 PENDING REWARDS IS % OF AMOUNTS DISTRIBUTED
    user1_invest = foundation.investment(user1)
    assert (
        abs(
            foundation.pendingRewards(user1)
            - owner_created_supply
            * (user1_invest["lpDouble"] + 2 * user1_invest["lpSingle"])
            / (total_double + (total_single * 2))
        )
        < 100000000
    )
    # USER2 PENDING REWARDS IS % OF AMOUNTS DISTRIBUTED
    user2_invest = foundation.investment(user2)
    assert (
        abs(
            foundation.pendingRewards(user2)
            - owner_created_supply
            * (user2_invest["lpDouble"] + 2 * user2_invest["lpSingle"])
            / (total_double + (total_single * 2))
        )
        < 100000000
    )
    pass


def test_lpSingle_increase(setup):
    (
        owner,
        foundation,
        treasury,
        vault,
        prefoundation,
        user1,
        user2,
        user3,
        token,
        lp,
        lock,
        busd,
        paybackWallet,
        faucet,
    ) = setup

    # User 1 Deposit
    foundation.deposit(Web3.toWei(100, "ether"), {"from": user1})

    # Decrease vault value so that depositing forces the use of lp Single
    vault.addWhitelist(user3, {"from": owner})
    # User 2 deposit, check that lpSingle is added
    init_lp = lp.totalSupply()
    vault.withdraw(Web3.toWei(2990, "ether"), {"from": user3})

    foundation.deposit(Web3.toWei(100, "ether"), {"from": user2})
    created_lp = lp.totalSupply() - init_lp
    user2_invest = foundation.investment(user2)
    # check that lpDouble is never increased
    assert user2_invest["lpDouble"] == 0
    # Check that lpSingle is added
    assert user2_invest["lpSingle"] - int(created_lp * 90 / 95) < 100000
    # We use compound since that doesn't generate extra rewards
    foundation.compoundReward({"from": user1})

    reward_lp = int(lp.balanceOf(owner) * 0.01)
    lp.transfer(foundation, reward_lp, {"from": owner})
    assert foundation.pendingToDistribute() == reward_lp

    foundation.distributePending({"from": user3})

    total_double = foundation.totalDouble()
    total_single = foundation.totalSingle()
    # USER1 PENDING REWARDS IS % OF AMOUNTS DISTRIBUTED
    user1_invest = foundation.investment(user1)
    assert (
        abs(
            foundation.pendingRewards(user1)
            - int(
                reward_lp
                * (user1_invest["lpDouble"] + 2 * user1_invest["lpSingle"])
                / (total_double + (total_single * 2))
            )
        )
        < 100000000
    )
    # USER2 PENDING REWARDS IS % OF AMOUNTS DISTRIBUTED
    user2_invest = foundation.investment(user2)
    assert (
        abs(
            foundation.pendingRewards(user2)
            - int(
                reward_lp
                * (user2_invest["lpDouble"] + 2 * user2_invest["lpSingle"])
                / (total_double + (total_single * 2))
            )
        )
        < 100000000
    )

    # NOW TEST WITHDRAW
    # We need to claim first (so that we force withdraw from single LP)
    foundation.claimReward({"from": user2})
    assert foundation.pendingRewards(user2) == 0

    # Withdraw 10%
    single_u2_withdraw = int(user2_invest["lpSingle"])
    foundation.withdraw(single_u2_withdraw, {"from": user2})
    # Check withdraw amount was correct
    user2_post = foundation.investment(user2)
    assert user2_invest["lpSingle"] - single_u2_withdraw == user2_post["lpSingle"]
    pass
