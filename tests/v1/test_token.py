from brownie import accounts, StakeToken, reverts
from web3 import Web3
import pytest


@pytest.fixture
def setup():
    owner = accounts[0]
    vault = accounts[5]  # set the vault as an initial
    token = StakeToken.deploy(vault, {"from": owner})
    # token.excludeAddress(owner, True, False, {"from": owner})
    return owner, token, vault


def test_owner_balance(setup):
    owner, token, _ = setup
    assert token.balanceOf(owner) == Web3.toWei(750000, "ether")


def test_transfer(setup):
    owner, token, vault = setup

    user1 = accounts[1]

    assert token.balanceOf(user1) == 0
    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    assert token.balanceOf(user1) == Web3.toWei(900, "ether")
    assert token.balanceOf(vault) == Web3.toWei(100, "ether")


def test_token_excluded_transfer(setup):
    owner, token, vault = setup

    user1 = accounts[1]
    token.excludeAddress(owner, False, False, {"from": owner})
    assert token.balanceOf(user1) == 0
    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    assert token.balanceOf(user1) == Web3.toWei(1000, "ether")
    assert token.balanceOf(vault) == Web3.toWei(0, "ether")


def test_receiver_excluded(setup):
    owner, token, vault = setup

    user1 = accounts[1]
    user2 = accounts[2]

    token.excludeAddress(user1, False, True, {"from": owner})

    assert token.balanceOf(user1) == 0
    token.transfer(user1, Web3.toWei(1000, "ether"), {"from": owner})
    token.transfer(user2, Web3.toWei(1000, "ether"), {"from": owner})
    assert token.balanceOf(user1) == Web3.toWei(1000, "ether")
    assert token.balanceOf(user2) == Web3.toWei(900, "ether")
    assert token.balanceOf(vault) == Web3.toWei(100, "ether")


def test_whitelister(setup):
    owner, token, _ = setup

    user1 = accounts[1]

    with reverts("Not Whitelist"):
        token.mint(Web3.toWei(1000, "ether"), {"from": user1})

    token.editWhitelist(user1, True, {"from": owner})
    token.mint(Web3.toWei(1500, "ether"), {"from": user1})

    assert token.balanceOf(user1) == Web3.toWei(1500, "ether")
