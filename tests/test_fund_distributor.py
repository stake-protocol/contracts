from brownie import reverts
from pytest import fixture


@fixture(scope="module", autouse=True)
def setup(TestToken, FundDistributor, accounts):
    dev = accounts[0]
    busd = TestToken.deploy("BUSD", "Binance USD", {"from": dev})
    fd = FundDistributor.deploy(busd, {"from": dev})
    return dev, busd, fd


def test_set_values(setup, accounts):
    dev, busd, fd = setup
    assert fd.treasury() == "0x0000000000000000000000000000000000000000"
    assert fd.pff() == "0x0000000000000000000000000000000000000000"
    assert fd.totalTeammates() == 0
    assert fd.totalPortion() == 0

    with reverts("Not Owner"):
        fd.setTreasury(accounts[1], {"from": accounts[1]})

    fd.setTreasury(accounts[1], {"from": dev})
    assert fd.treasury() == accounts[1]
    fd.setPriceFloorProtection(accounts[2], {"from": dev})
    assert fd.pff() == accounts[2]

    fd.setTeammate(accounts[3], 1, {"from": dev})
    assert fd.teammates(0) == accounts[3]
    assert fd.totalTeammates() == 1
    assert fd.totalPortion() == 1


def test_distribute_funds(setup, web3, accounts):
    dev, busd, fd = setup
    busd.approve(fd, web3.toWei(100, "ether"), {"from": dev})
    fd.distributeFunds(web3.toWei(100, "ether"), 2, 5, 3, {"from": dev})
    assert busd.balanceOf(accounts[1]) == web3.toWei(20, "ether")
    assert busd.balanceOf(accounts[2]) == web3.toWei(50, "ether")
    assert busd.balanceOf(accounts[3]) == web3.toWei(30, "ether")
    pass


def test_distribute_multiple_team(setup, web3, accounts):
    dev, busd, fd = setup
    fd.setTeammate(accounts[4], 1, {"from": dev})
    fd.setTeammate(accounts[3], 2, {"from": dev})
    assert fd.totalPortion() == 3
    busd.approve(fd, web3.toWei(100, "ether"), {"from": dev})
    fd.distributeFunds(web3.toWei(100, "ether"), 2, 5, 3, {"from": dev})
    assert busd.balanceOf(accounts[4]) == web3.toWei(10, "ether")
    # Accounts 3 had already 30 from the previous distribution so 30 + 20
    assert busd.balanceOf(accounts[3]) == web3.toWei(50, "ether")
    assert busd.balanceOf(accounts[1]) == web3.toWei(40, "ether")
    assert busd.balanceOf(accounts[2]) == web3.toWei(100, "ether")
