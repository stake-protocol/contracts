from brownie import StakeTokenV2, accounts
from web3 import Web3 as web3
from pytest import fixture


@fixture
def setup():
    dev = accounts[0]
    token = StakeTokenV2.deploy({"from": dev})
    return token, dev


def test_scenario(setup):
    pass
