from brownie import (
    accounts,
    chain,
    POLv2,
    interface,
    StakeTokenV2,
    TestToken,
    FoundationV6,
    FundDistributor,
    reverts,
)
from pytest import fixture


@fixture(scope="module", autouse=True)
def setup(web3):
    dev = accounts[0]
    stake = StakeTokenV2.deploy({"from": dev})
    busd = TestToken.deploy("BUSD", "Binance USD", {"from": dev})
    pol = POLv2.deploy(stake, busd, {"from": dev})
    pol.setLiquidityLock(accounts[6], {"from": dev})
    stake.changeExcludeStatus(pol, True, {"from": dev})

    for wallet in accounts:
        if wallet == dev:
            continue
        busd.transfer(wallet, web3.toWei(1_000, "ether"), {"from": dev})
        stake.transfer(wallet, web3.toWei(1_000, "ether"), {"from": dev})

    busd.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})

    pol.addLiquidity(
        web3.toWei(1000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )
    # Create Fund Distributor
    fd = FundDistributor.deploy(busd, {"from": dev})
    fd.setTreasury(accounts[1], {"from": dev})
    fd.setPriceFloorProtection(accounts[2], {"from": dev})
    fd.setTeammate(accounts[3], 1, {"from": dev})
    # Create test foundation
    locker = accounts[4]
    fdt = FoundationV6.deploy(busd, stake, pol, locker, {"from": dev})
    stake.changeExcludeStatus(fdt, True, {"from": dev})
    pol.setWhitelistStatus(fdt, True, {"from": dev})
    pol.setFoundation(fdt, {"from": dev})
    pol.setFundDistributor(fd, {"from": dev})

    return dev, stake, busd, pol, fd, fdt, locker


def test_init_deposit(setup, web3):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]

    lp_supply = pol.totalSupply()
    deposit_amount = web3.toWei(100, "ether")
    # amount taken in BUSD is withdrawn from user
    with reverts("ERC20: insufficient allowance"):
        fdt.deposit(deposit_amount, {"from": user1})
    busd.approve(fdt, deposit_amount, {"from": user1})
    fdt.deposit(deposit_amount, {"from": user1})
    assert busd.balanceOf(user1) == web3.toWei(900, "ether")
    user_info = fdt.shares(user1)
    assert user_info["deposits"] == deposit_amount
    # user shares are equal to the amount of LP they are providing - 3% of tax
    assert user_info["shares"] + pol.balanceOf(locker) == (
        pol.totalSupply() - lp_supply
    )
    assert pol.balanceOf(fdt) + pol.balanceOf(locker) == (pol.totalSupply() - lp_supply)
    pass


def test_subsequent_deposits(setup, web3):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]
    user2 = accounts[6]
    user3 = accounts[7]
    deposit_amount = web3.toWei(100, "ether")

    busd.approve(fdt, deposit_amount, {"from": user2})
    busd.approve(fdt, deposit_amount, {"from": user3})

    # Test that new deposits increase shares
    fdt.deposit(deposit_amount, {"from": user2})
    u1_info = fdt.shares(user1)
    u2_info = fdt.shares(user2)
    assert fdt.totalShares() == u1_info["shares"] + u2_info["shares"]
    fdt.deposit(deposit_amount // 2, {"from": user3})
    u3_info = fdt.shares(user3)
    assert (
        fdt.totalShares() == u1_info["shares"] + u2_info["shares"] + u3_info["shares"]
    )
    pass


def test_distribution(setup, web3):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]
    user2 = accounts[6]
    user3 = accounts[7]
    # test that tokens are distributed according to share amount.
    dev_bal = busd.balanceOf(dev)
    distribute_amount = web3.toWei(100, "ether")
    busd.approve(fdt, distribute_amount, {"from": dev})
    fdt.distribute(distribute_amount, {"from": dev})
    assert dev_bal - busd.balanceOf(dev) == distribute_amount
    assert busd.balanceOf(fdt) == distribute_amount
    # Test that correct amount to claim is shown
    u1_pending = fdt.getPendingRewards(user1)
    u2_pending = fdt.getPendingRewards(user2)
    u3_pending = fdt.getPendingRewards(user3)
    # Due to truncation of math divs there might be a minimum left over
    assert abs(u1_pending + u2_pending + u3_pending - distribute_amount) < 1000
    pass


def test_claim(setup):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]
    user2 = accounts[6]
    user3 = accounts[7]
    init_u1_balance = busd.balanceOf(user1)
    u1_reward = fdt.getPendingRewards(user1)
    u2_reward_i = fdt.getPendingRewards(user2)
    u3_reward_i = fdt.getPendingRewards(user3)
    # Test on claim the debt is updated
    fdt.claim({"from": user1})
    assert busd.balanceOf(user1) - init_u1_balance == u1_reward * 9 // 10
    # Test that claiming USER has no pending rewards
    assert fdt.getPendingRewards(user1) == 0
    # Test that Tax is redistributed
    u2_reward = fdt.getPendingRewards(user2)
    u3_reward = fdt.getPendingRewards(user3)
    # we allow leeway for the 2 least significant digits to vary
    assert (
        abs((u1_reward // 10) - (u2_reward + u3_reward - u2_reward_i - u3_reward_i))
        < 100
    )
    pass


def test_compound(setup, web3, history):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]
    user2 = accounts[6]
    user3 = accounts[7]

    u2_info = fdt.shares(user2)
    u1_reward = fdt.getPendingRewards(user1)
    u2_reward_i = fdt.getPendingRewards(user2)
    u3_reward_i = fdt.getPendingRewards(user3)

    pol_supply = pol.totalSupply()
    fdt_busd = busd.balanceOf(fdt)
    # Test that debt is updated
    fdt.compound({"from": user2})
    assert (
        pol.totalSupply() - pol_supply
        == fdt.shares(user2)["shares"] - u2_info["shares"]
    )
    # Test that amount is entirely converted to LP
    assert fdt_busd - busd.balanceOf(fdt) == u2_reward_i
    # Test auto compounding on deposits
    re_deposit = web3.toWei(50, "ether")
    busd.approve(fdt, re_deposit)
    fdt.deposit(re_deposit, {"from": user3})
    assert fdt.getPendingRewards(user3) == 0
    assert len(history[-1].events["Compound"]) == 1
    assert len(history[-1].events["Deposit"]) == 1
    assert history[-1].events["Deposit"][0]["_amount"] == re_deposit
    pass


def test_withdraw(setup):
    dev, stake, busd, pol, fd, fdt, locker = setup
    user1 = accounts[5]
    user2 = accounts[6]
    user3 = accounts[7]

    polSupply = pol.totalSupply()
    busd_res = busd.balanceOf(pol)
    stake_res = stake.balanceOf(pol)

    u3_info = fdt.shares(user3)
    fdt.withdraw(u3_info["shares"], {"from": user3})
    # 10% of liquidity is locked
    assert abs((polSupply - pol.totalSupply()) - (u3_info["shares"] * 95 // 100)) < 100
    assert False
    pass
