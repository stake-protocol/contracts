from brownie import StakeTokenV2, accounts, reverts
from web3 import Web3 as web3
from pytest import fixture


@fixture
def setup():
    dev = accounts[0]
    token = StakeTokenV2.deploy({"from": dev})
    return token, dev


def test_token_meta(setup):
    token, dev = setup
    assert token.name() == "Stake Protocol V2 Token"
    assert token.symbol() == "STAKE_V2"
    assert token.decimals() == 18


def test_transfer(setup):
    token, dev = setup
    # Test excluded to non
    token.transfer(accounts[1], web3.toWei(100, "ether"), {"from": dev})
    assert token.balanceOf(accounts[1]) == web3.toWei(100, "ether")
    assert token.balanceOf(dev) == web3.toWei(749_900, "ether")
    # test non to non
    token.transfer(accounts[2], web3.toWei(10, "ether"), {"from": accounts[1]})
    assert token.balanceOf(accounts[2]) == web3.toWei(9, "ether")
    assert token.balanceOf(accounts[1]) == web3.toWei(90, "ether")
    #   Check that tax is burned
    assert token.totalSupply() == web3.toWei(749_999, "ether")
    # test non to excluded
    token.transfer(dev, web3.toWei(5, "ether"), {"from": accounts[2]})
    assert token.balanceOf(accounts[2]) == web3.toWei(4, "ether")
    # NO BURN HAPPENS
    assert token.balanceOf(dev) == web3.toWei(749_905, "ether")
    assert token.totalSupply() == web3.toWei(749_999, "ether")
    pass


def test_allowances(setup):
    token, dev = setup
    # test setting approve
    assert token.allowance(dev, accounts[1]) == web3.toWei(0, "ether")
    token.approve(accounts[1], web3.toWei(100, "ether"), {"from": dev})
    # expecting allowance to be 100 ether
    assert token.allowance(dev, accounts[1]) == web3.toWei(100, "ether")
    # test increase allowance
    token.increaseAllowance(accounts[1], web3.toWei(1, "ether"), {"from": dev})
    # allowance of user1 to be 101 ether
    assert token.allowance(dev, accounts[1]) == web3.toWei(101, "ether")
    # test decrease allowance
    token.decreaseAllowance(accounts[1], web3.toWei(2, "ether"), {"from": dev})
    # allowance of user2 to be 99 ether
    assert token.allowance(dev, accounts[1]) == web3.toWei(99, "ether")
    token.approve(accounts[1], 0, {"from": dev})
    assert token.allowance(dev, accounts[1]) == 0
    pass


def test_transfer_from(setup):
    token, dev = setup
    allowedUser = accounts[1]
    with reverts("Insufficient Allowance"):
        token.transferFrom(
            dev, accounts[2], web3.toWei(100, "ether"), {"from": allowedUser}
        )

    token.approve(allowedUser, web3.toWei(100, "ether"), {"from": dev})
    token.transferFrom(dev, accounts[2], web3.toWei(10, "ether"), {"from": allowedUser})
    assert token.balanceOf(accounts[2]) == web3.toWei(10, "ether")

    with reverts("Insufficient Allowance"):
        token.transferFrom(
            dev, accounts[3], web3.toWei(100, "ether"), {"from": allowedUser}
        )

    token.approve(allowedUser, web3.toWei(100, "ether"), {"from": accounts[2]})

    token.transferFrom(
        accounts[2], accounts[3], web3.toWei(5, "ether"), {"from": allowedUser}
    )
    assert token.balanceOf(accounts[2]) == web3.toWei(5, "ether")
    assert token.balanceOf(accounts[3]) == web3.toWei(4.5, "ether")
    assert token.totalSupply() == web3.toWei(749_999.5, "ether")
    pass


def test_mint(setup):
    token, dev = setup

    faucet = accounts[3]
    # test only faucet can mint
    with reverts("Not Faucet"):
        token.mint(accounts[1], web3.toWei(100, "ether"), {"from": dev})
    with reverts("Not Faucet"):
        token.mint(accounts[2], web3.toWei(100, "ether"), {"from": accounts[1]})

    assert "0x0000000000000000000000000000000000000000" == token._faucet()
    with reverts("Not Owner"):
        token.setFaucet(accounts[3], {"from": accounts[1]})

    token.setFaucet(faucet, {"from": dev})
    assert faucet == token._faucet()
    # test transfer of faucet address
    token.mint(accounts[2], web3.toWei(200, "ether"), {"from": faucet})
    assert token.totalSupply() == web3.toWei(750_200, "ether")
    assert token.balanceOf(accounts[2]) == web3.toWei(200, "ether")
    with reverts("Cant be owner"):
        token.setFaucet(dev, {"from": dev})

    pass


def test_burn(setup):
    token, dev = setup
    # test burn from user
    token.transfer(accounts[1], web3.toWei(100, "ether"), {"from": dev})
    token.burn(web3.toWei(50, "ether"), {"from": accounts[1]})
    assert token.totalSupply() == web3.toWei(749_950, "ether")
    assert token.balanceOf(accounts[1]) == web3.toWei(50, "ether")
    # test burn by spender
    with reverts("Insufficient allowance"):
        token.burnFrom(dev, web3.toWei(150, "ether"), {"from": accounts[1]})
    token.approve(accounts[1], web3.toWei(150, "ether"), {"from": dev})
    token.burnFrom(dev, web3.toWei(100, "ether"), {"from": accounts[1]})
    assert token.totalSupply() == web3.toWei(750_000 - 150, "ether")
    assert token.balanceOf(dev) == web3.toWei(750_000 - 200, "ether")
    with reverts("Insufficient allowance"):
        token.burnFrom(dev, web3.toWei(150, "ether"), {"from": accounts[1]})
    pass


def test_var_setup(setup):
    token, dev = setup
    # test transfer ownership
    with reverts("Not Owner"):
        token.transferOwnership(accounts[1], {"from": accounts[2]})
    token.transferOwnership(accounts[1], {"from": dev})
    assert token.owner() == accounts[1]
    assert token.excluded(dev) == False
    # test set exclusion
    with reverts("Not Owner"):
        token.changeExcludeStatus(dev, True, {"from": dev})

    token.transfer(accounts[2], web3.toWei(100, "ether"), {"from": dev})
    assert token.balanceOf(accounts[2]) == web3.toWei(90, "ether")
    # also test that tax is not applied on transfer
    # test renounce ownership
    with reverts("Not Owner"):
        token.renounceOwnership({"from": dev})
    token.renounceOwnership({"from": accounts[1]})
    assert token.owner() == "0x0000000000000000000000000000000000000000"
    pass
