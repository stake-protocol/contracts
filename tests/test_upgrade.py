from brownie import UpgradeToken, interface, TestToken, accounts, reverts, chain
from pytest import fixture


@fixture(scope="module")
def setup(web3):
    dev = accounts[0]
    vault = accounts[1]
    v1 = TestToken.deploy("v1", "v1", {"from": dev})
    v2 = TestToken.deploy("v2", "v2", {"from": dev})
    upg = UpgradeToken.deploy(v1, vault, {"from": dev})

    for wallet in accounts:
        if wallet != dev and wallet != vault:
            v1.transfer(wallet, web3.toWei(1000, "ether"), {"from": dev})

    return dev, vault, upg, v1, v2


def test_send_tokens(setup, web3):
    dev, vault, upg, v1, v2 = setup
    user1 = accounts[2]
    balance = web3.toWei(1000, "ether")
    with reverts("ERC20: insufficient allowance"):
        upg.sendToVault({"from": user1})
    v1.approve(upg, balance, {"from": user1})
    upg.sendToVault({"from": user1})
    assert v1.balanceOf(user1) == 0
    assert v1.balanceOf(vault) == balance
    assert upg.user(user1)["received"] == balance
    assert upg.totalReceived() == balance
    pass


def test_end(setup, web3):
    dev, vault, upg, v1, v2 = setup
    user2 = accounts[3]

    chain.mine(10, timestamp=upg.timeLimit() + 10)
    v1.approve(upg, web3.toWei(1000, "ether"), {"from": user2})

    with reverts("Blocked"):
        upg.sendToVault({"from": user2})
    #  Set new Limit
    upg.setTimeLimit(chain.time() + 3600)
    # with new limit, allows to send to vault
    upg.sendToVault({"from": user2})
    balance = web3.toWei(2000, "ether")
    assert v1.balanceOf(user2) == 0
    assert v1.balanceOf(vault) == balance
    assert upg.user(user2)["received"] == balance / 2
    assert upg.totalReceived() == balance
    pass


def test_set_v2(setup):
    dev, vault, upg, v1, v2 = setup
    user1 = accounts[2]
    with reverts("Not owner"):
        upg.setV2(v2, {"from": accounts[3]})
    upg.setV2(v2, {"from": dev})
    assert upg.v2() == v2
    pass


def test_claim_v2(setup, web3):
    dev, vault, upg, v1, v2 = setup
    user1 = accounts[2]
    v2.transfer(upg, web3.toWei(10_000, "ether"), {"from": dev})

    upg.claim({"from": user1})
    assert v2.balanceOf(user1) == upg.user(user1)["received"]
    assert web3.toWei(1_000, "ether") == upg.user(user1)["claimed"]
    assert upg.user(user1)["hasClaimed"]
    with reverts("Already claimed"):
        upg.claim({"from": user1})
    assert v2.balanceOf(upg) == web3.toWei(9_000, "ether")
    assert upg.totalClaimed() == web3.toWei(1_000, "ether")
    pass
