from brownie import (
    accounts,
    chain,
    POLv2,
    interface,
    reverts,
    StakeTokenV2,
    TestToken,
    FoundationV6,
    FundDistributor,
)
from web3 import Web3 as web3
from pytest import fixture


@fixture
def setup():
    dev = accounts[0]
    stake = StakeTokenV2.deploy({"from": dev})
    busd = TestToken.deploy("BUSD", "Binance USD", {"from": dev})
    pol = POLv2.deploy(stake, busd, {"from": dev})
    pol.setLiquidityLock(accounts[6], {"from": dev})
    stake.changeExcludeStatus(pol, True, {"from": dev})
    busd.transfer(accounts[4], web3.toWei(1000, "ether"), {"from": dev})
    stake.transfer(accounts[5], web3.toWei(1000, "ether"), {"from": dev})
    return dev, stake, busd, pol


def test_pol_token_meta(setup):
    dev, stake, busd, pol = setup
    assert pol.name() == "Stake LP V2"
    assert pol.symbol() == "STOKEv2"
    assert pol.decimals() == 18


def test_addLiquidity(setup):
    dev, stake, busd, pol = setup
    busd.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})

    pol.addLiquidity(
        web3.toWei(1000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )

    assert pol.totalSupply() > 0
    assert pol.balanceOf(dev) == pol.totalSupply()
    assert pol.totalSupply() == web3.toWei(10_000, "ether")
    assert busd.balanceOf(pol) == stake.balanceOf(pol) == web3.toWei(10_000, "ether")


@fixture
def liquidity_setup(setup):
    dev, stake, busd, pol = setup
    busd.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})

    pol.addLiquidity(
        web3.toWei(1000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )
    # Create Fund Distributor
    fd = FundDistributor.deploy(busd, {"from": dev})
    fd.setTreasury(accounts[1], {"from": dev})
    fd.setPriceFloorProtection(accounts[2], {"from": dev})
    fd.setTeammate(accounts[3], 1, {"from": dev})
    # Create test foundation
    fdt = FoundationV6.deploy(busd, stake, pol, accounts[9], {"from": dev})
    pol.setFoundation(fdt, {"from": dev})
    pol.setFundDistributor(fd, {"from": dev})

    return *setup, fdt, fd


def test_remove_liquidity(liquidity_setup):
    dev, stake, busd, pol, fdt, fd = liquidity_setup
    assert pol.totalSupply() == web3.toWei(10_000, "ether")
    b_bal = busd.balanceOf(dev)
    s_bal = stake.balanceOf(dev)
    pol.removeLiquidity(
        web3.toWei(1_000, "ether"),
        web3.toWei(1_000, "ether"),
        web3.toWei(1_000, "ether"),
        {"from": dev},
    )
    # This only works without swaps happening before hand
    assert pol.totalSupply() == web3.toWei(9_000, "ether")
    assert abs(s_bal - stake.balanceOf(dev)) == web3.toWei(1_000, "ether")
    assert abs(b_bal - busd.balanceOf(dev)) == web3.toWei(1_000, "ether")
    pass


def test_swaps_busd_to_stake(liquidity_setup):
    dev, stake, busd, pol, fdt, fd = liquidity_setup
    user = accounts[4]
    user2 = accounts[5]
    busd.approve(pol, web3.toWei(100, "ether"), {"from": user})
    lock = accounts[6]
    # GET CONTROL VALUES WITH GET INPUT PRICE
    busd_reserve = busd.balanceOf(pol)
    stake_reserve = stake.balanceOf(pol)
    stake_total_supply = stake.totalSupply()
    pol_supply = pol.totalSupply()

    busd_buy_amount = web3.toWei(100, "ether")
    expected_output = (
        busd_buy_amount * stake_reserve // (busd_reserve + busd_buy_amount)
    )
    (expected_liquidity, _) = pol.getBaseToLiquidityInputPrice(
        busd_buy_amount * 0.5 // 100
    )

    output = pol.swap(
        busd_buy_amount,
        0,
        0,
        0,
        expected_output * 87 // 100,
        user,
        {"from": user},
    )  # since this is a tx, we need to specify that we want the return value
    assert output.return_value == expected_output * 87 // 100

    stake_to_burn = (
        5 * (busd_buy_amount * stake_reserve // (busd_reserve + busd_buy_amount)) // 100
    )
    assert int(expected_output * 5 // 100) == stake_to_burn
    # Check foundation
    assert busd.balanceOf(fdt) == web3.toWei(4, "ether")
    # Check totalSupply STAKE
    assert stake_to_burn == stake_total_supply - stake.totalSupply()
    # Check totalSupply LP
    assert expected_liquidity == pol.totalSupply() - pol_supply
    # Check current reserves
    assert busd.balanceOf(pol) - busd_reserve == web3.toWei(93, "ether")
    # we lose only 92% of the expected output since 8% is in BUSD/liquidity taxes
    assert stake_reserve - stake.balanceOf(pol) == expected_output * 92 // 100
    # Check _lock
    assert pol.balanceOf(lock) == expected_liquidity
    pass


def test_swap_stake_to_busd(liquidity_setup):
    dev, stake, busd, pol, fdt, fd = liquidity_setup
    user2 = accounts[5]
    lock = accounts[6]
    # GET CONTROL VALUES WITH GET INPUT PRICE
    busd_reserve = busd.balanceOf(pol)
    stake_reserve = stake.balanceOf(pol)
    stake_total_supply = stake.totalSupply()
    pol_supply = pol.totalSupply()

    stake_buy_amount = web3.toWei(100, "ether")

    expected_output = (
        stake_buy_amount * busd_reserve // (stake_reserve + stake_buy_amount)
    )
    (expected_liquidity, _) = pol.getTokenToLiquidityInputPrice(
        stake_buy_amount * 0.5 // 100
    )

    # test swap from token to base
    stake.approve(pol, web3.toWei(100, "ether"), {"from": user2})
    output_2 = pol.swap(
        0,
        stake_buy_amount,
        0,
        0,
        expected_output * 82 // 100,
        user2,
        {"from": user2},
    )

    assert output_2.return_value == expected_output * 82 // 100
    # Check foundation
    assert busd.balanceOf(fdt) == expected_output * 4 // 100
    # Check totalSupply STAKE
    assert stake_total_supply - stake.totalSupply() == stake_buy_amount * 5 // 100
    # Check totalSupply LP
    assert pol.totalSupply() - pol_supply == expected_liquidity
    # Check current reserves
    assert stake.balanceOf(pol) - stake_reserve == stake_buy_amount * 95 // 100
    assert busd_reserve - busd.balanceOf(pol) == expected_output * 94 // 100
    # Check _lock
    assert pol.balanceOf(lock) == expected_liquidity

    # TEST MAX SELL
    stake.transfer(user2, web3.toWei(1000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(3000, "ether"), {"from": user2})
    with reverts("MXSELL"):
        pol.swap(
            0,
            web3.toWei(1100, "ether"),
            0,
            0,
            1,
            user2,
            {"from": user2},
        )
    pol.swap(
        0,
        web3.toWei(101, "ether"),
        0,
        0,
        1,
        user2,
        {"from": user2},
    )
    chain.mine(10, timedelta=23 * 3600)

    with reverts("MXSELL"):
        pol.swap(
            0,
            web3.toWei(900, "ether"),
            0,
            0,
            1,
            user2,
            {"from": user2},
        )
    chain.mine(10, timedelta=3601)
    with reverts("MXSELL2"):
        pol.swap(
            0,
            web3.toWei(1100, "ether"),
            0,
            0,
            1,
            user2,
            {"from": user2},
        )
    pol.swap(
        0,
        web3.toWei(299, "ether"),
        0,
        0,
        1,
        user2,
        {"from": user2},
    )

    pass
