from brownie import (
    NerdFaucetV4,
    FundDistributor,
    TestToken,
    StakeTokenV2,
    POLv2,
    accounts,
    chain,
    reverts,
    history,
    FoundationV6,
)
from web3 import Web3 as web3
from pytest import fixture
import json


@fixture
def setup():
    dev = accounts[0]
    leaderDrops = accounts[1]
    flm = TestToken.deploy("FLM", "Flame", {"from": dev})
    busd = TestToken.deploy("BUSD", "Binance USD", {"from": dev})
    stake = StakeTokenV2.deploy({"from": dev})
    fd = FundDistributor.deploy(busd, {"from": dev})
    pol = POLv2.deploy(stake, busd, {"from": dev})
    faucet = NerdFaucetV4.deploy(
        stake,
        flm,
        leaderDrops,
        busd,
        fd,
        pol,
        {"from": dev},
    )
    fdt = FoundationV6.deploy(busd, stake, pol, accounts[8], {"from": dev})
    pol.setFoundation(fdt, {"from": dev})
    pol.setFundDistributor(fd, {"from": dev})
    stake.setFaucet(faucet, {"from": dev})
    fd.setTreasury(accounts[9], {"from": dev})
    fd.setPriceFloorProtection(accounts[9], {"from": dev})
    fd.setTeammate(accounts[9], 1, {"from": dev})

    for i in range(7):
        stake.transfer(accounts[i + 2], web3.toWei(1200, "ether"), {"from": dev})
        busd.transfer(accounts[i + 2], web3.toWei(200, "ether"), {"from": dev})

    pol.setLiquidityLock(accounts[8], {"from": dev})
    busd.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})

    pol.addLiquidity(
        web3.toWei(1000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )

    return faucet, dev, flm, stake, leaderDrops, busd, fd, pol


# Before any action that requires funds, stake needs to be approved


def test_deposits(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup
    with reverts("Insufficient allowance"):
        faucet.deposit(web3.toWei(10, "ether"), dev, {"from": accounts[2]})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[2]})
    faucet.deposit(web3.toWei(10, "ether"), dev, {"from": accounts[2]})
    # 10 gets burned and 10% of realized gets minted for leaderDrops || lottery
    assert stake.totalSupply() == web3.toWei(750_000 - 10 + 0.9, "ether")

    a2Accounting = faucet.accounting(accounts[2])
    a2Team = faucet.team(accounts[2])
    assert a2Accounting["netFaucet"] == web3.toWei(9, "ether")
    assert a2Team["upline"] == dev

    # AC3 DEPOSITs with ac2 as upline
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[3]})
    faucet.deposit(web3.toWei(10, "ether"), accounts[2], {"from": accounts[3]})

    a3Accounting = faucet.accounting(accounts[3])
    a3Team = faucet.team(accounts[3])
    assert a3Accounting["netFaucet"] == web3.toWei(9, "ether")
    assert a3Team["upline"] == accounts[2]

    # AC2 should get nothing since no FLAME
    a2Accounting = faucet.accounting(accounts[2])
    assert a2Accounting["airdrops_rcv"] == 0

    # AC2 have 5 referrals
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[4]})
    faucet.deposit(web3.toWei(10, "ether"), accounts[2], {"from": accounts[4]})
    # SEND FLAME TO AC2
    flm.transfer(accounts[2], web3.toWei(2, "ether"), {"from": dev})

    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[5]})
    faucet.deposit(web3.toWei(10, "ether"), accounts[2], {"from": accounts[5]})
    # AC2 gets 10% of bonus
    a2Accounting = faucet.accounting(accounts[2])
    assert a2Accounting["airdrops_rcv"] == web3.toWei(0.9, "ether")

    # AC4 DEPOSITs with AC2 as upline and has enough FLAME
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[6]})
    faucet.deposit(web3.toWei(10, "ether"), accounts[2], {"from": accounts[6]})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[7]})
    faucet.deposit(web3.toWei(10, "ether"), accounts[2], {"from": accounts[7]})
    # AC4 should get 1/4 of airdrop amount
    a2Accounting = faucet.accounting(accounts[2])
    assert a2Accounting["airdrops_rcv"] == web3.toWei(1.8 + (0.75 * 0.9), "ether")
    assert faucet.accounting(accounts[7])["airdrops_rcv"] == web3.toWei(
        0.25 * 0.9, "ether"
    )
    pass


def test_compounds(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup

    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[2]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[2]})

    # TEST COMPOUND ALL
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    # 1000 deposit, effective -> 900
    faucet.compoundAll({"from": accounts[2]})
    a2Acc = faucet.accounting(accounts[2])
    # 1 day = 1% of faucet (9)
    # 1 day = 2% of rebase (18) 47/48 -> 17.625
    # expected rolls to grow by 9 * 0.95 = 8.55 (faucet)
    # expected rolls to increase by 0.9  (rebase)
    assert a2Acc["rolls"] == web3.toWei(9.45, "ether") or a2Acc["rolls"] - web3.toWei(
        9.45, "ether"
    ) < web3.toWei(0.0001, "ether")
    # expected rebase compound to grow by 16.2 * 47 / 48 since on deposit rebase is only earned starting next rebase cycle
    assert abs(
        a2Acc["rebaseCompounded"] - web3.toWei(16.2 * 47 / 48, "ether")
    ) < web3.toWei(0.0001, "ether")

    # TEST COMPOUND FAUCET
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[3]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[3]})
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    faucet.compoundFaucet({"from": accounts[3]})
    a3Acc = faucet.accounting(accounts[3])
    assert a3Acc["rolls"] == web3.toWei(8.55, "ether") or abs(
        a3Acc["rolls"] - web3.toWei(8.55, "ether")
    ) < web3.toWei(0.0001, "ether")
    assert a3Acc["accRebase"] == web3.toWei(17.625, "ether")
    # TEST COMPOUND REBASE
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[4]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[4]})
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    faucet.compoundRebase({"from": accounts[4]})
    a4Acc = faucet.accounting(accounts[4])
    assert abs(
        a4Acc["rebaseCompounded"] - web3.toWei(16.2 * 47 / 48, "ether")
    ) < web3.toWei(0.0001, "ether")
    # Test DEPOSIT COMPOUND ALL
    stake.approve(faucet, web3.toWei(1_200, "ether"), {"from": accounts[5]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[5]})
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    faucet.deposit(web3.toWei(100, "ether"), dev, {"from": accounts[5]})
    a5Acc = faucet.accounting(accounts[5])
    # basically runs a compound all, but instead of 5% it makes it a 10% tax on the faucet
    assert abs(
        a5Acc["rebaseCompounded"] - web3.toWei(16.2 * 47 / 48, "ether")
    ) < web3.toWei(0.0001, "ether")
    # 9 * 0.9 = 8.1(faucet)
    assert a5Acc["rolls"] == web3.toWei(8.1, "ether") or abs(
        a5Acc["rolls"] - web3.toWei(8.1, "ether")
    ) < web3.toWei(0.0001, "ether")

    pass


def test_claims(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup
    # Test regular claims
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[2]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[2]})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[3]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[3]})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[4]})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": accounts[4]})

    chain.mine(10, timedelta=24 * 3600)  # 1 day
    # Claim Both
    a2_stake = stake.balanceOf(accounts[2])
    faucet.claim({"from": accounts[2]})
    #   Rebase claim (18, effective 16.2)
    #   Faucet Claim (9, effective 8.1)
    #   Total (27, effective 24.3)
    rebase_claim = 16.2 * 47 / 48
    effective_claim = rebase_claim + 8.1
    assert stake.balanceOf(accounts[2]) - a2_stake == web3.toWei(
        effective_claim, "ether"
    ) or abs(
        stake.balanceOf(accounts[2]) - a2_stake - web3.toWei(effective_claim, "ether")
    ) < web3.toWei(
        0.0001, "ether"
    )
    # Rebase only
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    # Rebase is calculated normally
    rebase_claim = 18 * 0.9
    a2_stake = stake.balanceOf(accounts[2])
    faucet.rebaseClaim({"from": accounts[2]})
    assert stake.balanceOf(accounts[2]) - a2_stake == web3.toWei(rebase_claim, "ether")
    # Faucet only
    chain.mine(10, timedelta=24 * 3600)  # 1 day
    a2_stake = stake.balanceOf(accounts[2])
    faucet.faucetClaim({"from": accounts[2]})
    # Since 2 days have passed since last faucet movement, claiming is over 2 days (9 * 2 = 18 * 90% = 16.2)
    assert stake.balanceOf(accounts[2]) - a2_stake == web3.toWei(16.2, "ether") or abs(
        stake.balanceOf(accounts[2]) - a2_stake - web3.toWei(16.2, "ether")
    ) < web3.toWei(0.0001, "ether")
    # Test claims with nerd  <= 0%
    chain.mine(10, timedelta=24 * 3600 * 31)  # 30 days
    faucet.claim({"from": accounts[2]})
    a2_nrd = faucet.getNerdData(accounts[2])
    a2_act = faucet.accounting(accounts[2])
    a2_claim = faucet.claims(accounts[2])
    a2_rebase = faucet.useRebaseFragment(accounts[2])
    # rebase claim is less than the 2% of gfv
    chain.mine(10, timedelta=5 * 24 * 3600)
    faucet.claim({"from": accounts[3]})
    u2_data = faucet.getNerdData(accounts[3])
    assert u2_data["_nerdPercent"] < 0 and u2_data["_nerdPercent"] > -330000
    chain.mine(10, timedelta=24 * 3600)
    u2_data = faucet.getNerdData(accounts[3])
    assert (
        u2_data["_rebasePayout"]
        == (u2_data["_grossFaucetValue"] // 50)
        * (u2_data["_nerdPercent"] + 330000)
        // 330000
    )
    chain.mine(10, timedelta=30 * 24 * 3600)

    # Test claims <= -33%
    faucet.claim({"from": accounts[4]})
    chain.mine(10, timedelta=24 * 3600)
    u3_data = faucet.getNerdData(accounts[4])
    assert u3_data["_nerdPercent"] < -330000
    chain.mine(10, timedelta=24 * 3600)
    u3_data = faucet.getNerdData(accounts[4])
    assert u3_data["_rebasePayout"] == 0

    # test deposit more after done and not max payout
    chain.mine(10, timedelta=365 * 24 * 3600)
    # After one year, claim everything.
    faucet.claim({"from": accounts[4]})
    # user should now be done
    assert faucet.accounting(accounts[4])["done"]
    chain.mine(10, timedelta=3 * 24 * 3600)
    with reverts("POX"):
        faucet.claim({"from": accounts[4]})
    # Adding more deposit (since it's not over cap payout, removes the done flag)
    stake.approve(faucet, web3.toWei(100, "ether"), {"from": accounts[4]})
    faucet.deposit(web3.toWei(100, "ether"), dev, {"from": accounts[4]})
    assert faucet.accounting(accounts[4])["done"] == False
    pass


def test_level_boost(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup

    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[2]})
    faucet.deposit(web3.toWei(10, "ether"), dev, {"from": accounts[2]})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": accounts[3]})
    faucet.deposit(web3.toWei(10, "ether"), dev, {"from": accounts[3]})

    busd.approve(faucet, web3.toWei(100, "ether"), {"from": accounts[2]})
    busd_bal = busd.balanceOf(accounts[2])
    st_total = stake.totalSupply()
    # Buy a boost
    faucet.boost(1, 1, {"from": accounts[2]})

    # Test that Distribution happens
    assert len(history[-1].events["Swap"]) == 1
    #  One for Swap and other for just the funds being distributed
    assert len(history[-1].events["Distributed"]) == 2
    assert history[-1].events["Distributed"]["amount"] == web3.toWei(3 * 0.75, "ether")
    # Test Stake was not left over
    assert stake.balanceOf(faucet) == 0
    #  Test tokens were burned
    assert stake.totalSupply() - st_total < 0
    # Can't boost while boosted
    with reverts("B1"):
        faucet.boost(1, 1, {"from": accounts[2]})

    assert busd_bal - busd.balanceOf(accounts[2]) == web3.toWei(3, "ether")
    a2_boost = faucet.claims(accounts[2])
    assert abs(a2_boost["boostEnd"] - (chain.time() + (12 * 3600))) <= 1

    chain.mine(10, timedelta=5 * 3600)
    a2_fd = faucet.getNerdData(accounts[2])

    # TEST PAYOUT INCREASES ACCORDINGLY
    assert abs(
        a2_fd["_faucetPayout"]
        -
        # 102% of time, 5 hours, 1% daily / 24 hours
        web3.toWei(1.02 * 5 * (9 / 100) / 24, "ether")
    ) < web3.toWei(0.001, "ether")
    # 102% of 2% rebase rewards by 5 hours (2% is 9 / 100 * 2) it divides 24 because it's 2% over 24 hours
    # since it's the first claim since deposit, rebase is 4.5 hours instead of 5
    assert a2_fd["_rebasePayout"] == web3.toWei(
        1.02 * 4.5 * (9 / 100) / 24 * 2, "ether"
    )
    # ) < web3.toWei(0.00001, "ether")

    # Check that boost rewards are kept after the 12 hour period
    chain.mine(10, timedelta=8 * 3600)
    # 1 hour after boost ends
    a2_fd = faucet.getNerdData(accounts[2])
    assert (
        abs(
            a2_fd["_faucetPayout"]
            - web3.toWei(
                # 102% time of 12 hours boosted + 1 hour of regular time
                (1.02 * 12 * 9 / 100 / 24) + (9 / 100 / 24),
                "ether",
            )
        )
        < web3.toWei(0.001, "ether")
    )
    # 1 Extra hour of rebase
    assert abs(
        a2_fd["_rebasePayout"]
        - web3.toWei((1.02 * 11.5 * 9 / 100 / 24 * 2) + (18 / 100 / 24), "ether")
    ) < web3.toWei(0.001, "ether")
    # Check that reboosting saves the amount.
    faucet.boost(1, 1, {"from": accounts[2]})
    a2_act = faucet.accounting(accounts[2])
    assert abs(
        a2_act["accFaucet"]
        - web3.toWei((1.02 * 12 * 9 / 100 / 24) + (9 / 100 / 24), "ether")
    ) < web3.toWei(0.001, "ether")
    pass


# test that tokens move as expected.
def test_airdrops(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup
    user1 = accounts[2]
    user2 = accounts[3]
    stake.approve(faucet, web3.toWei(1_200, "ether"), {"from": user1})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": user1})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": user2})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": user2})

    stake_supply = stake.totalSupply()

    # let a day pass
    chain.mine(10, timedelta=24 * 3600)  # 1 day

    ad_amount = web3.toWei(100, "ether")

    with reverts("burning nothing"):
        faucet.airdrop(user2, ad_amount, 0, True, {"from": user1})
    ad = faucet.airdrop(user2, ad_amount, 0, False, {"from": user1})
    # Only 90% is realized
    assert ad.events["Airdrop"]["amount"] == ad_amount * 9 // 10
    assert stake_supply - stake.totalSupply() == ad_amount
    u1 = faucet.team(user1)
    u2 = faucet.accounting(user2)
    assert u1["airdrops_sent"] == ad_amount
    assert u2["airdrops_rcv"] == ad_amount * 9 // 10
    # accumulation happens
    assert u2["accFaucet"] > 0
    assert u2["accRebase"] > 0

    pass


@fixture
def user_setup(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    stake.approve(faucet, web3.toWei(1_200, "ether"), {"from": user1})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": user1})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": user2})
    faucet.deposit(web3.toWei(1_000, "ether"), dev, {"from": user2})
    stake.approve(faucet, web3.toWei(1_000, "ether"), {"from": user3})
    faucet.deposit(web3.toWei(1_000, "ether"), user2, {"from": user3})
    return setup


def test_super_drops(user_setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = user_setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]

    u2_busd_bal = busd.balanceOf(user2)
    # Setup
    busd.approve(faucet, web3.toWei(500, "ether"), {"from": user2})
    # To user3 add a boost of a minimum of 150STAKE, for 12 hours, 1 level
    faucet.superDrop(user3, web3.toWei(150, "ether"), 1, 1, 0, {"from": user2})
    # test that a boost is added to the recipient
    u3_boost = faucet.claims(user3)
    assert abs(u3_boost["boostEnd"] - (chain.time() + (12 * 3600))) < 100
    assert u3_boost["boostLvl"] == 1
    # test that busd funds are distributed correctly
    # Taxes are distributed correctly
    # 3 distributions, initial swap, boost and bbb
    assert len(history[-1].events["Distributed"]) == 3
    superBoostPrice = u2_busd_bal - busd.balanceOf(user2)
    assert history[-1].events["SuperDrop"]["time"] == 1
    # test that airdrop actually happens
    assert (
        abs(history[-1].events["Airdrop"]["amount"] - web3.toWei(150 * 0.9, "ether"))
        < 1000
    )
    pass


def test_reset(user_setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = user_setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]
    # only users that are "DONE" can reset
    with reverts("Keep playing"):
        faucet.leaderReset(web3.toWei(150, "ether"), {"from": user1})
    chain.mine(10, timedelta=3600 * 366 * 24)
    faucet.claim({"from": user1})
    # make sure user is DONE
    assert faucet.accounting(user1)["done"]
    minDeposit = web3.toWei((75_000 * 0.5 / 100), "ether")
    initDeposit = web3.toWei(10, "ether")

    stake.approve(faucet, minDeposit + initDeposit, {"from": user1})
    # In theory this will only deposit 10 since the minimum deposit is 0.5% of 75000
    faucet.leaderReset(minDeposit + initDeposit, {"from": user1})

    u1_info = faucet.accounting(user1)
    u1_reset = faucet.leaders(user1)
    assert u1_info["done"] == False
    assert abs(u1_info["deposits"] - initDeposit * 9 // 10) < 100
    assert u1_info["rolls"] == 0
    assert abs(u1_info["netFaucet"] - initDeposit * 9 // 10) < 100
    assert u1_info["rebaseCompounded"] == 0
    assert u1_info["airdrops_rcv"] == 0
    assert u1_info["accFaucet"] == 0
    assert u1_info["accRebase"] == 0
    assert u1_info["rebaseCount"] == faucet.getTotalRebaseCount() + 1

    assert u1_reset["resets"] == 1
    assert u1_reset["lastResetLvl"] == 0

    #  make sure everything else remains the same
    chain.mine(10, timedelta=24 * 3600)
    faucet.claim({"from": user1})
    assert abs(
        history[-1].events["FaucetClaim"]["_amount"] - initDeposit * 9 / 1000
    ) < web3.toWei(0.0001, "ether")
    assert abs(
        history[-1].events["RebaseClaim"]["_amount"] - initDeposit * 18 * 47 / 48 / 1000
    ) < web3.toWei(0.0001, "ether")
    pass


def test_full_claims(user_setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = user_setup
    user1 = accounts[2]
    user2 = accounts[3]
    user3 = accounts[4]

    u1_bal = stake.balanceOf(user1)
    u2_bal = stake.balanceOf(user2)
    u3_bal = stake.balanceOf(user3)

    # User 1 will claim all after 232 days 365 / 3
    #   NOTE: Rebase only allows to claim up to 133% of NFV at any one time
    chain.mine(10, timedelta=24 * 3600 * 232)
    faucet.claim({"from": user1})
    # This is just to make sure the done FLAG gets set since due to match bullshit it might not be great
    if not faucet.accounting(user1)["done"]:
        chain.mine(1, timedelta=60)
        faucet.claim({"from": user1})
    #  max claim is 900 * 3.65 = 3285 - 10% => 2956.5
    with reverts("POX"):
        faucet.claim({"from": user1})
    assert faucet.accounting(user1)["done"]
    assert stake.balanceOf(user1) - u1_bal == web3.toWei(900 * 3.65 * 0.9, "ether")
    # user 2 will claims only rebase after 233 days
    #  user2 rebase is capped at 133% of nfv
    chain.mine(10, timedelta=24 * 3600)
    faucet.rebaseClaim({"from": user2})
    assert stake.balanceOf(user2) - u2_bal == web3.toWei(900 * 1.33 * 0.9, "ether")
    chain.mine(10, timedelta=24 * 3600)
    assert faucet.getNerdData(user2)["_nerdPercent"] <= -330000
    assert faucet.getNerdData(user2)["_rebasePayout"] == 0

    # user 3 will claim only faucet after 366 days
    chain.mine(10, timedelta=24 * 3600 * 181)
    faucet.faucetClaim({"from": user3})
    assert faucet.accounting(user3)["done"]
    assert stake.balanceOf(user3) - u3_bal == web3.toWei(900 * 3.65 * 0.9, "ether")
    # no claims after each person hits max payout
    with reverts("POX"):
        faucet.claim({"from": user3})
    # depositing renables claim
    stake.approve(faucet, web3.toWei(100, "ether"), {"from": user3})
    faucet.deposit(web3.toWei(100, "ether"), dev, {"from": user3})
    chain.mine(10, timedelta=24 * 3600)
    u3 = faucet.getNerdData(user3)
    assert abs(u3["_faucetPayout"] - web3.toWei(990 / 100, "ether")) < web3.toWei(
        0.0001, "ether"
    )
    pass


def test_owner_setters(user_setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = user_setup
    # Test updating to new govToken
    # Make sure that "has enough FLM" function updates accordingly
    pass


def test_migrations(user_setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = user_setup
    #  Test user Migration
    file = open("all_v2_data.json")
    data = json.load(file)
    user1_data = data["data"][0]
    faucet.migrate(
        user1_data["user"],
        user1_data["accounting"],
        user1_data["team"],
        user1_data["leaders"],
        user1_data["claims"],
        {"from": dev},
    )
    assert faucet.accounting(user1_data["user"])["deposits"] == 928800000000000000000
    assert faucet.claims(user1_data["user"])["faucetClaims"] == 2764316262843316677023
    # Check on gas cost for 1 user
    pass
