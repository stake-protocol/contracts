from brownie import (
    NerdFaucetV4,
    FundDistributor,
    TestToken,
    StakeTokenV2,
    POLv2,
    accounts,
    chain,
    reverts,
    history,
    FoundationV6,
)
from web3 import Web3 as web3
from pytest import fixture


@fixture
def setup():
    dev = accounts[0]
    leaderDrops = accounts[1]
    flm = TestToken.deploy("FLM", "Flame", {"from": dev})
    busd = TestToken.deploy("BUSD", "Binance USD", {"from": dev})
    stake = StakeTokenV2.deploy({"from": dev})
    fd = FundDistributor.deploy(busd, {"from": dev})
    pol = POLv2.deploy(stake, busd, {"from": dev})
    faucet = NerdFaucetV4.deploy(
        stake,
        flm,
        leaderDrops,
        busd,
        fd,
        pol,
        {"from": dev},
    )
    fdt = FoundationV6.deploy(busd, stake, pol, accounts[8], {"from": dev})
    pol.setFoundation(fdt, {"from": dev})
    pol.setFundDistributor(fd, {"from": dev})
    stake.setFaucet(faucet, {"from": dev})
    fd.setTreasury(accounts[9], {"from": dev})
    fd.setPriceFloorProtection(accounts[9], {"from": dev})
    fd.setTeammate(accounts[9], 1, {"from": dev})

    for i in range(7):
        stake.transfer(accounts[i + 2], web3.toWei(1200, "ether"), {"from": dev})
        busd.transfer(accounts[i + 2], web3.toWei(200, "ether"), {"from": dev})

    pol.setLiquidityLock(accounts[8], {"from": dev})
    busd.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})
    stake.approve(pol, web3.toWei(10_000, "ether"), {"from": dev})

    pol.addLiquidity(
        web3.toWei(1000, "ether"),
        web3.toWei(10_000, "ether"),
        web3.toWei(10_000, "ether"),
        {"from": dev},
    )

    return faucet, dev, flm, stake, leaderDrops, busd, fd, pol


def test_nerd_movements(setup):
    faucet, dev, flm, stake, leaderDrops, busd, fd, pol = setup
    user1 = accounts[1]
    user2 = accounts[2]
    # This is GMG info
    faucet.migrate(
        user1,
        [
            39615959433240907411023,
            16467464787345482632647,
            17984828807301641560574,
            39019303280736644924275,
            5163665838593783217802,
            292248262837127086215,
            0,
            8828,
            1670504405,
            False,
        ],
        [
            44,
            dev,
            1654549303,
            0,
            4620604557290750518372,
            1670504405,
            1537890685496916838026,
            95,
            3,
            0,
            "0x0000000000000000000000000000000000000000",
            2,
        ],
        [0, 0, False],
        [
            18480609090957168656915,
            3382680569574620476830,
            16143000706274806372057,
            0,
            0,
        ],
    )
    faucet.migrate(
        user2,
        [
            3149876193645357576891,
            1072452847031249998644,
            665928220404219699074,
            376502146604391044662,
            1411495126209887879173,
            880204974548924894650,
            0,
            7437,
            1667935848,
            False,
        ],
        [
            2,
            dev,
            1654544118,
            0,
            1403330388865162593761,
            1670430525,
            175000000000000000000,
            8,
            3,
            0,
            "0x0000000000000000000000000000000000000000",
            1,
        ],
        [0, 0, False],
        [1539514667837203107073, 4854717769363068219973, 1539514667837203107073, 0, 0],
    )

    u1_acc = faucet.accounting(user1)
    u1_tm = faucet.team(user1)
    u1_cl = faucet.claims(user1)
    # Check that percentage is what we're claiming
    u1_nerd = faucet.getNerdData(user1)

    assert u1_nerd["_nerdPercent"] == (
        u1_acc["netFaucet"]
        + u1_acc["rolls"]
        + u1_tm["airdrops_sent"]
        - u1_cl["faucetEffectiveClaims"]
        - u1_cl["rebaseClaims"]
    ) * 100_0000 // (u1_acc["netFaucet"] + u1_tm["airdrops_sent"])
    # compound all
    faucet.compoundAll({"from": user1})
    after_compound = faucet.getNerdData(user1)
    assert u1_nerd["_nerdPercent"] < after_compound["_nerdPercent"]
    assert False
    pass
