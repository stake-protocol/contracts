// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "../interfaces/IToken.sol";

contract FundDistributor {
    struct Portion {
        uint256 _portion;
        uint256 index;
        bool set;
        uint256 distributed;
    }

    address public owner;

    IToken public BUSD;

    address public treasury;
    address public pff; // PriceFloor Protection
    address[] public teammates;
    mapping(address => Portion) public portion;
    uint256 public totalPortion;

    event AddressUpdate(string _address, address previous, address update);
    event TeammateUpdate(
        address indexed _teammate,
        uint256 index,
        uint256 portion
    );
    event Distributed(
        uint256 amount,
        uint256 treasury,
        uint256 pricefloor,
        uint256 team
    );

    modifier onlyOwner() {
        require(msg.sender == owner, "Not Owner");
        _;
    }

    constructor(address _busd) {
        owner = msg.sender;
        BUSD = IToken(_busd);
    }

    function setTreasury(address _newTreasury) external onlyOwner {
        emit AddressUpdate("Treasury", treasury, _newTreasury);
        treasury = _newTreasury;
    }

    function setPriceFloorProtection(address _newPFF) external onlyOwner {
        emit AddressUpdate("Treasury", pff, _newPFF);
        pff = _newPFF;
    }

    function setTeammate(address _teammate, uint256 _portion)
        external
        onlyOwner
    {
        Portion storage member = portion[_teammate];
        if (!member.set) {
            teammates.push(_teammate);
            member.set = true;
            member.index = teammates.length - 1;
        } else totalPortion -= member._portion;
        member._portion = _portion;
        totalPortion += _portion;
        emit TeammateUpdate(_teammate, member.index, member._portion);
    }

    function totalTeammates() external view returns (uint256) {
        return teammates.length;
    }

    function distributeFunds(
        uint256 amount,
        uint256 _tr,
        uint256 _pff,
        uint256 _tm
    ) public {
        uint256 totalDistributed = _tr + _pff + _tm;
        uint256 treasuryAmount = (_tr * amount) / totalDistributed;
        uint256 pffAmount = (_pff * amount) / totalDistributed;
        uint256 teamAmount = amount - treasuryAmount - pffAmount;
        uint256 tinyPortion = 0;
        // Transfer to treasury
        if (treasuryAmount > 0)
            BUSD.transferFrom(msg.sender, treasury, treasuryAmount);
        // Transfer to PriceFloorProtection
        if (pffAmount > 0) BUSD.transferFrom(msg.sender, pff, pffAmount);
        // Teammate Transfer Loop
        if (teamAmount > 0)
            for (uint256 i = 0; i < teammates.length; i++) {
                Portion storage member = portion[teammates[i]];
                if (member._portion == 0) continue;
                tinyPortion = (teamAmount * member._portion) / totalPortion;
                member.distributed += tinyPortion;
                BUSD.transferFrom(msg.sender, teammates[i], tinyPortion);
            }
        emit Distributed(amount, treasuryAmount, pffAmount, teamAmount);
    }
}
