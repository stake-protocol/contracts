// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/access/Ownable.sol";
import "../../interfaces/IToken.sol";
import "../../interfaces/ITokenLocker.sol";

contract TokenLocker is Ownable, ITokenLocker {
    uint256 public unlockTime;
    uint256 public prevLockAmount;
    address public immutable token;

    constructor(address _token) {
        require(_token != address(0), "Invalid Token");
        unlockTime = block.timestamp;
        token = _token;
    }

    function updateLock() external {
        if (prevLockAmount >= IToken(token).balanceOf(address(this))) return;
        prevLockAmount = IToken(token).balanceOf(address(this));
        unlockTime = block.timestamp + 12 weeks;
    }

    function withdrawTokens(uint256 amount) external onlyOwner {
        require(unlockTime < block.timestamp, "Funds Locked");
        uint256 availAmount = IToken(token).balanceOf(address(this));
        require(availAmount >= amount, "Insufficient Funds");
        IToken(token).transfer(owner(), amount);
        prevLockAmount -= amount;
    }

    // In case tokens not associated with this locker are sent to the contract
    function withdrawOtherTokens(address _wrongToken) external onlyOwner {
        require(_wrongToken != token, "No sneaky");
        uint256 amount = IToken(_wrongToken).balanceOf(address(this));
        require(amount > 0, "No tokens");
        IToken(token).transfer(owner(), amount);
    }
}
