// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/access/Ownable.sol";

contract DebtPrePool is Ownable {
    struct Debt {
        uint256 busdOwed;
        uint8 optionSelected;
    }

    mapping(address => Debt) public debts;
    uint256 public totalOwed;

    event SelectedOption(address indexed user, uint8 option);

    function selectOption(uint8 _option) external {
        Debt storage userDebt = debts[msg.sender];
        require(userDebt.busdOwed > 0, "Invalid User");
        require(userDebt.optionSelected == 0, "Option selected");
        require(_option <= 3 && _option > 0, "Invalid Option");
        userDebt.optionSelected = _option;
        emit SelectedOption(msg.sender, _option);
    }

    function setDebt(address _user, uint256 busdOwed) public onlyOwner {
        if (debts[_user].busdOwed > 0) {
            totalOwed -= debts[_user].busdOwed;
        }
        debts[_user] = Debt(busdOwed, 0);
        totalOwed += busdOwed;
    }

    function setMultiple(
        address[] calldata _users,
        uint256[] calldata owedAmounts
    ) external onlyOwner {
        require(_users.length > 0, "Invalid data");
        require(_users.length == owedAmounts.length, "Incorrect lengths");
        for (uint256 i = 0; i < _users.length; i++) {
            setDebt(_users[i], owedAmounts[i]);
        }
    }
}
