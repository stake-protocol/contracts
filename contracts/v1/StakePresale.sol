// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract TokenPresale is Ownable, ReentrancyGuard {
    using SafeERC20 for IERC20;

    struct UserInfo {
        uint256 bought;
        bool claimed;
    }

    uint256 public immutable HARD_CAP = 75 ether;
    uint256 public constant MAX_BUY = 2 ether; // per wallet
    uint256 public constant tokenPerBNB = 525;
    address public fundWallet;
    uint256 public constant BUY_INTERVAL = 0.01 ether;

    uint256 public constant duration = 24 hours;
    uint256 public immutable saleStart = 1651489200; //MAY 2ND 2022 7AM EST

    IERC20 public STAKE;

    uint256 public totalRaised;

    mapping(address => UserInfo) public userInfo;

    event BoughtToken(address indexed _user, uint256 amount, uint256 _raised);
    event TokenClaimed(address indexed _user, uint256 amount);
    event TokenSet(address _token);

    receive() external payable {
        // We do nothing... if people send funds directly that's on them... use the function people
    }

    fallback() external payable {
        // We do nothing... if people send funds directly that's on them... use the function people
    }

    constructor() {
        fundWallet = owner();
    }

    function buyToken() external payable nonReentrant {
        uint256 amount = msg.value;
        require(amount > 0, "Need money");
        require(amount % BUY_INTERVAL == 0, "Only intervals of 0.01 BNB");
        require(
            block.timestamp < saleStart + duration &&
                block.timestamp >= saleStart,
            "Sale not running"
        );
        UserInfo storage user = userInfo[msg.sender];
        require(
            user.bought < MAX_BUY && user.bought + amount <= MAX_BUY,
            "User Cap reached"
        );
        uint256 raised = totalRaise();
        require(
            raised < HARD_CAP && raised + amount <= HARD_CAP,
            "Main cap reached"
        );
        user.bought += amount;
        totalRaised += amount;

        emit BoughtToken(msg.sender, amount, totalRaised);
    }

    function claimToken() external nonReentrant {
        require(block.timestamp > saleStart + duration, "Sale running");
        require(address(STAKE) != address(0), "Token not yet available");
        UserInfo storage user = userInfo[msg.sender];
        require(!user.claimed && user.bought > 0, "Already claimed");
        user.claimed = true;
        uint256 claimable = user.bought * tokenPerBNB;
        STAKE.safeTransfer(msg.sender, claimable);
        emit TokenClaimed(msg.sender, claimable);
    }

    function setToken(address _token) external onlyOwner {
        require(address(STAKE) == address(0), "Token Set");
        STAKE = IERC20(_token);
        emit TokenSet(_token);
    }

    function totalRaise() public view returns (uint256) {
        return address(this).balance;
    }

    function withdraw() external payable onlyOwner {
        uint256 raised = totalRaise();
        (bool success, ) = payable(fundWallet).call{value: raised}("");
        require(success, "Unsuccessful, withdraw");
    }

    function setFundWallet(address _newFund) external onlyOwner {
        fundWallet = _newFund;
    }
}
