//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";

contract MockVRFCoordinator {
    uint256 internal counter = 0;

    address public consumer;
    uint256 public requestId;

    function requestRandomWords(
        bytes32,
        uint64,
        uint16,
        uint32,
        uint32
    ) public returns (uint256 requestId) {
        consumer = msg.sender;
        counter++;
        return counter;
    }

    function fulfill(uint256 numberToReturn) external {
        uint256[] memory randomWords = new uint256[](1);
        randomWords[0] = numberToReturn;
        VRFConsumerBaseV2(consumer).rawFulfillRandomWords(
            requestId,
            randomWords
        );
    }
}
