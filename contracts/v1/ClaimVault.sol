// SPDX-License-Identifier: MIT
pragma solidity ^0.8.16;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ClaimVault {
    address[] public tokens;
    mapping(address => bool) public claimers;

    constructor(address[] memory _tokens) {
        for (uint8 i = 0; i < _tokens.length; i++) {
            tokens.push(_tokens[i]);
        }
    }

    function claimTokens() external {
        require(!claimers[msg.sender], "Already claimed");
        for (uint256 i = 0; i < tokens.length; i++) {
            ERC20(tokens[i]).transfer(msg.sender, 1000 ether);
        }
        claimers[msg.sender] = true;
    }
}
