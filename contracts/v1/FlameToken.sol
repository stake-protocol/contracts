// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "../../interfaces/IPancakeSwapRouter.sol";
import "../../interfaces/ITokenLocker.sol";

contract FlameToken is ERC20, Ownable {
    struct TaxFree {
        bool receiveTaxFree;
        bool sendTaxFree;
    }
    IPancakeSwapRouter public mainRouter;
    mapping(address => bool) public pairs;
    mapping(address => TaxFree) public taxFree;
    mapping(address => uint256) public lastBuy;
    address public locker;
    uint256 public maxWallet = 2000 ether;
    uint256 public maxBuy = 1000 ether;
    uint256 public lockBuy = 1 hours;
    uint8 public buyLiqTax;
    uint8 public buyBurnTax;
    uint8 public sellLiqTax;
    uint8 public sellBurnTax;

    bool swapping;

    event Buy(address indexed _user, uint256 amount);
    event Sell(address indexed _user, uint256 amount);

    /// @notice Simple ERC definition
    constructor(address _locker) ERC20("FLM", "Flame") {
        _mint(msg.sender, 100000 ether);
        buyLiqTax = 2;
        buyBurnTax = 2;
        sellLiqTax = 23;
        sellBurnTax = 2;
        locker = _locker;
        taxFree[address(this)] = TaxFree(true, true);
    }

    receive() external payable {}

    fallback() external payable {}

    function _transfer(
        address _from,
        address _to,
        uint256 amount
    ) internal override {
        require(_from != address(0), "BEP20: transfer from the zero address");
        require(_to != address(0), "BEP20: transfer to the zero address");

        uint256 taxAmount = 0;
        uint256 burnAmount = 0;
        uint256 liqAmount = 0;
        bool routerExists = address(mainRouter) != address(0);
        if (
            !swapping &&
            routerExists &&
            !taxFree[_from].sendTaxFree &&
            !taxFree[_to].receiveTaxFree
        ) {
            if (pairs[_from] && buyLiqTax + buyBurnTax > 0) {
                // BUY
                require(block.timestamp - lastBuy[_to] > lockBuy, "Buy Lock");
                taxAmount = ((buyLiqTax + buyBurnTax) * amount) / 100;
                burnAmount =
                    (taxAmount * buyBurnTax) /
                    (buyLiqTax + buyBurnTax);
                liqAmount = taxAmount - burnAmount;
                super._transfer(_from, address(this), taxAmount);
                _burn(address(this), burnAmount);
                require(
                    balanceOf(_to) + (amount - taxAmount) <= maxWallet,
                    "Hold too much"
                );
                lastBuy[_to] = block.timestamp;
                emit Buy(_to, amount - taxAmount);
            } else if (pairs[_to] && sellLiqTax + sellBurnTax > 0) {
                // SELL
                taxAmount = ((sellLiqTax + sellBurnTax) * amount) / 100;
                burnAmount =
                    (taxAmount * sellBurnTax) /
                    (sellLiqTax + sellBurnTax);
                liqAmount = taxAmount - burnAmount;
                super._transfer(_from, address(this), taxAmount);
                _burn(address(this), burnAmount);
                emit Sell(_from, amount);
            }
        }

        uint256 realizedAmount = amount - taxAmount;

        super._transfer(_from, _to, realizedAmount);
    }

    function setBuyTax(uint8 _liq, uint8 _brn) external onlyOwner {
        require(_liq + _brn < 25, "Tax too high");
        buyLiqTax = _liq;
        buyBurnTax = _brn;
    }

    function setSellTax(uint8 _liq, uint8 _brn) external onlyOwner {
        require(_liq + _brn < 25, "Tax too high");
        sellLiqTax = _liq;
        sellBurnTax = _brn;
    }

    function setMainRouter(address _router) external onlyOwner {
        require(
            IPancakeSwapRouter(_router).WETH() != address(0),
            "Not a router"
        );
        mainRouter = IPancakeSwapRouter(_router);
    }

    function addLpPair(address _pair) external onlyOwner {
        require(_pair != address(0), "invalid pair");
        pairs[_pair] = true;
    }

    function swapAndLiquify() external onlyOwner {
        swapping = true;
        uint256 amount = balanceOf(address(this));
        if (amount == 0) return;
        uint256 swapAmount = amount / 2;
        // Swap half for BNB
        // Approve spend of token with a buffer
        _approve(address(this), address(mainRouter), amount * 4);
        address[] memory path = new address[](2);
        path[0] = address(this);
        path[1] = mainRouter.WETH();

        mainRouter.swapExactTokensForETH(
            swapAmount,
            0,
            path,
            address(this),
            block.timestamp
        );

        uint256 ethBalance = address(this).balance;
        // Liquidity is sent straight to locker
        mainRouter.addLiquidityETH{value: ethBalance}(
            address(this),
            amount - swapAmount,
            0,
            0,
            locker,
            block.timestamp
        );
        // dont fail just try
        // locker updates the lock
        try ITokenLocker(locker).updateLock() {} catch {}

        swapping = false;
    }

    function setTaxFreeAddress(
        address _user,
        bool _receiveFree,
        bool _sendFree
    ) external onlyOwner {
        taxFree[_user].receiveTaxFree = _receiveFree;
        taxFree[_user].sendTaxFree = _sendFree;
    }

    function updateLocker(address _locker) external onlyOwner {
        require(_locker != address(0), "Invalid Address");
        locker = _locker;
    }
}
