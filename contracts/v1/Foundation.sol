// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "../../interfaces/IToken.sol";
import "../../interfaces/IVault.sol";
import "../../interfaces/IFountain.sol";
import "../../interfaces/ITokenLocker.sol";
import "../../interfaces/IFaucet.sol";

contract Foundation is Ownable, ReentrancyGuard {
    struct Investment {
        uint256 lpDouble; // The double amount since we provided half of STAKE
        uint256 lpSingle; // Thhe user input the full LP (BNB swapped to STAKE) and added LP
        uint256 claimed; // bnb CLAIMED
        uint256 rewarded; // LP rewarded
        uint256 debt; // DEBT for Rewards
        uint256 busdDeposit; // bnb Deposited, not sure if worth it since BNB is buying lP and is subject to impermanent loss
        uint256 withdraws; // BUSD withdraws
    }

    IToken public stake;
    IToken public busd;
    IVault public vault;
    IFountain public pol;
    ITokenLocker public locker;
    IFaucet public faucet;
    address public paybackWallet;

    uint8 public paybackFee = 20; // 20% of vault fee
    uint8 public vaultFee = 5;
    uint8 public rewardFee = 2;
    uint8 public lockFee = 3;
    uint8 public compoundFee = 5;

    uint256 public constant PRECISION_FACTOR = 1e12;
    uint256 public VAULT_THRESHOLD;

    uint256 public totalDouble;
    uint256 public totalSingle;
    uint256 public totalStaked;
    uint256 public accRewardsPerShare;

    uint256 public providers;

    uint256 public distributedRewards;
    uint256 public totalLocked;

    mapping(address => Investment) public investment;

    // EVENTS
    event Deposit(address indexed _user, uint256 bnbDeposit, uint256 lpAmount);
    event Withdraw(address indexed _user, uint256 bnbWithdraw);
    event Compound(
        address indexed _user,
        uint256 _lp_rewarded,
        uint256 _lp_tax
    );
    event Log(string _description, uint256 _val);
    event AuditLog(string _description, address _address);
    event Claimed(address indexed _user, uint256 _lp_amount, uint256 _bnb);
    event Lock(address indexed _user, uint256 lockAmount);
    event CreatedLP(uint256 _amount);
    event Vaulted(address indexed _user, uint256 vaultAmount);
    event AddToRewards(uint256 _shareDiv);
    event RewardShareIncrease(uint256 _newRewardShare);
    event IncreasedStake(uint256 _totalStaked, bool _here);
    event FeesUpdated(
        uint8 _newVault,
        uint8 _oldVault,
        uint8 _newReward,
        uint8 _oldReward,
        uint8 _newLock,
        uint8 _oldLock,
        uint8 _newCompound,
        uint8 _oldCompound
    );
    event LogTokenApproval(
        address indexed _token,
        address _spender,
        uint256 _amount
    );

    constructor(
        address _vault,
        address _stake,
        address _busd,
        address _payback,
        address _faucet
    ) {
        require(
            _vault != address(0) &&
                _stake != address(0) &&
                _busd != address(0) &&
                _payback != address(0) &&
                _faucet != address(0),
            "Cant with address 0"
        );
        // When deploying make sure that this contract is tax free on STAKE and POL
        vault = IVault(_vault);
        stake = IToken(_stake);
        busd = IToken(_busd);
        VAULT_THRESHOLD = 120;
        paybackWallet = _payback;
        faucet = IFaucet(_faucet);
    }

    // ----
    // RUn once functions
    // ----
    function setPolData(address _pol, address _locker) external onlyOwner {
        require(
            address(pol) == address(0) && address(locker) == address(0),
            "Already set"
        );
        pol = IFountain(_pol);
        locker = ITokenLocker(_locker);
    }

    // ------------------
    // EXTERNAL FUNCTIONS
    // ------------------
    function deposit(uint256 busdAmount) external nonReentrant {
        require(busdAmount > 1 ether, "Less than minimum");
        require(
            busd.transferFrom(msg.sender, address(this), busdAmount),
            "Failed BUSD Transfer"
        );
        // GET TOTAL BNB AMOUNT
        busd.approve(address(pol), busdAmount);

        uint256 taxAmount = (busdAmount * vaultFee) / 100;
        Investment storage user = investment[msg.sender];
        user.busdDeposit += busdAmount;

        busdAmount -= taxAmount;
        // TAKE BUSD for payback
        if (paybackFee > 0) {
            uint256 payback = (taxAmount * paybackFee) / 100;
            taxAmount -= payback;
            require(busd.transfer(paybackWallet, payback), "Need to payback");
        }
        //update totalStaked and already allocated rewards.
        if (pendingToDistribute() > 0) distributePending();
        bool firstDeposit = user.lpDouble + user.lpSingle == 0;
        if (!firstDeposit) {
            _compound(msg.sender);
        } else {
            providers++;
        }
        // Get vault threshold and only withdraw as long as status < needed ( use checkvault() from Faucet )
        (uint256 stakeNeeded, , , uint256 vaultBalance) = faucet.checkVault();
        uint256 stLpAvail = 0;
        if ((stakeNeeded * VAULT_THRESHOLD) / 100 < vaultBalance)
            stLpAvail = vaultBalance - stakeNeeded;

        // GET STAKE AMOUNT NEEDED FOR LIQUIDITY
        (, stakeNeeded) = pol.getBnbToLiquidityInputPrice(busdAmount);
        // Approve STAKE and BUSD for spend
        stake.approve(address(pol), stakeNeeded);

        bool notEnough = stakeNeeded > stLpAvail;
        if (notEnough) {
            stakeNeeded = pol.bnbToTokenSwapInput(
                1,
                (busdAmount / 2) + taxAmount
            );
            busdAmount -= busdAmount / 2;
        } else {
            vault.withdraw(stakeNeeded);
            pol.bnbToTokenSwapInput(1, taxAmount);
        }
        stLpAvail = pol.addLiquidity(1, stakeNeeded, busdAmount);
        emit CreatedLP(stLpAvail);
        taxAmount = (stLpAvail * lockFee) / 95;
        totalLocked += taxAmount;
        require(pol.transfer(address(locker), taxAmount), "Failed lock");
        emit Lock(msg.sender, taxAmount);
        locker.updateLock();
        if (providers > 1) {
            emit AddToRewards((stLpAvail * rewardFee) / 95);
            stLpAvail -= (taxAmount + (stLpAvail * rewardFee) / 95);
        } else stLpAvail -= taxAmount;
        totalStaked += stLpAvail;
        totalStaked = increaseRewardsForOthers(msg.sender);

        // Whatever was not liquified send to vault or back to user
        emit Deposit(msg.sender, busdAmount, stLpAvail);
        busdAmount = busd.balanceOf(address(this));
        if (busdAmount > 0) {
            busd.transfer(msg.sender, busdAmount);
            user.busdDeposit -= busdAmount;
        }
        uint256 _stakeVaulted = stake.balanceOf(address(this));
        emit Vaulted(msg.sender, _stakeVaulted);
        stake.transfer(address(vault), _stakeVaulted);
        // Update User values
        user.lpSingle += notEnough ? stLpAvail : 0;
        user.lpDouble += notEnough ? 0 : stLpAvail;
        user.debt = getRewardAmount(user.lpDouble, user.lpSingle);
        totalSingle += notEnough ? stLpAvail : 0;
        totalDouble += notEnough ? 0 : stLpAvail;
    }

    function distributePending() public {
        uint256 currentBalance = increaseAccRewards();
        totalStaked = currentBalance;
    }

    function pendingToDistribute() public view returns (uint256) {
        uint256 currentBalance = pol.balanceOf(address(this));
        if (currentBalance > totalStaked) return currentBalance - totalStaked;
        return 0;
    }

    function increaseAccRewards() internal returns (uint256) {
        uint256 currentBalance = pol.balanceOf(address(this));
        if (currentBalance > totalStaked && totalStaked > 0) {
            distributedRewards += currentBalance - totalStaked;
            accRewardsPerShare +=
                ((currentBalance - totalStaked) * PRECISION_FACTOR) /
                ((totalSingle * 2) + totalDouble);
        }
        return currentBalance;
    }

    function increaseRewardsForOthers(address _user)
        internal
        returns (uint256)
    {
        Investment storage user = investment[_user];
        uint256 currentBalance = pol.balanceOf(address(this));
        if (currentBalance > totalStaked && totalStaked > 0) {
            uint256 rewardsToDistribute = currentBalance - totalStaked;
            distributedRewards += rewardsToDistribute;
            uint256 numerator = rewardsToDistribute * PRECISION_FACTOR;
            uint256 denominator = (totalSingle - user.lpSingle) * 2;
            denominator += totalDouble - user.lpDouble;
            // increase shares only if there are other people to distribute to
            if (denominator > 0) {
                emit Log("Dividends Distributed", rewardsToDistribute);
                accRewardsPerShare += numerator / denominator;
            }
        }
        return currentBalance;
    }

    function _compound(address _user) internal {
        Investment storage user = investment[_user];
        uint256 rewarded = getRewardAmount(user.lpDouble, user.lpSingle);
        uint256 prevDebt = user.debt;
        if (rewarded > user.debt) {
            user.debt = rewarded;
            rewarded -= prevDebt;
            uint256 tax = (rewarded * compoundFee) / 100;

            rewarded -= tax;
            totalStaked -= tax;
            totalLocked += tax;
            pol.transfer(address(locker), tax);
            locker.updateLock();
            user.rewarded += rewarded;
            user.lpDouble += rewarded;
            totalDouble += rewarded;
            user.debt = getRewardAmount(user.lpDouble, user.lpSingle);
            emit Compound(_user, rewarded, tax);
        } else {
            emit AuditLog("Nothing to Compound", _user);
        }
    }

    function getRewardAmount(uint256 _double, uint256 _single)
        internal
        view
        returns (uint256)
    {
        return
            (accRewardsPerShare * (_double + (_single * 2))) / PRECISION_FACTOR;
    }

    function compoundReward() external nonReentrant {
        require(totalStaked > 0, "Nothing Staked");
        if (pendingToDistribute() > 0) distributePending();
        _compound(msg.sender);
    }

    function claimReward() external nonReentrant {
        require(totalStaked > 0, "Nothing Staked");
        if (pendingToDistribute() > 0) distributePending();
        _claim(msg.sender);
    }

    function _claim(address _user) internal {
        Investment storage user = investment[_user];
        uint256 rewarded = getRewardAmount(user.lpDouble, user.lpSingle);
        uint256 prevDebt = user.debt;
        if (rewarded > user.debt) {
            rewarded -= prevDebt;
            // get currentLiquidity
            uint256 currentBalance = pol.balanceOf(address(this));
            uint256 divSpread = (rewarded * rewardFee) / 100; // 2%
            uint256 lockLiquidity = (rewarded * lockFee) / 100; // 3%
            uint256 removedLiq = rewarded - divSpread - lockLiquidity;
            uint8 nonLpTotal = 100 - rewardFee - lockFee;
            // lockTokens
            if (lockLiquidity > 0) {
                totalLocked += lockLiquidity;
                pol.transfer(address(locker), lockLiquidity);
                locker.updateLock();
            }
            // remove liquidity of rest
            pol.removeLiquidity(removedLiq, 1, 1);

            uint256 _busd = busd.balanceOf(address(this));
            uint256 swap = (_busd * vaultFee) / nonLpTotal;

            if (paybackFee > 0) {
                uint256 payback = (swap * paybackFee) / 100;
                swap -= payback;
                require(
                    busd.transfer(paybackWallet, payback),
                    "Need to payback"
                );
            }

            busd.approve(address(pol), _busd);
            pol.bnbToTokenSwapInput(1, swap);
            _busd = busd.balanceOf(address(this));
            removedLiq =
                ((currentBalance - pol.balanceOf(address(this))) *
                    (nonLpTotal - vaultFee)) /
                nonLpTotal;
            // update totalStaked since we're basically removing all that will be rewarded to the user.
            totalStaked -= rewarded;
            totalStaked = increaseRewardsForOthers(_user); // distribute pending
            user.debt = getRewardAmount(user.lpDouble, user.lpSingle);
            // Amount that was actually converted to BNB
            user.rewarded += removedLiq; //Reduce it again to get 10% since the second 5% is converted from the reward BNB
            //user gets the BNB;
            user.claimed += _busd;
            bool succ = busd.transfer(_user, _busd);
            require(succ, "FAIL BUSD CLAIM");
            emit Claimed(_user, rewarded, _busd);
            // Stake gets sent to VAULT

            uint256 _stakeVaulted = stake.balanceOf(address(this));
            emit Vaulted(_user, _stakeVaulted);
            stake.transfer(address(vault), _stakeVaulted);
        } else {
            emit AuditLog("Nothing to Claim", _user);
        }
    }

    function pendingRewards(address _user) external view returns (uint256) {
        Investment storage user = investment[_user];
        uint256 rewards = getRewardAmount(user.lpDouble, user.lpSingle);
        return rewards - user.debt;
    }

    function withdraw(uint256 amount) external nonReentrant {
        require(totalStaked > 0, "Nothing Staked");
        require(amount > 0, "Amount = 0");
        uint256 retrievedAmount = amount;
        Investment storage user = investment[msg.sender];
        require(user.lpDouble + user.lpSingle >= amount, "too much");
        if (pendingToDistribute() > 0) distributePending();
        _claim(msg.sender);
        uint256 retrieveDouble = amount;
        uint256 retrieveSingle;
        if (user.lpDouble < amount) {
            retrieveSingle = retrieveDouble - user.lpDouble;
            retrieveDouble -= retrieveSingle;
        }
        user.lpDouble -= retrieveDouble;
        user.lpSingle -= retrieveSingle;
        totalDouble -= retrieveDouble;
        totalSingle -= retrieveSingle;
        totalStaked -= amount;
        if (user.lpDouble + user.lpSingle == 0) providers--;
        // Lock Tax (3% sent to Pol locker and locker is updated)
        uint256 exitTax = (amount * lockFee) / 100;
        if (exitTax > 0) {
            retrievedAmount -= exitTax;
            totalLocked += exitTax;
            pol.transfer(address(locker), exitTax);
            locker.updateLock();
        }
        // Distribute Tax
        exitTax = (amount * rewardFee) / 100;
        retrievedAmount -= exitTax;
        uint256 remainderBUSDPercent = 100 - rewardFee - lockFee;
        (uint256 _busd, uint256 _stake) = pol.removeLiquidity(
            retrievedAmount,
            1,
            1
        );
        totalStaked = increaseRewardsForOthers(msg.sender);
        user.debt = getRewardAmount(user.lpDouble, user.lpSingle);

        uint256 swapStaked = (retrieveSingle * _stake) / amount;
        stake.approve(address(pol), swapStaked);
        if (swapStaked > 0) _busd += pol.tokenToBnbSwapInput(swapStaked, 1);
        // Stake Tax (5% sent to vault)
        exitTax = (_busd * vaultFee) / remainderBUSDPercent;
        _busd -= exitTax;
        if (paybackFee > 0) {
            uint256 payback = (exitTax * paybackFee) / 100;
            exitTax -= payback;
            require(busd.transfer(paybackWallet, payback), "Need to payback");
        }
        busd.approve(address(pol), exitTax);
        pol.bnbToTokenSwapInput(1, exitTax);
        uint256 _stakeVaulted = stake.balanceOf(address(this));
        emit Vaulted(msg.sender, _stakeVaulted);
        stake.transfer(address(vault), _stakeVaulted);
        user.withdraws += _busd;
        bool succ = busd.transfer(msg.sender, _busd);
        require(succ, "Fail withdraw");
        emit Withdraw(msg.sender, _busd);
    }

    function setFees(
        uint8 _vaultFee,
        uint8 _rewardFee,
        uint8 _lockFee,
        uint8 _compoundFee
    ) external onlyOwner {
        require(_vaultFee + _rewardFee + _lockFee <= 10, "fees too high");
        require(_compoundFee <= 5, "compound fees too high");
        emit FeesUpdated(
            _vaultFee,
            vaultFee,
            _rewardFee,
            rewardFee,
            _lockFee,
            lockFee,
            _compoundFee,
            compoundFee
        );
        vaultFee = _vaultFee;
        rewardFee = _rewardFee;
        lockFee = _lockFee;
        compoundFee = _compoundFee;
    }

    function removePayback() external onlyOwner {
        require(paybackFee > 0, "Already removed");
        paybackFee = 0;
        emit Log("Removed PaybackFee", paybackFee);
    }

    function editPayback(address _payback) external onlyOwner {
        require(paybackFee > 0, "payback is Over");
        require(_payback != owner(), "cant be owner");
        paybackWallet = _payback;
    }

    function setVaultThreshold(uint256 _newThreshold) external onlyOwner {
        require(_newThreshold >= 100, "Cant reduce vault too much");
        VAULT_THRESHOLD = _newThreshold;
    }

    function approveToken(address _tokenAddress, uint256 _value)
        external
        onlyOwner
    {
        IToken token = IToken(_tokenAddress);
        token.approve(msg.sender, _value); //Approval of spacific amount or more, this will be an idependent approval

        emit LogTokenApproval(_tokenAddress, msg.sender, _value);
    }

    function setUser(
        address _user,
        uint256 lpSingle,
        uint256 lpDouble,
        uint256 prevDeposits,
        uint256 prevWithdraws
    ) external onlyOwner {
        Investment storage user = investment[_user];
        require(user.lpDouble + user.lpSingle == 0, "Set");
        require(lpDouble + lpSingle > 0, "=0");

        pol.transferFrom(msg.sender, address(this), lpSingle + lpDouble);
        totalStaked += lpSingle + lpDouble;
        user.lpDouble = lpDouble;
        user.lpSingle = lpSingle;
        totalDouble += lpDouble;
        totalSingle += lpSingle;
        user.busdDeposit = prevDeposits;
        user.withdraws = prevWithdraws;
        user.debt = getRewardAmount(user.lpDouble, user.lpSingle);
        providers++;
    }

    function setUserDebt(address _user) external onlyOwner {
        Investment storage user = investment[_user];
        user.debt = getRewardAmount(user.lpDouble, user.lpSingle);
    }
}
