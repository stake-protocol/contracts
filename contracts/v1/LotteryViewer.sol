//SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

import "./LotteryV2.sol";

contract LotteryDataViewer {
    LotteryV2 public lottery;

    struct RoundInfo {
        uint256 totalTickets;
        uint256 ticketsClaimed;
        uint32 winnerNumber;
        uint256 pool;
        uint256 endTime;
        uint256 burn;
        uint256 totalWinners;
        uint256 ticketValue; // in BUSD
    }

    constructor(address _lot) {
        lottery = LotteryV2(_lot);
    }

    function getCurrentRound()
        public
        view
        returns (
            uint256 current,
            uint256 totalTickets,
            uint256 ticketsClaimed,
            uint32 winnerNumber,
            uint256 pool,
            uint256 endTime,
            uint256 burn,
            uint256 totalWinners,
            uint256 ticketValue
        )
    {
        current = lottery.currentRound();
        (
            totalTickets,
            ticketsClaimed,
            winnerNumber,
            pool,
            endTime,
            burn,
            totalWinners,
            ticketValue
        ) = lottery.roundInfo(current);
    }

    function getCurrentDistribution() public view returns (uint256[8] memory) {
        uint256 current = lottery.currentRound();
        return lottery.getRoundDistribution(current);
    }
}
