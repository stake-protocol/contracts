//SPDX-License-Identifier: MIT

pragma solidity ^0.8.16;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract DummyFaucet {
    struct Accounting {
        uint256 netFaucet; // User level NetFaucetValue
        // Detail of netFaucet
        uint256 deposits; // Hard Deposits made
        uint256 rolls; // faucet Compounds
        uint256 rebaseCompounded; // RebaseCompounds
        uint256 airdrops_rcv; // Received Airdrops
        uint256 accFaucet; // accumulated but not claimed faucet due to referrals
        uint256 accRebase; // accumulated but not claimed rebases
        // Total Claims
        uint256 faucetClaims;
        uint256 rebaseClaims;
        uint256 rebaseCount;
        uint256 lastAction;
        bool done;
    }
    struct Team {
        uint256 referrals; // People referred
        address upline; // Who my referrer is
        uint256 upline_set; // Last time referrer set
        uint256 refClaimRound; // Whose turn it is to claim referral rewards
        uint256 match_bonus; // round robin distributed
        uint256 lastAirdropSent;
        uint256 airdrops_sent;
        uint256 structure; // Total users under structure
        uint256 maxDownline;
        // Team Swap
        uint256 referralsToUpdate; // Users who haven't updated if team was switched
        address prevUpline; // If updated team, who the previous upline user was to switch user's referrals
        uint256 leaders;
    }

    IERC20 public token;
    mapping(address => Team) public team;
    mapping(address => Accounting) public userStat;

    constructor(address _token) {
        token = IERC20(_token);
    }

    function deposit(uint256 amount, address _upline) public {
        Accounting storage _u = userStat[msg.sender];
        Team storage _t = team[msg.sender];

        _u.deposits += amount;
        _t.upline = _upline;
    }

    function airdrop(
        address _receiver,
        uint256 _amount,
        uint8 _level
    ) public {
        token.transferFrom(msg.sender, address(this), _amount);
        userStat[_receiver].airdrops_rcv += _amount;
    }
}
