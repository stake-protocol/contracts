// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;
import "@openzeppelin/contracts/access/Ownable.sol";
import "../../interfaces/IVault.sol";
import "../../interfaces/IToken.sol";

contract StakeVault is Ownable, IVault {
    IToken public token;

    struct TaxDistribution {
        uint256 treasury;
        uint256 foundation;
        uint256 vault;
        uint256 total;
    }

    mapping(address => bool) public whitelist;
    mapping(address => TaxDistribution) public taxDistribution;

    address public foundation;
    address public treasury;

    event AddedWhitelist(address indexed _user);
    event RemovedWhitelist(address indexed _user);

    modifier onlyWhitelist() {
        require(whitelist[msg.sender], "Not Whitelist");
        _;
    }

    constructor(address _token) {
        token = IToken(_token);
        taxDistribution[address(0)] = TaxDistribution(0, 0, 1, 1); //Tax Distribution for transfers, all for vault
        taxDistribution[address(1)] = TaxDistribution(3, 5, 5, 13); //Tax Distribution for buys
        taxDistribution[address(2)] = TaxDistribution(8, 5, 5, 18); //Tax Distribution for sells
    }

    function addWhitelist(address _user) external onlyOwner {
        whitelist[_user] = true;
        emit AddedWhitelist(_user);
    }

    function removeWhitelist(address _user) external onlyOwner {
        whitelist[_user] = false;
        emit RemovedWhitelist(_user);
    }

    function editTreasury(address _newTreasury) external onlyOwner {
        require(_newTreasury != address(0), "Cant do that");
        treasury = _newTreasury;
    }

    function editFoundation(address _newFoundation) external onlyOwner {
        require(_newFoundation != address(0), "Cant do that");
        foundation = _newFoundation;
    }

    function withdraw(uint256 amount) external onlyWhitelist {
        require(amount <= token.balanceOf(address(this)), "Not enough funds");
        IToken(token).transfer(msg.sender, amount);
    }

    function withdraw(address _receiver, uint256 amount)
        external
        onlyWhitelist
    {
        require(_receiver != address(0), "Invalid receiver");
        require(amount <= token.balanceOf(address(this)), "Not enough funds");
        IToken(token).transfer(_receiver, amount);
    }

    function spread(
        uint256 amount,
        uint8 _type,
        address custom
    ) external override onlyWhitelist returns (bool) {
        require(_type > 0, "Wrong Implementation");

        TaxDistribution storage _spread = _type < 4
            ? taxDistribution[address(uint160(_type - 1))]
            : taxDistribution[custom];
        if (_spread.total == 0)
            // if custom taxes have not been setup, use transfers Distribution
            _spread = taxDistribution[address(0)];
        require(amount <= token.balanceOf(address(this)), "Insufficient funds");
        receiveFunds(
            amount,
            _spread.vault,
            _spread.foundation,
            _spread.treasury
        );
        return true;
    }

    ///@notice it sends funds from the user/contract calling the function and distributes the token into the three categories
    ///@param _received Amount of tokens to work with
    ///@param _vault Percentage that stays in the vault
    ///@param _collateral Percentage that goes to the
    function receiveFunds(
        uint256 _received,
        uint256 _vault,
        uint256 _collateral,
        uint256 _treasury
    ) internal {
        uint256 total = _vault + _collateral + _treasury;
        uint256 vaultAmount = (_received * _vault) / total;
        uint256 collateralAmount = (_received * _collateral) / total;
        uint256 _trove = _received - vaultAmount - collateralAmount;

        // token.transfer(address(this), vaultAmount); // redundant and not needed since vault holds all the funds
        token.transfer(foundation, collateralAmount);
        token.transfer(treasury, _trove);
    }

    function taxCustomDistribution(
        address _user,
        uint256 _total,
        uint256 _treasure,
        uint256 _foundation,
        uint256 _vault
    ) external onlyOwner {
        require(_treasure + _foundation + _vault == _total, "Fee mismatch");
        taxDistribution[_user] = TaxDistribution(
            _treasure,
            _foundation,
            _vault,
            _total
        );
    }
}
