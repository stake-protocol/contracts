// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract DebtPool is Ownable {
    struct Debt {
        uint256 amountOwed;
        uint256 amountWithdrawn;
    }

    uint256 public totalOwed;
    uint256 public payback;
    uint256 public totalWithdrawn;
    uint256 public usersOwed;

    IERC20 public immutable busd;

    mapping(address => Debt) public debt;

    constructor(address _busd) {
        busd = IERC20(_busd);
    }

    function pendingDebt() external view returns (uint256 _pendingDebt) {
        Debt storage user = debt[msg.sender];
        _pendingDebt = user.amountOwed - user.amountWithdrawn;
    }

    function pendingClaim(address _user)
        public
        view
        returns (uint256 _claimable)
    {
        Debt storage user = debt[_user];
        uint256 pendingPayback = pendingAllocation();
        uint256 availablePayback = ((payback + pendingPayback) *
            user.amountOwed) / totalOwed;
        if (availablePayback > user.amountWithdrawn)
            _claimable = availablePayback - user.amountWithdrawn;
        else _claimable = 0;
    }

    function pendingAllocation() public view returns (uint256 _toDistribute) {
        uint256 currentBalance = busd.balanceOf(address(this));
        if (currentBalance > payback - totalWithdrawn)
            return currentBalance + totalWithdrawn - payback;
        return 0;
    }

    function setUser(address _user, uint256 _amountOwed) public onlyOwner {
        Debt storage userDebt = debt[_user];
        if (userDebt.amountOwed == 0) usersOwed++;
        userDebt.amountOwed = _amountOwed;
        totalOwed += _amountOwed;
    }

    function addMultipleUsers(
        address[] calldata _users,
        uint256[] calldata _amounts
    ) external onlyOwner {
        require(_users.length > 0, "Cant be zero");
        require(_users.length == _amounts.length, "Invalid amounts");
        for (uint8 i = 0; i < _users.length; i++) {
            setUser(_users[i], _amounts[i]);
        }
    }

    function claim() external {
        distribute();
        Debt storage user = debt[msg.sender];
        uint256 userOwed = user.amountOwed - user.amountWithdrawn;
        require(userOwed > 0, "Not owed");
        uint256 toClaim = ((payback * user.amountOwed) / totalOwed) -
            user.amountWithdrawn;
        user.amountWithdrawn += toClaim;
        totalWithdrawn += toClaim;
        require(toClaim <= busd.balanceOf(address(this)), "Not enough funds");
        require(busd.transfer(msg.sender, toClaim), "Failed Transfer");
    }

    function distribute() public {
        uint256 toAllocate = pendingAllocation();
        if (toAllocate == 0) return;
        payback += toAllocate;
        if (payback > totalOwed) payback = totalOwed;
    }

    function extractBEP20(address _token) external onlyOwner {
        IERC20 token = IERC20(_token);
        uint256 totalBalance = token.balanceOf(address(this));
        token.transfer(msg.sender, totalBalance);
    }
}
