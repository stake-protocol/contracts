// SPDX-License-Identifier: MIT
pragma solidity ^0.8.16;

// VRF
import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";
import "@chainlink/contracts/src/v0.8/interfaces/VRFCoordinatorV2Interface.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

import "../../interfaces/IFountain.sol";

import "../../interfaces/IFaucet.sol";

abstract contract DevFaucetIntegration is Ownable {
    uint256 public devAmount;
    event UpdateCut(uint256 _old, uint256 _new);

    IFaucet public faucet;

    mapping(address => bool) public hasFaucet;

    constructor(address _faucet) {
        faucet = IFaucet(_faucet);
    }

    function updateDevCut(uint256 _newAmount) external onlyOwner {
        require(_newAmount < 50 * 1e10, "Invalid");
        emit UpdateCut(devAmount, _newAmount);
        devAmount = _newAmount;
    }

    function checkAndSetFaucet() public returns (bool) {
        if (!hasFaucet[msg.sender]) {
            (, address _upline, , , , , , , , , , ) = faucet.team(msg.sender);
            if (_upline == address(0)) return false;
            hasFaucet[msg.sender] = true;
        }
        return true;
    }
}

/**
 * @title  Bitcrush's lottery game
 * @author Bitcrush Devs
 * @notice Simple Lottery contract, matches winning numbers from left to right.
 *
 *
 *
 */
contract LotteryV2 is VRFConsumerBaseV2, ReentrancyGuard, DevFaucetIntegration {
    // Libraries
    using SafeMath for uint256;
    using SafeERC20 for ERC20;
    using SafeERC20 for ERC20Burnable;

    // Contracts
    // Contracts
    ERC20Burnable public immutable token;
    ERC20 public immutable busd;
    IFountain public pol;
    address public devAddress; //Address to send Ticket cut to.

    // Data Structures
    struct RoundInfo {
        uint256 totalTickets;
        uint256 ticketsClaimed;
        uint32 winnerNumber;
        uint256 pool;
        uint256 endTime;
        uint256[8] distribution;
        uint256 burn;
        uint256 totalWinners;
        uint256[6] winnerDigits;
        uint256 ticketValue; // in BUSD
    }

    struct TicketView {
        uint256 id;
        uint256 round;
        uint256 ticketNumber;
    }

    struct NewTicket {
        uint32 ticketNumber;
        uint256 round;
    }

    struct ClaimRounds {
        uint256 roundId;
        uint256 nonWinners;
        uint256 winners;
    }

    struct RoundTickets {
        uint256 totalTickets;
        uint256 firstTicketId;
    }

    struct Claimer {
        address claimer;
        uint256 percent;
    }
    // This struct defines the values to be stored on a per Round basis
    struct BonusCoin {
        address bonusToken;
        uint256 bonusAmount;
        uint256 bonusClaimed;
        uint256 bonusMaxPercent; // accumulated percentage of winners for a round
    }

    struct Partner {
        uint256 spread;
        uint256 id;
        bool set;
    }

    // VRF Specific
    bytes32 internal keyHashVRF;
    VRFCoordinatorV2Interface internal COORDINATOR;
    uint64 internal immutable susId;
    uint32 internal callbackGasLimit = 900_000;
    uint16 internal requestConfirmations = 3;

    /// Timestamp Specific
    uint256 constant SECONDS_PER_DAY = 24 hours;
    uint256 constant SECONDS_PER_HOUR = 1 hours;
    uint256 constant SECONDS_PER_MINUTE = 60;
    int256 constant OFFSET19700101 = 2440588;
    // CONSTANTS
    uint256 constant ONE100PERCENT = 1e8;
    uint256 constant ONE__PERCENT = 1e10;
    uint256 constant PERCENT_BASE = 1e12;
    uint32 constant WINNER_BASE = 1000000; //6 digits are necessary
    uint256 constant distributionShare = 5 * ONE__PERCENT;
    // Variables
    bool public currentIsActive = false;
    bool public pause = false;
    uint256 public currentRound = 0;
    uint256 public roundEnd;
    uint256 public ticketValue = 4 ether; //Value of Ticket value in BUSD

    uint256 public burnThreshold = 10 * ONE__PERCENT;
    // Fee Distributions
    /// @dev these values are used with PERCENT_BASE as 100%
    uint256 public match6 = 28_30 * ONE100PERCENT;
    uint256 public match5 = 16_10 * ONE100PERCENT;
    uint256 public match4 = 6 * ONE__PERCENT;
    uint256 public match3 = 3_20 * ONE100PERCENT;
    uint256 public match2 = 2 * ONE__PERCENT;
    uint256 public match1 = 1 * ONE__PERCENT;
    uint256 public noMatch = 80 * ONE100PERCENT;
    uint256 public burn = 12_60 * ONE100PERCENT;
    uint256 public claimFee = 75 * ONE100PERCENT; // This is deducted from the no winners 2%
    // Mappings
    mapping(uint256 => RoundInfo) public roundInfo; //Round Info
    mapping(uint256 => BonusCoin) public bonusCoins; //Track bonus partner coins to distribute
    mapping(uint256 => mapping(uint256 => uint256)) public holders; // ROUND => DIGITS => #OF HOLDERS
    mapping(address => uint256) public exchangeableTickets;
    mapping(address => Partner) public partnerSplit;
    // NEW IMPLEMENTATION
    mapping(address => mapping(uint256 => NewTicket)) public userNewTickets; // User => ticketId => ticketData
    mapping(address => mapping(uint256 => RoundTickets))
        public userRoundTickets; // User => Last created ticket Id
    mapping(address => uint256) public userTotalTickets; // User => Last created ticket Id
    mapping(address => uint256) public userLastTicketClaimed; // User => Last ticket claimed Id
    mapping(address => uint256) public bonusTokenIndex;
    mapping(uint256 => uint256) public initPool;

    mapping(uint256 => Claimer) private claimers; // Track claimers to autosend claiming Bounty

    mapping(address => bool) public thirdPartyTokenBuyers; // Allowed to buy ticket
    mapping(address => bool) public operators; //Operators allowed to execute certain functions

    address[] public partners;
    address[] public bonusAddresses;

    uint8[] public endHours = [18];
    uint8 public endHourIndex;
    // EVENTS
    event FundedBonusCoins(
        address indexed _partner,
        uint256 _amount,
        uint256 _startRound,
        uint256 _numberOfRounds
    );
    event FundPool(uint256 indexed _round, uint256 _amount);
    event OperatorChanged(address indexed operators, bool active_status);
    event RoundStarted(
        uint256 indexed _round,
        address indexed _starter,
        uint256 _timestamp
    );
    event TicketBought(
        uint256 indexed _round,
        address indexed _user,
        uint256 _ticketAmounts
    );
    event OtherTokenBought(
        address indexed buyer,
        uint256 indexed _round,
        address indexed _user,
        uint256 _ticketAmounts
    );
    event SelectionStarted(
        uint256 indexed _round,
        address _caller,
        uint256 _requestId
    );
    event WinnerPicked(
        uint256 indexed _round,
        uint256 _winner,
        uint256 _requestId
    );
    event TicketsRewarded(address _rewardee, uint256 _ticketAmount);
    event UpdateTicketValue(uint256 _timeOfUpdate, uint256 _newValue);
    event PartnerUpdated(address indexed _partner);
    event PartnerSold(
        address indexed _partner,
        uint256 indexed _round,
        uint256 _tickets
    );
    event TicketsClaimed(
        address indexed _user,
        uint256 amount,
        uint256[] matches
    );
    event PercentagesChanged(
        address indexed owner,
        string percentName,
        uint256 newPercent
    );
    event LogEvent(uint256 _data, string _annotation);
    event AuditLog(address _data, string _annotation);
    // MODIFIERS
    modifier operatorOnly() {
        require(operators[msg.sender] == true || msg.sender == owner(), "OO"); // dev: Operator Only
        _;
    }

    /// @dev Select the appropriate VRF Coordinator and LINK Token addresses
    constructor(
        address _token,
        address _pol,
        address _busd,
        uint64 suscriptionId,
        address _coordinator,
        address _faucet
    ) VRFConsumerBaseV2(_coordinator) DevFaucetIntegration(_faucet) {
        // VRF Init
        if (block.chainid == 56) {
            keyHashVRF = 0x114f3da0a805b6a67d6e9cd2ec746f7028f1b7376365af575cfea3550dd1aa04; // MAINNET HASH 200gwei
        } else if (block.chainid == 97) {
            keyHashVRF = 0xd4bb89654db74673a187bd804519e65e3f71a52bc55f11da7601a13dcf505314; // TESTNET HASH
        }
        COORDINATOR = VRFCoordinatorV2Interface(_coordinator);
        pol = IFountain(_pol);
        token = ERC20Burnable(_token);
        busd = ERC20(_busd);
        susId = suscriptionId;
        operators[msg.sender] = true;

        devAddress = msg.sender;
        bonusAddresses.push(_token);
        bonusTokenIndex[_token] = 0;
        devAmount = 10 * ONE__PERCENT;
    }

    // External functions
    /// @notice Buy Tickets to participate in current round from a partner
    /// @param _ticketNumbers takes in an array of uint values as the ticket number to buy
    /// @param _partnerId the id of the partner to send the funds to if 0, no partner is checked.
    function buyWithToken(uint32[] calldata _ticketNumbers, uint256 _partnerId)
        external
        nonReentrant
    {
        /// get amount of tickets to Buy at ticketPrice
        uint256 toBuy = _ticketNumbers.length;
        require(toBuy > 0 && toBuy <= 100, "Limit");
        require(
            currentIsActive == true && block.timestamp < roundEnd,
            "Not ok"
        );
        // Check if User has funds for ticket
        //get price from POL
        uint256 price = roundInfo[currentRound].ticketValue * toBuy;
        price = pol.getBnbToTokenInputPrice(price);
        token.safeTransferFrom(msg.sender, address(this), price);
        // Get devCut in BUSD (from POL)
        uint256 devCut = (price * devAmount) / PERCENT_BASE;
        price -= devCut;
        token.approve(address(pol), devCut);
        pol.tokenToBnbSwapInput(devCut, 1);
        devCut = busd.balanceOf(address(this));
        // Split with partner when necessary, funds sent from this contract
        splitWithPartner(devCut, _partnerId, true, _ticketNumbers.length);

        // Create Tickets
        // Add Tickets to respective Mappings
        for (uint256 i = 0; i < _ticketNumbers.length; i++) {
            createTicket(msg.sender, _ticketNumbers[i], currentRound, i);
        }
        increaseVals(price, toBuy, msg.sender);
    }

    /// @notice buy tickets using BUSD
    /// @param _ticketNumbers array of number of tickets to buy
    /// @param _partnerId the id of the partner to send the funds to if 0, no partner is checked.
    function buyWithBUSD(uint24[] calldata _ticketNumbers, uint256 _partnerId)
        external
        nonReentrant
    {
        /// get amount of tickets to Buy at ticketPrice
        uint256 toBuy = _ticketNumbers.length;
        require(toBuy > 0 && toBuy <= 100, "Limit");
        require(
            currentIsActive == true && block.timestamp < roundEnd,
            "Round not active"
        );
        uint256 price = roundInfo[currentRound].ticketValue * toBuy;
        /// send cut to TreasuryDevWallet & partners
        uint256 devCut = (price * devAmount) / PERCENT_BASE;
        splitWithPartner(devCut, _partnerId, false, _ticketNumbers.length);
        price -= devCut;
        /// transfer BUSD to contract
        busd.safeTransferFrom(msg.sender, address(this), price);
        /// convert rest to TOKEN
        price = busd.balanceOf(address(this));
        busd.approve(address(pol), price);
        uint256 tokenSwapped = pol.bnbToTokenSwapInput(1, price);
        /// BUYS AND SETS TICKETS FOR MSG.SENDER
        // Create Tickets
        for (uint8 i = 0; i < toBuy; i++) {
            createTicket(msg.sender, _ticketNumbers[i], currentRound, i);
        }
        increaseVals(tokenSwapped, toBuy, msg.sender);
    }

    /// @notice sets the tickets from partner contract,
    /// @param _ticketNumbers array of number of tickets to buy
    /// @param funded the amount of STAKE added to the Pool
    function buyWithPartnerToken(
        uint24[] calldata _ticketNumbers,
        uint256 funded,
        address _buyer
    ) external nonReentrant {
        // caller is approved address
        require(thirdPartyTokenBuyers[msg.sender], "IC"); // dev: Invalid Caller
        // We're trusting that previous contract made the necessary checks for
        // the tickets they are buying in and sent STAKE to pool
        // and sent the dev cut to dev

        roundInfo[currentRound].pool += funded;
        emit FundPool(currentRound, funded);

        uint256 toBuy = _ticketNumbers.length;
        // create tickets and sets them for the required user
        if (userRoundTickets[msg.sender][currentRound].firstTicketId == 0) {
            userRoundTickets[msg.sender][currentRound]
                .firstTicketId = userTotalTickets[msg.sender].add(1);
        }
        userTotalTickets[msg.sender] += toBuy;
        userRoundTickets[msg.sender][currentRound].totalTickets += toBuy;
        for (uint8 i = 0; i < toBuy; i++) {
            createTicket(_buyer, _ticketNumbers[i], currentRound, i);
        }
        emit OtherTokenBought(msg.sender, currentRound, _buyer, toBuy);
        roundInfo[currentRound].totalTickets += toBuy;
    }

    /// @notice add/remove/edit partners
    /// @param _partnerAddress the address where funds will go to.
    /// @param _split the negotiated split percentage. Value goes from 0 to 90.
    /// @dev their ID doesn't change, nor is it removed once partnership ends.
    function editPartner(
        address _partnerAddress,
        uint8 _split,
        bool tp
    ) external operatorOnly {
        if (tp) {
            thirdPartyTokenBuyers[_partnerAddress] = _split > 0;
        } else {
            require(_split <= 90, "SH"); //dev: Split is too high.
            Partner storage _p = partnerSplit[_partnerAddress];
            if (_p.id == 0) {
                partners.push(_partnerAddress);
                _p.id = partners.length;
            }
            _p.spread = _split;
            _p.set = _split > 0;
            emit PartnerUpdated(_partnerAddress);
        }
    }

    /// @notice retrieve a provider wallet ID
    /// @param _checkAddress the address to check
    /// @return _id the ID of the provider
    function getProviderId(address _checkAddress)
        external
        view
        returns (uint256 _id)
    {
        Partner storage partner = partnerSplit[_checkAddress];
        if (!partner.set) return 0;
        _id = partner.id;
    }

    /// @notice Give Redeemable Tickets to a particular user
    /// @param _rewardee Address the tickets will be awarded to
    /// @param ticketAmount number of tickets awarded
    function rewardTicket(address _rewardee, uint256 ticketAmount)
        external
        operatorOnly
    {
        exchangeableTickets[_rewardee] += ticketAmount;
        emit TicketsRewarded(_rewardee, ticketAmount);
    }

    /// @notice Exchange awarded tickets for the current round
    /// @param _ticketNumbers array of numbers to add to the caller as tickets
    function exchangeForTicket(uint32[] calldata _ticketNumbers) external {
        require(currentIsActive == true && block.timestamp < roundEnd, "RNA"); // dev: Round Not Active
        uint256 toExchange = _ticketNumbers.length;
        require(
            toExchange > 0 && toExchange <= exchangeableTickets[msg.sender],
            "IEA"
        ); //dev: INVALID EXCHANGE AMOUNT
        if (userRoundTickets[msg.sender][currentRound].firstTicketId == 0) {
            userRoundTickets[msg.sender][currentRound]
                .firstTicketId = userTotalTickets[msg.sender].add(1);
        }
        for (uint256 exchange = 0; exchange < toExchange; exchange++) {
            createTicket(
                msg.sender,
                _ticketNumbers[exchange],
                currentRound,
                exchange
            );
            exchangeableTickets[msg.sender] -= 1;
        }
        userTotalTickets[msg.sender] += toExchange;
        roundInfo[currentRound].totalTickets += toExchange;
        emit TicketBought(currentRound, msg.sender, toExchange);
    }

    /// @notice Start of new Round. This function is only needed for the first round, next rounds will be automatically started once the winner number is received
    function firstStart() external operatorOnly {
        require(currentRound == 0); // dev: First Round only
        calcNextHour();
        startRound();
        // Rollover all of pool zero at start
        initPool[currentRound] = roundInfo[0].pool;
        roundInfo[currentRound].pool = roundInfo[0].pool;
        roundInfo[currentRound].endTime = roundEnd;
        roundInfo[currentRound].distribution = [
            noMatch,
            match1,
            match2,
            match3,
            match4,
            match5,
            match6,
            burn
        ];
        roundInfo[currentRound].ticketValue = ticketValue;
        pause = false;
    }

    /// @notice Ends current round
    /// @dev WIP - the end of the round will always happen at set intervals
    function endRound() external {
        require(
            currentIsActive == true &&
                block.timestamp > roundInfo[currentRound].endTime,
            "RA"
        ); // dev: Round Active
        currentIsActive = false;
        calcNextHour();
        RoundInfo storage nextRound = roundInfo[currentRound + 1];
        nextRound.endTime = roundEnd;
        claimers[currentRound] = Claimer(msg.sender, 0);
        // Request Random Number for Winner
        uint256 rqId = COORDINATOR.requestRandomWords(
            keyHashVRF,
            susId,
            requestConfirmations,
            callbackGasLimit,
            1
        );
        emit SelectionStarted(currentRound, msg.sender, rqId);
    }

    /// @notice Add or remove operator
    /// @param _operator address to add / remove operator
    function toggleOperator(address _operator) external operatorOnly {
        operators[_operator] = !operators[_operator];
        emit OperatorChanged(_operator, operators[msg.sender]);
    }

    // SETTERS
    /// @notice Change the claimer's fee
    /// @param _fee the value of the new fee
    /// @dev Fee cannot be greater than noMatch percentage ( since noMatch percentage is the amount given out to nonWinners )
    function setClaimerFee(uint256 _fee) external onlyOwner {
        require(_fee.mul(ONE100PERCENT) < noMatch, "Invalid fee amount");
        claimFee = _fee.mul(ONE100PERCENT);
        emit PercentagesChanged(
            msg.sender,
            "claimFee",
            _fee.mul(ONE100PERCENT)
        );
    }

    /// @notice Set the token that will be used as a Bonus for a particular round
    /// @param _partnerToken Token address
    /// @param _amount amount of tokens to distribute
    /// @param _round round where this token applies
    /// @param _roundAmount amount of rounds to do the giveaway from
    function setBonusCoin(
        address _partnerToken,
        uint256 _amount,
        uint256 _round,
        uint256 _roundAmount
    ) external operatorOnly {
        require(_roundAmount > 0 && _round > currentRound, "NPR"); // dev: "Need at least 1" || No past rounds
        require(
            _partnerToken != address(0) &&
                bonusCoins[_round].bonusToken == address(0)
        ); // dev: "issue with address"
        if (bonusTokenIndex[_partnerToken] == 0) {
            bonusTokenIndex[_partnerToken] = bonusAddresses.length;
            bonusAddresses.push(_partnerToken);
        }
        ERC20 bonusToken = ERC20(_partnerToken);
        uint256 spreadAmount = _amount.div(_roundAmount);
        uint256 totalAmount = spreadAmount.mul(_roundAmount); //get the actual total to take into account division issues
        bonusToken.safeTransferFrom(msg.sender, address(this), totalAmount);
        for (
            uint256 rounds = _round;
            rounds < _round.add(_roundAmount);
            rounds++
        ) {
            require(bonusCoins[rounds].bonusToken == address(0), "BS"); // dev: Bonus Set already
            // Uses the claimFee as the base since that will always be distributed to the claimer.
            bonusCoins[rounds] = BonusCoin(_partnerToken, spreadAmount, 0, 0);
        }
        emit FundedBonusCoins(_partnerToken, _amount, _round, _roundAmount);
    }

    /// @notice Set the ticket value
    /// @param _newValue the new value of the ticket
    /// @dev Ticket value MUST BE IN WEI format, minimum is left as greater than 1 due to the deflationary nature of CRUSH
    function setTicketValue(uint256 _newValue) external onlyOwner {
        require(_newValue >= 1 ether); // dev: cant give for free
        ticketValue = _newValue;
        emit UpdateTicketValue(block.timestamp, _newValue);
    }

    /// @notice Edit the times array
    /// @param _newTimes Array of hours when Lottery will end
    /// @dev adding a sorting algorithm would be nice but honestly we have too much going on to add that in. So help us out and add your times sorted
    function setEndHours(uint8[] calldata _newTimes) external operatorOnly {
        require(_newTimes.length > 0 && _newTimes.length <= 2, "T"); // dev: algo only works with 2
        for (uint256 i = 0; i < _newTimes.length; i++) {
            require(_newTimes[i] < 24, "T1"); // dev: time needs to be less than 24
            if (i > 0) require(_newTimes[i] > _newTimes[i - 1], "ST"); // dev: HELP SORT THE TIMES
        }
        endHours = _newTimes;
    }

    /// @notice Setup the burn threshold
    /// @param _threshold new threshold in percent amount
    /// @dev setting the minimum threshold as 0 will always burn, setting max as 50
    function setBurnThreshold(uint256 _threshold) external onlyOwner {
        require(_threshold <= 50, "oor"); // dev: Out of range
        burnThreshold = _threshold * ONE__PERCENT;
    }

    /// @notice toggle pause state of lottery
    /// @dev if the round is over and the lottery is unpaused then the round is started
    function togglePauseStatus() external onlyOwner {
        pause = !pause;
        if (currentIsActive == false && !pause) {
            startRound();
        }
        emit LogEvent(pause ? 1 : 0, "PSC"); //dev: Pause Status changed
    }

    /// @notice Destroy contract and retrieve funds
    /// @dev This function is meant to retrieve funds in case of non usage and/or upgrading in the future.
    function crushTheContract() external onlyOwner {
        require(pause && block.timestamp > roundEnd.add(2649600), "WNA"); // dev: Paused OR wait for long inactivity time
        // Transfer Held CRUSH
        token.safeTransfer(msg.sender, token.balanceOf(address(this)));
        // Transfer held Link
        // Transfer Held Bonus Tokens
        for (uint256 i = 1; i < bonusAddresses.length; i++) {
            ERC20 bonusToken = ERC20(bonusAddresses[i]);
            bonusToken.safeTransfer(
                msg.sender,
                bonusToken.balanceOf(address(this))
            );
        }
        selfdestruct(payable(msg.sender));
    }

    /// @notice Set the distribution percentage amounts... all amounts must be given for this to work
    /// @param _newDistribution array of distribution amounts
    /// @dev we expect all values to sum 100 and that all items are given. The new distribution only applies to next rounds
    /// @dev all values are in the one onehundreth percentile amount.
    /// @dev expected order [ jackpot, match5, match4, match3, match2, match1, noMatch, burn]
    function setDistributionPercentages(uint256[] calldata _newDistribution)
        external
        onlyOwner
    {
        require(_newDistribution[7] > 0 && _newDistribution.length == 8, "WC"); // dev: Wrong config
        match6 = _newDistribution[0].mul(ONE100PERCENT);
        match5 = _newDistribution[1].mul(ONE100PERCENT);
        match4 = _newDistribution[2].mul(ONE100PERCENT);
        match3 = _newDistribution[3].mul(ONE100PERCENT);
        match2 = _newDistribution[4].mul(ONE100PERCENT);
        match1 = _newDistribution[5].mul(ONE100PERCENT);
        noMatch = _newDistribution[6].mul(ONE100PERCENT);
        burn = _newDistribution[7].mul(ONE100PERCENT);
        require(
            match6
                .add(match5)
                .add(match4)
                .add(match3)
                .add(match2)
                .add(match1)
                .add(noMatch)
                .add(burn) <= PERCENT_BASE
        ); // dev: "Numbers don't add up"
        emit PercentagesChanged(msg.sender, "jackpot", match6);
        emit PercentagesChanged(msg.sender, "match5", match5);
        emit PercentagesChanged(msg.sender, "match4", match4);
        emit PercentagesChanged(msg.sender, "match3", match3);
        emit PercentagesChanged(msg.sender, "match2", match2);
        emit PercentagesChanged(msg.sender, "match1", match1);
        emit PercentagesChanged(msg.sender, "noMatch", noMatch);
        emit PercentagesChanged(msg.sender, "burnPercent", burn);
    }

    /// @notice Claim all tickets for selected Rounds
    /// @param _rounds the round info to look at
    /// @param _ticketIds array of ticket Ids that will be claimed
    /// @param _matches array of match per ticket Id
    /// @dev _ticketIds and _matches have to be same length since they are matched 1-to-1
    function claimAllPendingTickets(
        ClaimRounds[] calldata _rounds,
        uint256[] calldata _ticketIds,
        uint256[] calldata _matches,
        bool toFaucet
    ) external {
        require(
            _ticketIds.length == _matches.length && _rounds.length > 0,
            "AI"
        ); // dev: Array lengths dont match || rounds length is zero
        uint256 claims = userLastTicketClaimed[msg.sender];
        require(
            _rounds[0].roundId > userNewTickets[msg.sender][claims].round,
            "NP"
        ); // dev: Cant claim past rounds
        uint256 ticketIdCounter;
        uint256[] memory partnerBonusTokens = new uint256[](
            bonusAddresses.length
        );
        for (uint256 i = 0; i < _rounds.length; i++) {
            uint256 _idCounter;
            uint256 _claims;
            uint256[3] memory rewarded;
            (_idCounter, _claims, rewarded) = calculateRewardsPerRound(
                _rounds[i],
                ticketIdCounter,
                claims,
                _ticketIds,
                _matches
            );
            partnerBonusTokens[0] += rewarded[0];
            partnerBonusTokens[rewarded[2]] += rewarded[1];
            ticketIdCounter += _idCounter;
            claims += _claims;
        }
        userLastTicketClaimed[msg.sender] = claims;
        if (partnerBonusTokens[0] > 0) {
            transferToken(msg.sender, partnerBonusTokens[0], toFaucet);
        }
        if (bonusAddresses.length > 1) {
            for (uint256 p = 1; p < bonusAddresses.length; p++) {
                if (partnerBonusTokens[p] == 0) continue;
                ERC20 bonusContract = ERC20(bonusAddresses[p]);
                uint256 availableFunds = bonusContract.balanceOf(address(this));
                if (availableFunds > 0) {
                    if (partnerBonusTokens[p] > availableFunds)
                        bonusContract.safeTransfer(msg.sender, availableFunds);
                    else
                        bonusContract.safeTransfer(
                            msg.sender,
                            partnerBonusTokens[p]
                        );
                }
            }
        }
        emit TicketsClaimed(msg.sender, claims, _matches);
    }

    function calculateRewardsPerRound(
        ClaimRounds calldata _round,
        uint256 _ticketCounter,
        uint256 claims,
        uint256[] calldata _ticketIds,
        uint256[] calldata _matches
    )
        internal
        view
        returns (
            uint256 _counter,
            uint256 _totalClaimed,
            uint256[3] memory _rewards
        )
    {
        require(
            _round.roundId < currentRound &&
                _round.nonWinners.add(_round.winners) ==
                userRoundTickets[msg.sender][_round.roundId].totalTickets &&
                _round.winners <= _ticketIds.length,
            "TCI"
        ); // dev: Issue with tickets being claimed. Current Round, Missing ticket info , round must have all tickets
        uint256 roundId = _round.roundId;
        uint256 winners = _round.winners;
        uint256 nonWinners = _round.nonWinners;
        RoundInfo storage roundChecked = roundInfo[roundId];
        BonusCoin storage roundBonus = bonusCoins[roundId];
        uint256 mr_tc = getNoMatchAmount(roundId); //match reduction then becomes ticketCounter
        if (nonWinners > 0) {
            _rewards[0] += (
                getFraction(
                    roundChecked.pool,
                    mr_tc.mul(nonWinners),
                    roundChecked
                        .totalTickets
                        .sub(roundChecked.totalWinners)
                        .mul(PERCENT_BASE)
                )
            );
            if (roundBonus.bonusToken != address(0)) {
                _rewards[2] = bonusTokenIndex[roundBonus.bonusToken];
                _rewards[1] += (
                    nonWinners.mul(
                        getBonusReward(
                            roundChecked.totalTickets.sub(
                                roundChecked.totalWinners
                            ),
                            roundBonus,
                            mr_tc
                        )
                    )
                );
            }
        }
        mr_tc = _ticketCounter;
        if (winners > 0) {
            for (uint256 j = 0; j < winners; j++) {
                uint256 ticketId = j + mr_tc;
                ticketId = _ticketIds[ticketId];
                if (j == 0 && mr_tc == 0) require(ticketId > claims, "TAC"); // dev: Tickets already claimed
                if (j > 0 || mr_tc > 0)
                    require(ticketId > _ticketIds[j.add(mr_tc).sub(1)], "ST"); // dev: Sort Tickets, can only claim once
                uint256 matchBatch = _matches[j.add(mr_tc)];

                _rewards[0] += checkTicketRequirements(
                    ticketId,
                    matchBatch,
                    roundChecked
                );
                if (roundBonus.bonusToken != address(0)) {
                    _rewards[2] = bonusTokenIndex[roundBonus.bonusToken];
                    _rewards[1] += getBonusReward(
                        holders[roundId][
                            roundChecked.winnerDigits[matchBatch - 1]
                        ],
                        roundBonus,
                        roundChecked.distribution[matchBatch]
                    );
                }
            }
            _counter = mr_tc + winners;
        }
        _totalClaimed = nonWinners + winners;
    }

    function transferToken(
        address _user,
        uint256 _amount,
        bool _toFaucet
    ) internal {
        if (_toFaucet && checkAndSetFaucet()) {
            token.approve(address(faucet), 1_000_000 ether);
            faucet.airdrop(_user, _amount, 0);
        } else {
            token.safeTransfer(_user, _amount);
        }
    }

    // External functions that are view
    /// @notice Get Tickets for the caller for during a specific round
    /// @param _round The round to query
    function getUserRoundTickets(uint256 _round, address _user)
        external
        view
        returns (NewTicket[] memory)
    {
        RoundTickets storage roundReview = userRoundTickets[_user][_round];
        NewTicket[] memory tickets = new NewTicket[](roundReview.totalTickets);
        for (uint256 i = 0; i < roundReview.totalTickets; i++)
            tickets[i] = userNewTickets[_user][
                roundReview.firstTicketId.add(i)
            ];
        return tickets;
    }

    /// @notice Get a specific round's distribution percentages
    /// @param _round the round to check
    /// @dev this is necessary since solidity doesn't return the nested array in a struct when calling the variable containing the struct
    function getRoundDistribution(uint256 _round)
        external
        view
        returns (uint256[8] memory distribution)
    {
        distribution[0] = roundInfo[_round].distribution[0];
        distribution[1] = roundInfo[_round].distribution[1];
        distribution[2] = roundInfo[_round].distribution[2];
        distribution[3] = roundInfo[_round].distribution[3];
        distribution[4] = roundInfo[_round].distribution[4];
        distribution[5] = roundInfo[_round].distribution[5];
        distribution[6] = roundInfo[_round].distribution[6];
        distribution[7] = roundInfo[_round].distribution[7];
    }

    /// @notice Get all Claimable Tickets
    /// @return TicketView array
    /// @dev this is specific to UI, returns ID and ROUND number in order to make the necessary calculations.
    function ticketsToClaim() external view returns (TicketView[] memory) {
        uint256 claimableTickets = userTotalTickets[msg.sender].sub(
            userLastTicketClaimed[msg.sender]
        );
        if (claimableTickets == 0) return new TicketView[](0);
        TicketView[] memory pendingTickets = new TicketView[](claimableTickets);
        for (uint256 i = 0; i < claimableTickets; i++) {
            NewTicket storage viewTicket = userNewTickets[msg.sender][
                userLastTicketClaimed[msg.sender].add(i + 1)
            ];
            pendingTickets[i] = TicketView(
                userLastTicketClaimed[msg.sender].add(i + 1),
                viewTicket.round,
                viewTicket.ticketNumber
            );
        }
        return pendingTickets;
    }

    // Public functions
    /// @notice Add funds to pool directly, only applies funds to currentRound
    /// @param _amount the amount of TOKEN to transfer from current account to current Round
    /// @dev Approve needs to be run beforehand so the transfer can succeed.
    function addToPool(uint256 _amount, bool _affectsBurn) public {
        token.safeTransferFrom(msg.sender, address(this), _amount);
        roundInfo[currentRound].pool = _amount;
        if (!_affectsBurn) {
            initPool[currentRound] += _amount;
        }
        emit FundPool(currentRound, _amount);
    }

    // Internal functions
    function increaseVals(
        uint256 _pool,
        uint256 _tickets,
        address _user
    ) internal {
        /// add to Pool
        roundInfo[currentRound].pool += _pool;
        roundInfo[currentRound].totalTickets += _tickets;
        userRoundTickets[_user][currentRound].totalTickets += _tickets;
        if (userRoundTickets[_user][currentRound].firstTicketId == 0) {
            userRoundTickets[_user][currentRound]
                .firstTicketId = userTotalTickets[_user].add(1);
        }
        userTotalTickets[_user] += _tickets;
        emit FundPool(currentRound, _pool);
        emit TicketBought(currentRound, _user, _tickets);
    }

    /// @notice Split with Partner
    /// @param devCut the amount to split
    /// @param _partnerId Id of the partner to send the funds to
    /// @param fromSelf if tokens are sent from contract or from the caller - used for third party token buys
    /// @dev _partnerId is base 1 and should be base 0
    function splitWithPartner(
        uint256 devCut,
        uint256 _partnerId,
        bool fromSelf,
        uint256 _tickets
    ) internal {
        if (_partnerId > 0 && _partnerId <= partners.length) {
            address partner = partners[_partnerId - 1];
            Partner storage _p = partnerSplit[partner];
            if (_p.set) {
                uint256 toPartner = (devCut * _p.spread) / 100;
                if (fromSelf) {
                    busd.safeTransfer(partner, toPartner);
                    busd.safeTransfer(devAddress, devCut - toPartner);
                } else {
                    busd.safeTransferFrom(msg.sender, partner, toPartner);
                    busd.safeTransferFrom(
                        msg.sender,
                        devAddress,
                        devCut - toPartner
                    );
                }
                emit PartnerSold(partner, currentRound, _tickets);
            } else {
                if (fromSelf) busd.safeTransfer(devAddress, devCut);
                else busd.safeTransferFrom(msg.sender, devAddress, devCut);
            }
        } else {
            if (fromSelf) busd.safeTransfer(devAddress, devCut);
            else busd.safeTransferFrom(msg.sender, devAddress, devCut);
        }
    }

    /// @notice Get the percentage to use for non winners difference after claimer fee
    /// @param _round the round to get the info from.
    function getNoMatchAmount(uint256 _round)
        internal
        view
        returns (uint256 _matchReduction)
    {
        _matchReduction = roundInfo[_round].distribution[0].sub(
            claimers[_round].percent
        );
    }

    /// @notice Set the next start hour and next hour index
    /// @dev if next hour goes over the alleged next round necessary, it skips it and goes to the next available hour
    function calcNextHour() internal {
        uint256 tempEnd = roundEnd;
        uint8 newIndex = endHourIndex;
        while (tempEnd <= block.timestamp) {
            newIndex = newIndex + 1 >= endHours.length ? 0 : newIndex + 1;
            tempEnd = setNextRoundEndTime(
                block.timestamp,
                endHours[newIndex],
                newIndex != 0 || endHourIndex < newIndex,
                uint8(endHours.length)
            );
        }
        roundEnd = tempEnd;
        endHourIndex = newIndex;
    }

    /// @notice Adds the ticket to chain
    /// @param ticketOwner the owner of the ticket
    /// @param _ticketNumber the number playing
    /// @param _round the round currently being played
    /// @param _ticketCount the ticket id offset
    /// @dev Depending on how many tickets, the gas usage for this function can be quite high.
    function createTicket(
        address ticketOwner,
        uint32 _ticketNumber,
        uint256 _round,
        uint256 _ticketCount
    ) internal {
        assembly {
            let currentTicket := add(
                mod(_ticketNumber, WINNER_BASE),
                WINNER_BASE
            )
            // save ticket to userNewTickets
            mstore(0, ticketOwner)
            mstore(32, userTotalTickets.slot)
            let baseOffset := keccak256(0, 64)
            let secondaryVal := add(
                add(sload(keccak256(0, 64)), 1),
                _ticketCount
            )
            mstore(0, ticketOwner)
            mstore(32, userNewTickets.slot)
            baseOffset := keccak256(0, 64)
            mstore(0, secondaryVal)
            mstore(32, baseOffset)
            sstore(keccak256(0, 64), currentTicket)
            sstore(add(keccak256(0, 64), 1), _round)
            // Get base again
            mstore(0, _round)
            mstore(32, holders.slot)
            baseOffset := keccak256(0, 64)
            // save 6
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            let offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
            // save 5
            currentTicket := div(currentTicket, 10)
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
            // save 4
            currentTicket := div(currentTicket, 10)
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
            // save 3
            currentTicket := div(currentTicket, 10)
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
            // save 2
            currentTicket := div(currentTicket, 10)
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
            // save 1
            currentTicket := div(currentTicket, 10)
            mstore(0, currentTicket)
            mstore(32, baseOffset)
            offset := keccak256(0, 64)
            secondaryVal := sload(offset)
            sstore(offset, add(secondaryVal, 1))
        }
    }

    function getWinnerHolders(uint256 _round)
        external
        view
        returns (uint256[6] memory _digitHolders)
    {
        uint256[6] memory wD = roundInfo[_round].winnerDigits;
        for (uint8 i = 0; i < 6; i++) {
            // Holders are already adjusted
            uint256 _cHolder = holders[_round][wD[5 - i]];
            _digitHolders[5 - i] = _cHolder;
        }
    }

    /// @notice Get bonus reward amount based on holders and match amount
    /// @param _holders amount of holders
    /// @param bonus The bonus token data, used to obtain the bonus amount and maxpercent
    /// @param _match The percentage matched
    /// @return bonusAmount total amount to be distributed
    function getBonusReward(
        uint256 _holders,
        BonusCoin storage bonus,
        uint256 _match
    ) internal view returns (uint256 bonusAmount) {
        if (_holders == 0) return 0;
        if (bonus.bonusToken != address(0)) {
            if (_match == 0) return 0;
            bonusAmount = getFraction(
                bonus.bonusAmount,
                _match,
                bonus.bonusMaxPercent
            ).div(_holders);
            return bonusAmount;
        }
        return 0;
    }

    /// @notice Function that starts a round, it's only internal since it's for use of VRF
    /// @dev distribution array is setup on a round basis as to simplify matching
    function startRound() internal {
        require(currentIsActive == false, "CA"); // dev: current roun dis active
        require(pause == false, "LP"); // dev: Lottery is paused
        // Add new Round
        unchecked {
            currentRound++;
        }
        currentIsActive = true;
        RoundInfo storage newRound = roundInfo[currentRound];
        newRound.distribution = [
            noMatch,
            match1,
            match2,
            match3,
            match4,
            match5,
            match6,
            burn
        ];
        newRound.ticketValue = ticketValue;
        emit RoundStarted(currentRound, msg.sender, block.timestamp);
    }

    /// @notice function that calculates distribution and transfers rewards to claimer
    /// @dev From calculations on calculateRollover fn, it transfers the claimer fee to claimer and the distributes rewards to bankroll
    /// @dev finally it sets the pool amount in the next round
    function distributeTokens() internal {
        RoundInfo storage thisRound = roundInfo[currentRound];
        (
            uint256 rollOver,
            uint256 burnAmount,
            uint256 forClaimer
        ) = calculateRollover();
        // Transfer Amount to Claimer
        Claimer storage roundClaimer = claimers[currentRound];
        if (forClaimer > 0)
            token.safeTransfer(roundClaimer.claimer, forClaimer);
        if (bonusCoins[currentRound].bonusToken != address(0)) {
            ERC20 bonusTokenContract = ERC20(
                bonusCoins[currentRound].bonusToken
            );
            bonusTokenContract.safeTransfer(
                roundClaimer.claimer,
                getBonusReward(
                    1,
                    bonusCoins[currentRound],
                    roundClaimer.percent
                )
            );
        }

        // BURN AMOUNT
        if (burnAmount > 0) {
            token.burn(burnAmount);
            thisRound.burn = burnAmount;
        }
        initPool[currentRound + 1] = rollOver;
        roundInfo[currentRound + 1].pool = rollOver;
        if (!pause) startRound();
    }

    /// @notice checks for winner holders and returns distribution info
    /// @return _rollover the amount to be rolled over to next round
    /// @return _burn the amount to burn
    /// @return _forClaimer the claimer fee
    function calculateRollover()
        internal
        returns (
            uint256 _rollover,
            uint256 _burn,
            uint256 _forClaimer
        )
    {
        RoundInfo storage info = roundInfo[currentRound];
        _rollover = 0;
        // for zero match winners
        BonusCoin storage roundBonusCoin = bonusCoins[currentRound];
        uint256[6] memory winnerDigits = getDigits(info.winnerNumber);
        uint256 totalMatchHolders = 0;

        for (uint8 i = 0; i < 6; i++) {
            uint256 digitToCheck = winnerDigits[5 - i];
            uint256 matchHolders = holders[currentRound][digitToCheck];
            if (matchHolders > 0) {
                if (totalMatchHolders == 0) totalMatchHolders = matchHolders;
                else {
                    matchHolders -= totalMatchHolders;
                    totalMatchHolders += matchHolders;
                    holders[currentRound][digitToCheck] = matchHolders;
                }
                if (matchHolders > 0) {
                    _forClaimer += info.distribution[6 - i];
                    roundBonusCoin.bonusMaxPercent =
                        roundBonusCoin.bonusMaxPercent +
                        info.distribution[6 - i];
                }
            }
            if (matchHolders == 0)
                _rollover += getFraction(
                    info.pool,
                    info.distribution[6 - i],
                    PERCENT_BASE
                );
        }
        _rollover += getExtraRollover();
        _forClaimer = (_forClaimer * claimFee) / PERCENT_BASE;
        uint256 nonWinners = info.totalTickets - totalMatchHolders;
        info.totalWinners = totalMatchHolders;
        info.winnerDigits = winnerDigits;
        // Are there any noMatch tickets
        if (nonWinners == 0)
            _rollover += getFraction(
                info.pool,
                info.distribution[0].sub(_forClaimer),
                PERCENT_BASE
            );
        else roundBonusCoin.bonusMaxPercent += info.distribution[0];
        if (
            getFraction(initPool[currentRound], burnThreshold, PERCENT_BASE) <=
            info.pool - initPool[currentRound]
        ) _burn = getFraction(info.pool, info.distribution[7], PERCENT_BASE);
        else {
            _burn = 0;
            _rollover += getFraction(info.pool, burn, PERCENT_BASE);
        }
        claimers[currentRound].percent = _forClaimer;
        _forClaimer = getFraction(info.pool, _forClaimer, PERCENT_BASE);
    }

    function getExtraRollover() internal returns (uint256 _rollover) {
        RoundInfo storage _info = roundInfo[currentRound];
        uint256 totalToDistribute;
        for (uint8 d = 0; d < 8; d++) {
            totalToDistribute += _info.distribution[d];
        }
        if (totalToDistribute < PERCENT_BASE)
            return
                getFraction(
                    _info.pool,
                    PERCENT_BASE - totalToDistribute,
                    PERCENT_BASE
                );
        return 0;
    }

    /// @notice Function that gets called by VRF to deliver number and distribute claimer reward
    /// @param requestId id of VRF request
    /// @param randomWords Random number array delivered by VRF
    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords)
        internal
        override
    {
        RoundInfo storage info = roundInfo[currentRound];
        info.winnerNumber = standardTicketNumber(
            uint24(randomWords[0]),
            WINNER_BASE
        );
        distributeTokens();
        emit WinnerPicked(currentRound, info.winnerNumber, requestId);
    }

    /// @notice Function to get the fraction amount from a value
    /// @param _amount total to be fractioned
    /// @param _percent percentage to be applied
    /// @param _base  percentage base, most cases will use PERCENT_BASE since percentage is not used as 100
    /// @return fraction -> the result of the fraction computation
    function getFraction(
        uint256 _amount,
        uint256 _percent,
        uint256 _base
    ) internal pure returns (uint256 fraction) {
        fraction = _amount.mul(_percent).div(_base);
    }

    /// @notice Get all participating digits from number
    /// @param _ticketNumber the ticket number to extract digits from
    /// @return digits Array of 6, each with the matching pattern
    function getDigits(uint256 _ticketNumber)
        internal
        pure
        returns (uint256[6] memory digits)
    {
        digits[0] = _ticketNumber / 100000; // WINNER_BASE
        digits[1] = _ticketNumber / 10000;
        digits[2] = _ticketNumber / 1000;
        digits[3] = _ticketNumber / 100;
        digits[4] = _ticketNumber / 10;
        digits[5] = _ticketNumber;
    }

    /// @notice requirements to check per round and calculate distributed amount
    /// @param _ticketId the ticket Id to check
    /// @param _matchBatch the matching amount of the ticket Id
    /// @param _currentRound round to check
    /// @return _distributedAmount Total amount to be distributed
    function checkTicketRequirements(
        uint256 _ticketId,
        uint256 _matchBatch,
        RoundInfo storage _currentRound
    ) internal view returns (uint256 _distributedAmount) {
        require(
            userNewTickets[msg.sender][_ticketId].ticketNumber > 0 &&
                _matchBatch > 0 &&
                _matchBatch < 7,
            "M1"
        ); // dev: ticket doesnt exist || Minimum match 1
        require(
            getDigits(userNewTickets[msg.sender][_ticketId].ticketNumber)[
                _matchBatch - 1
            ] == getDigits(_currentRound.winnerNumber)[_matchBatch - 1],
            "NOWD"
        ); // dev: Not over or Wrong Data

        uint256 batchDistribution = _currentRound.distribution[_matchBatch];
        uint256 batchHolders = holders[
            userNewTickets[msg.sender][_ticketId].round
        ][_currentRound.winnerDigits[_matchBatch - 1]];
        return
            getFraction(_currentRound.pool, batchDistribution, PERCENT_BASE)
                .div(batchHolders);
    }

    /// @notice convert a number into the ticket range
    /// @param _ticketNumber any number, the used values are the least significant digits
    /// @param _base base of the ticket, usually WINNER_BASE
    /// @return uint32 with ticket number value
    function standardTicketNumber(uint32 _ticketNumber, uint32 _base)
        internal
        pure
        returns (uint32)
    {
        uint32 ticketNumber = (_ticketNumber % _base) + _base;
        return ticketNumber;
    }

    /// @notice Gets the next hour at the specific hour this is fixed from previous iteration
    /// @param _currentTimestamp the timestamp to verify
    /// @param _hour the new hour
    /// @param _sameDay if the time is set for the same day
    /// @param _timesPerDay times per day the lottery is played.
    function setNextRoundEndTime(
        uint256 _currentTimestamp,
        uint256 _hour,
        bool _sameDay,
        uint8 _timesPerDay
    ) internal pure returns (uint256 _endTimestamp) {
        uint256 nextDay = _sameDay
            ? _currentTimestamp
            : SECONDS_PER_DAY.add(_currentTimestamp);
        (uint256 year, uint256 month, uint256 day) = timestampToDateTime(
            nextDay
        );
        _endTimestamp = timestampFromDateTime(year, month, day, _hour, 0, 0);
        if (_endTimestamp - _currentTimestamp > SECONDS_PER_DAY / _timesPerDay)
            _endTimestamp = timestampFromDateTime(
                year,
                month,
                day - 1,
                _hour,
                0,
                0
            );
    }

    function updateDevAddress(address _newDev) external onlyOwner {
        require(_newDev != address(0)); // dev: Zero Address
        devAddress = _newDev;
        emit AuditLog(devAddress, "Update Dev");
    }

    // -------------------------------------------------------------------`
    // Timestamp fns taken from BokkyPooBah's DateTime Library
    //
    // Gas efficient Solidity date and time library
    //
    // https://github.com/bokkypoobah/BokkyPooBahsDateTimeLibrary
    //
    // Enjoy. (c) BokkyPooBah / Bok Consulting Pty Ltd 2018.
    //
    // GNU Lesser General Public License 3.0
    // https://www.gnu.org/licenses/lgpl-3.0.en.html
    // ----------------------------------------------------------------------------
    function timestampToDateTime(uint256 timestamp)
        internal
        pure
        returns (
            uint256 year,
            uint256 month,
            uint256 day
        )
    {
        (year, month, day) = _daysToDate(timestamp / SECONDS_PER_DAY);
    }

    function timestampFromDateTime(
        uint256 year,
        uint256 month,
        uint256 day,
        uint256 hour,
        uint256 minute,
        uint256 second
    ) internal pure returns (uint256 timestamp) {
        timestamp =
            _daysFromDate(year, month, day) *
            SECONDS_PER_DAY +
            hour *
            SECONDS_PER_HOUR +
            minute *
            SECONDS_PER_MINUTE +
            second;
    }

    function _daysToDate(uint256 _days)
        internal
        pure
        returns (
            uint256 year,
            uint256 month,
            uint256 day
        )
    {
        int256 __days = int256(_days);

        int256 L = __days + 68569 + OFFSET19700101;
        int256 N = (4 * L) / 146097;
        L = L - (146097 * N + 3) / 4;
        int256 _year = (4000 * (L + 1)) / 1461001;
        L = L - (1461 * _year) / 4 + 31;
        int256 _month = (80 * L) / 2447;
        int256 _day = L - (2447 * _month) / 80;
        L = _month / 11;
        _month = _month + 2 - 12 * L;
        _year = 100 * (N - 49) + _year + L;

        year = uint256(_year);
        month = uint256(_month);
        day = uint256(_day);
    }

    function _daysFromDate(
        uint256 year,
        uint256 month,
        uint256 day
    ) internal pure returns (uint256 _days) {
        require(year >= 1970);
        int256 _year = int256(year);
        int256 _month = int256(month);
        int256 _day = int256(day);

        int256 __days = _day -
            32075 +
            (1461 * (_year + 4800 + (_month - 14) / 12)) /
            4 +
            (367 * (_month - 2 - ((_month - 14) / 12) * 12)) /
            12 -
            (3 * ((_year + 4900 + (_month - 14) / 12) / 100)) /
            4 -
            OFFSET19700101;

        _days = uint256(__days);
    }
}
