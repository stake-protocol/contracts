// SPDX-License-Identifier: MIT
pragma solidity ^0.8.15;

// Openzeppelin
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

// Stake Protocol
import "./StakeToken.sol";
import "./TokenLock.sol";

/// This contract will be in charge of swapping BNB for $STAKE, adding Liquidity as $STAKE/BNB and minting LP token to user for the given amount

/// When the user swaps from BNB to STAKE, 3% of it is sent to treasury as BNB.
/// Operators should not be taxed

/// When user swaps from STAKE to BNB, 18% of the STAKE is taxed.
/// 13% of the total value to swap is converted to BNB and sent to treasury.
/// 5% of the STAKE to swap is converted to LP token by converting half of it to BNB and then sent to foundation.

/// Taxes will never be over 25% and can be edited

contract StakeFountain is ERC20, Ownable {
    using SafeMath for uint256;
    using SafeERC20 for ERC20;

    // Vars
    StakeToken public token; // ERC20 token traded on this contract
    TokenLocker public token_lock;
    address public treasury;
    address public vault;
    address public foundation;

    // BNB to token fee percentages
    uint256 public bt_treasury_fee;
    uint256 public bt_input_minus_fees;

    // Token to BNB fee percentages
    uint256 public tb_fee_total;
    uint256 public tb_treasury_fee;
    uint256 public tb_vault_fee;
    uint256 public tb_lock_fee;
    uint256 public tb_foundation_fee;
    uint256 public tb_input_with_total_fees;
    uint256 public tb_output_with_total_fees;
    uint256 public tb_input_minus_fees;

    // Contract stats
    uint256 public totalTxs;
    uint256 public lastBalance_;
    uint256 public trackingInterval_ = 1 minutes;
    uint256 public providers;

    // Maps
    mapping(address => bool) public whitelist;
    mapping(address => bool) public _providers;
    mapping(address => uint256) public _txs;

    // Events
    event onTokenPurchase(
        address indexed buyer,
        uint256 indexed bnb_amount,
        uint256 indexed token_amount
    );
    event onBnbPurchase(
        address indexed buyer,
        uint256 indexed token_amount,
        uint256 indexed bnb_amount
    );
    event onAddLiquidity(
        address indexed provider,
        uint256 indexed bnb_amount,
        uint256 indexed token_amount
    );
    event onRemoveLiquidity(
        address indexed provider,
        uint256 indexed bnb_amount,
        uint256 indexed token_amount
    );
    event onLiquidity(address indexed provider, uint256 indexed amount);
    event onContractBalance(uint256 balance);
    event onPrice(uint256 price);
    event onSummary(uint256 liquidity, uint256 price);
    event OperatorAddressAdded(address operator);
    event OperatorAddressRemoved(address operator);
    event TreasuryFee(uint256 amount);
    event VaultFee(uint256 amount);
    event FoundationFee(
        uint256 bnb_amount,
        uint256 stake_amount,
        uint256 liquidity_minted
    );
    event WhitelistedAddressAdded(address addr);
    event WhitelistedAddressRemoved(address addr);
    event SetLock(address addr);
    event BnbToTokenTax(uint256 treasury);
    event TokenToBnbTaxSpread(
        uint256 treasury,
        uint256 vault,
        uint256 foundation,
        uint256 lock
    );

    constructor(
        address _token,
        address _treasury,
        address _vault,
        address _foundation
    ) ERC20("Stake LP token", "STOKE") {
        token = StakeToken(_token);
        lastBalance_ = block.timestamp;
        treasury = _treasury;
        vault = _vault;
        foundation = _foundation;
        setFees(3, 13, 0, 5);
    }

    /***********************************|
    |         SETUP FUNCTIONS           |
    |__________________________________*/

    /// @dev Sets lock address. Make sure to set a lock address after deploying.
    /// @param _lock token lock address
    function setLock(address _lock) external onlyOwner {
        token_lock = TokenLocker(_lock);
        emit SetLock(_lock);
    }

    /// @dev Update the treasury wallet to another destination... used when treasury contract is live
    function setTreasury(address _treasury) external onlyOwner {
        require(_treasury != owner(), "Treasury not owner");
        treasury = _treasury;
    }

    /// @dev Sets fee amount numerators to calculate percentage. It also calculates input and output multipliers.
    /// @param _bt_treasury token to bnb treasury fee
    /// @param _tb_treasury bnb to token treasury fee
    /// @param _tb_vault bnb to token treasury fee
    /// @param _tb_foundation_and_lock bnb to token treasury fee

    function setFees(
        uint256 _bt_treasury,
        uint256 _tb_treasury,
        uint256 _tb_vault,
        uint256 _tb_foundation_and_lock
    ) public onlyOwner {
        tb_fee_total = _tb_treasury + _tb_vault + _tb_foundation_and_lock;
        require(_bt_treasury < 25 && tb_fee_total < 25);

        // BNB to token
        bt_treasury_fee = _bt_treasury; // Please divide bt_treasury_fee/100
        bt_input_minus_fees = 100 - bt_treasury_fee; // Please divide bt_inpu_minu_fees/100

        // Token to BNB
        tb_treasury_fee = _tb_treasury; // Please divide tb_treasury_fee/100
        tb_vault_fee = _tb_vault; //Please divide tb_vault_fee/100
        tb_lock_fee = 9 * _tb_foundation_and_lock; // Please divide tb_lock_fee/1000
        tb_foundation_fee = (10 * _tb_foundation_and_lock) - tb_lock_fee; // Please divide tb_foundation_fee/1000
        tb_input_minus_fees = 100 - tb_fee_total; // Please divide tb_input_with_total_fees/100

        emit BnbToTokenTax(bt_treasury_fee);
        emit TokenToBnbTaxSpread(
            tb_treasury_fee,
            tb_vault_fee,
            tb_foundation_fee,
            tb_lock_fee
        );
    }

    /***********************************|
    |        WHITELIST FUNCTIONS        |
    |__________________________________*/
    // Whitelisted addresses pay no tax when swapping

    /**
     * @dev add an address to the whitelist
     * @param addr address
     */
    function addAddressToWhitelist(address addr)
        public
        onlyOwner
        returns (bool success)
    {
        if (!whitelist[addr]) {
            whitelist[addr] = true;
            emit WhitelistedAddressAdded(addr);
            success = true;
        }
    }

    /**
     * @dev add addresses to the whitelist
     * @param addrs addresses
     */
    function addAddressesToWhitelist(address[] memory addrs)
        public
        onlyOwner
        returns (bool success)
    {
        for (uint256 i = 0; i < addrs.length; i++) {
            if (addAddressToWhitelist(addrs[i])) {
                success = true;
            }
        }
        return success;
    }

    /**
     * @dev remove an address from the whitelist
     * @param addr address
     */
    function removeAddressFromWhitelist(address addr)
        public
        onlyOwner
        returns (bool success)
    {
        if (whitelist[addr]) {
            whitelist[addr] = false;
            emit WhitelistedAddressRemoved(addr);
            success = true;
        }
        return success;
    }

    /**
     * @dev remove addresses from the whitelist
     * @param addrs addresses
     */
    function removeAddressesFromWhitelist(address[] memory addrs)
        public
        onlyOwner
        returns (bool success)
    {
        for (uint256 i = 0; i < addrs.length; i++) {
            if (removeAddressFromWhitelist(addrs[i])) {
                success = true;
            }
        }
        return success;
    }

    /***********************************|
    |    $STAKE  EXCHANGE FUNCTIONS     |
    |__________________________________*/

    /**
     * @notice Convert BNB to Tokens.
     * @dev User specifies exact input (msg.value).
     */
    receive() external payable {
        bnbToTokenInput(msg.value, 1, msg.sender, msg.sender);
    }

    /**
     * @dev Pricing function for converting between BNB && Tokens without fee.
     * @param input_amount Amount of BNB or Tokens being sold.
     * @param input_reserve Amount of BNB or Tokens (input type) in exchange reserves.
     * @param output_reserve Amount of BNB or Tokens (output type) in exchange reserves.
     * @return Amount of BNB or Tokens bought.
     */
    function getInputPrice(
        uint256 input_amount,
        uint256 input_reserve,
        uint256 output_reserve
    ) public pure returns (uint256) {
        require(input_reserve > 0 && output_reserve > 0, "INVALID_VALUE");
        uint256 numerator = input_amount * output_reserve;
        uint256 denominator = input_reserve + input_amount;
        return numerator / denominator;
    }

    /**
     * @dev Pricing function for converting between BNB && Tokens without fee.
     * @param output_amount Amount of BNB or Tokens being bought.
     * @param input_reserve Amount of BNB or Tokens (input type) in exchange reserves.
     * @param output_reserve Amount of BNB or Tokens (output type) in exchange reserves.
     * @return Amount of BNB or Tokens sold.
     */
    function getOutputPrice(
        uint256 output_amount,
        uint256 input_reserve,
        uint256 output_reserve
    ) public pure returns (uint256) {
        require(input_reserve > 0 && output_reserve > 0);
        uint256 numerator = input_reserve * output_amount;
        uint256 denominator = (output_reserve - output_amount);
        return (numerator / denominator) + 1;
    }

    /**
     * @dev Function to convert BNB into STAKE. Set an amount of BNB to obtain an amount of tokens. Fees included.
     * @param _bnb_sold Amount of BNB that user has paid to buy tokens.
     * @param _min_tokens Amount of minimum tokens after slippage.
     * @param _buyer address that paid for the tokens.
     * @param _recipient address that will receive the tokens.
     */
    function bnbToTokenInput(
        uint256 _bnb_sold,
        uint256 _min_tokens,
        address _buyer,
        address _recipient
    ) private returns (uint256) {
        require(_bnb_sold > 0 && _min_tokens > 0, "sold and min 0");
        uint256 bnb_sold;
        uint256 treasury_fee;
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_reserve = address(this).balance;

        if (whitelist[_buyer] && whitelist[_recipient]) {
            bnb_sold = _bnb_sold;
        } else {
            treasury_fee = (bt_treasury_fee * _bnb_sold) / 100;
            bnb_sold = _bnb_sold - treasury_fee;
        }
        uint256 tokens_bought = getInputPrice(
            bnb_sold,
            bnb_reserve - bnb_sold,
            token_reserve
        );

        require(tokens_bought >= _min_tokens, "tokens_bought >= min_tokens");
        // Transferring tokens to msg.sender and transferring fee
        require(token.transfer(_recipient, tokens_bought), "transfer err");
        if (treasury_fee > 0) transferTreasuryFee(treasury_fee);

        emit TreasuryFee(treasury_fee);
        emit onTokenPurchase(_buyer, _bnb_sold, tokens_bought);
        emit onContractBalance(bnbBalance());

        trackGlobalStats();

        return tokens_bought;
    }

    /**
     * @notice Convert BNB to Tokens.
     * @dev User specifies exact input (msg.value) && minimum output.
     * @param min_tokens Minimum Tokens bought. Considers slippage.
     * @return Amount of Tokens bought.
     */
    function bnbToTokenSwapInput(uint256 min_tokens)
        public
        payable
        returns (uint256)
    {
        return bnbToTokenInput(msg.value, min_tokens, msg.sender, msg.sender);
    }

    /**
     * @dev Function to convert BNB into STAKE. Set an amount of STAKE get it at a price in BNB .
     * @param _tokens_bought Set amount of tokens
     * @param _max_bnb Maximum amount of BNB that can be charged for the set amount of stake.
     * @param _buyer address that paid for the tokens.
     * @param _recipient address that will receive the tokens.
     */
    function bnbToTokenOutput(
        uint256 _tokens_bought,
        uint256 _max_bnb,
        address _buyer,
        address _recipient
    ) private returns (uint256) {
        require(_tokens_bought > 0 && _max_bnb > 0);
        uint256 total_bnb_sold;
        uint256 treasury_fee;
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_reserve = address(this).balance;
        uint256 bnb_sold = getOutputPrice(
            _tokens_bought,
            bnb_reserve - _max_bnb,
            token_reserve
        );
        if (whitelist[_buyer] && whitelist[_recipient]) {
            total_bnb_sold = bnb_sold;
        } else {
            treasury_fee = (bt_treasury_fee * total_bnb_sold) / 100;
            total_bnb_sold = (bt_input_minus_fees * bnb_sold) / 100;
        }

        // Throws if total_bnb_sold > _max_bnb
        uint256 bnb_refund = _max_bnb - total_bnb_sold;
        if (bnb_refund > 0) {
            (bool succ, ) = payable(_buyer).call{value: bnb_refund}("");
            require(succ, "Fail refund");
        }
        require(token.transfer(_recipient, _tokens_bought));
        if (treasury_fee > 0) transferTreasuryFee(treasury_fee);
        emit TreasuryFee(treasury_fee);
        emit onTokenPurchase(_buyer, total_bnb_sold, _tokens_bought);
        emit TreasuryFee(treasury_fee);
        trackGlobalStats();
        return total_bnb_sold;
    }

    /**
     * @notice Convert BNB to Tokens.
     * @dev User specifies maximum input (msg.value) && exact output.
     * @param tokens_bought Amount of tokens bought.
     * @return Amount of BNB sold.
     */
    function bnbToTokenSwapOutput(uint256 tokens_bought)
        public
        payable
        returns (uint256)
    {
        return
            bnbToTokenOutput(tokens_bought, msg.value, msg.sender, msg.sender);
    }

    /**
     * @dev Function to convert STAKE into BNB. Taxes before calculating and transferring the tokens.
     * When user swaps from STAKE to BNB, tb_total_input_fee of the STAKE is taxed from total swapped
     * tb_treasury_fee of the total value to swap is converted to BNB and sent to treasury
     * tb_vault_fee of the STAKE to swap is sent to vault in STAKE
     * tb_foundation_and_lock of the STAKE to swap is converted to LP token by converting half of it to BNB and then sent to foundation and lock
     * @param _tokens_sold Amount of STAKE that user has paid to buy BNB
     * @param _min_bnb Amount of minimum BNB after slippage
     * @param _buyer address that transferred the tokens
     * @param _recipient address that will receive the BNB
     */
    function tokenToBnbInput(
        uint256 _tokens_sold,
        uint256 _min_bnb,
        address _buyer,
        address _recipient
    ) private returns (uint256) {
        require(_tokens_sold > 0 && _min_bnb > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_reserve = address(this).balance;
        uint256 total_bnb_bought;
        uint256 bnb_bought = getInputPrice(
            _tokens_sold,
            token_reserve,
            bnb_reserve
        );
        require(bnb_bought >= _min_bnb);
        if (whitelist[_buyer] && whitelist[_recipient]) {
            total_bnb_bought = bnb_bought;
        } else {
            total_bnb_bought = (tb_input_minus_fees * bnb_bought) / 100;
            tokenToBnbFees(bnb_bought, _tokens_sold, bnb_reserve);
        }
        (bool succ, ) = payable(_recipient).call{value: total_bnb_bought}("");
        require(succ, "Fail transfer BNB");
        require(
            token.transferFrom(_buyer, address(this), _tokens_sold),
            "Fail transfer tokens"
        );
        emit onBnbPurchase(_buyer, _tokens_sold, total_bnb_bought);
        trackGlobalStats();
        return total_bnb_bought;
    }

    /**
     * @notice Convert Tokens to BNB.
     * @dev User specifies exact input && minimum output.
     * @param tokens_sold Amount of Tokens sold.
     * @param min_bnb Minimum BNB purchased.
     * @return Amount of BNB bought.
     */
    function tokenToBnbSwapInput(uint256 tokens_sold, uint256 min_bnb)
        public
        returns (uint256)
    {
        return tokenToBnbInput(tokens_sold, min_bnb, msg.sender, msg.sender);
    }

    function tokenToBnbOutput(
        uint256 _bnb_bought,
        uint256 _max_tokens,
        address _buyer,
        address _recipient
    ) private returns (uint256) {
        require(_bnb_bought > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_reserve = address(this).balance;
        uint256 tokens_sold = getOutputPrice(
            _bnb_bought,
            token_reserve,
            bnb_reserve
        );
        uint256 total_tokens_sold;
        require(_max_tokens >= tokens_sold, "max tokens exceeded");
        //Added fees into price in tokens
        if (whitelist[_buyer] && whitelist[_recipient]) {
            total_tokens_sold = tokens_sold;
        } else {
            total_tokens_sold = (100 * tokens_sold) / tb_input_minus_fees;
            tokenToBnbFees(_bnb_bought, total_tokens_sold, bnb_reserve);
        }

        // tokens sold is always > 0
        (bool succ, ) = payable(_recipient).call{value: _bnb_bought}("");
        require(succ, "Fail to send BNB");
        require(
            token.transferFrom(_buyer, address(this), total_tokens_sold),
            "Fail tokens send"
        );
        emit onBnbPurchase(_buyer, total_tokens_sold, _bnb_bought);
        trackGlobalStats();

        return total_tokens_sold;
    }

    /**
     * @notice Convert Tokens to BNB.
     * @dev User specifies maximum input && exact output.
     * @param bnb_bought Amount of BNB purchased.
     * @param max_tokens Maximum Tokens sold.
     * @return Amount of Tokens sold.
     */
    function tokenToBnbSwapOutput(uint256 bnb_bought, uint256 max_tokens)
        public
        returns (uint256)
    {
        return tokenToBnbOutput(bnb_bought, max_tokens, msg.sender, msg.sender);
    }

    function trackGlobalStats() private {
        uint256 price = getBnbToTokenOutputPrice(1e18);
        uint256 balance = bnbBalance();

        if (block.timestamp - lastBalance_ > trackingInterval_) {
            emit onSummary(balance * 2, price);
            lastBalance_ = block.timestamp;
        }

        emit onContractBalance(balance);
        emit onPrice(price);

        totalTxs += 1;
        _txs[msg.sender] += 1;
    }

    // Fee functions

    /// Function that charges the respective fee to treasury
    /// @param _treasury_fee is the amount of BNB charged as fee
    function transferTreasuryFee(uint256 _treasury_fee) internal {
        // Calculating fee and BNB sold
        require(_treasury_fee > 0);
        (bool os, ) = payable(treasury).call{value: _treasury_fee}("");
        require(os);
        emit TreasuryFee(_treasury_fee);
    }

    /// Function that calculates and transfers fees for treasury, vault, foundation and lock
    /// @param _bnb_bought is the amount of BNB that user has bought
    /// @param _tokens_sold is the amount of tokens user sells
    /// @param _bnb_reserve is this contract's bnb balance
    function tokenToBnbFees(
        uint256 _bnb_bought,
        uint256 _tokens_sold,
        uint256 _bnb_reserve
    ) internal {
        // 8% of the total value to swap is converted to BNB and sent to treasury.
        uint256 treasury_fee = (tb_treasury_fee * _bnb_bought) / 100;
        if (treasury_fee > 0) transferTreasuryFee(treasury_fee);

        // 5% of the STAKE to swap is sent to vault in STAKE.
        uint256 vault_fee = (tb_vault_fee * _tokens_sold) / 100;
        if (vault_fee > 0) token.transfer(vault, vault_fee);

        // 5% of the STAKE to swap is converted to LP token by converting half of it to BNB. Then 90% of it is sent to token lock and 10% to foundation.
        uint256 total_liquidity = totalSupply();
        uint256 foundation_fee_bnb = (tb_foundation_fee * _bnb_bought) / 2000;
        uint256 foundation_liquidity_minted = (foundation_fee_bnb *
            total_liquidity) / _bnb_reserve;

        uint256 lock_fee_bnb = (tb_lock_fee * _bnb_bought) / 2000;
        uint256 lock_liquidity_minted = (lock_fee_bnb * total_liquidity) /
            _bnb_reserve;

        if (foundation_liquidity_minted > 0)
            _mint(foundation, foundation_liquidity_minted);
        if (lock_liquidity_minted > 0)
            _mint(address(token_lock), lock_liquidity_minted);
        token_lock.updateLock();

        // Fee events
        emit TreasuryFee(treasury_fee);
        emit VaultFee(vault_fee);

        emit onLiquidity(foundation, balanceOf(foundation));
        emit Transfer(address(0), foundation, foundation_liquidity_minted);

        emit onLiquidity(address(token_lock), balanceOf(address(token_lock)));
        emit Transfer(address(0), address(token_lock), lock_liquidity_minted);
    }

    /// Exchange Getter Functions

    function getBnbToTokenInputPrice(uint256 bnb_sold)
        public
        view
        returns (uint256)
    {
        require(bnb_sold > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        return getInputPrice(bnb_sold, address(this).balance, token_reserve);
    }

    /**
     * @notice Public price function for BNB to Token trades with an exact output.
     * @param tokens_bought Amount of Tokens bought.
     * @return Amount of BNB needed to buy output Tokens.
     */
    function getBnbToTokenOutputPrice(uint256 tokens_bought)
        public
        view
        returns (uint256)
    {
        require(tokens_bought > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_sold = getOutputPrice(
            tokens_bought,
            address(this).balance,
            token_reserve
        );
        return bnb_sold;
    }

    /**
     * @notice Public price function for Token to BNB trades with an exact input.
     * @param tokens_sold Amount of Tokens sold.
     * @return Amount of BNB that can be bought with input Tokens.
     */
    function getTokenToBnbInputPrice(uint256 tokens_sold)
        public
        view
        returns (uint256)
    {
        require(tokens_sold > 0, "token sold < 0");
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_bought = getInputPrice(
            tokens_sold,
            token_reserve,
            address(this).balance
        );
        return bnb_bought;
    }

    /**
     * @notice Public price function for Token to BNB trades with an exact output.
     * @param bnb_bought Amount of output BNB.
     * @return Amount of Tokens needed to buy output BNB.
     */
    function getTokenToBnbOutputPrice(uint256 bnb_bought)
        public
        view
        returns (uint256)
    {
        require(bnb_bought > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        return getOutputPrice(bnb_bought, token_reserve, address(this).balance);
    }

    /**
     * @return Address of Token that is sold on this exchange.
     */
    function tokenAddress() public view returns (address) {
        return address(token);
    }

    function bnbBalance() public view returns (uint256) {
        return address(this).balance;
    }

    function tokenBalance() public view returns (uint256) {
        return token.balanceOf(address(this));
    }

    function getBnbToLiquidityInputPrice(uint256 bnb_sold)
        public
        view
        returns (uint256, uint256)
    {
        require(bnb_sold > 0);
        uint256 token_amount = 0;
        uint256 total_liquidity = totalSupply();
        uint256 bnb_reserve = address(this).balance;
        uint256 token_reserve = token.balanceOf(address(this));
        token_amount = ((bnb_sold * token_reserve) / bnb_reserve) + 1;
        uint256 liquidity_minted = (bnb_sold * total_liquidity) / bnb_reserve;

        return (liquidity_minted, token_amount);
    }

    function getLiquidityToReserveInputPrice(uint256 amount)
        public
        view
        returns (uint256, uint256)
    {
        uint256 total_liquidity = totalSupply();
        require(total_liquidity > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_amount = (amount * address(this).balance) / total_liquidity;
        uint256 token_amount = (amount * token_reserve) / total_liquidity;
        return (bnb_amount, token_amount);
    }

    function txs(address owner) public view returns (uint256) {
        return _txs[owner];
    }

    /// Exchange Liquidity Functions

    /**
     * @notice Deposit BNB && Tokens (STAKE) at current ratio to mint STOKE tokens.
     * @dev min_liquidity does nothing when total SWAP supply is 0.
     * @param min_liquidity Minimum number of STOKE sender will mint if total STAKE supply is greater than 0.
     * @param max_tokens Maximum number of tokens deposited. Deposits max amount if total STOKE supply is 0.
     * @return The amount of SWAP minted.
     */
    function addLiquidity(uint256 min_liquidity, uint256 max_tokens)
        public
        payable
        returns (uint256)
    {
        require(
            max_tokens > 0 && msg.value > 0,
            "Swap#addLiquidity: INVALID_ARGUMENT"
        );
        uint256 total_liquidity = totalSupply();

        uint256 token_amount = 0;

        if (_providers[msg.sender] == false) {
            _providers[msg.sender] = true;
            providers += 1;
        }

        if (total_liquidity > 0) {
            require(min_liquidity > 0, "Min liq = 0");
            uint256 bnb_reserve = address(this).balance - msg.value;
            uint256 token_reserve = token.balanceOf(address(this));
            token_amount = ((msg.value * token_reserve) / bnb_reserve) + 1;
            uint256 liquidity_minted = (msg.value * total_liquidity) /
                bnb_reserve;

            require(
                max_tokens >= token_amount && liquidity_minted >= min_liquidity,
                "require more tokens"
            );
            _mint(msg.sender, liquidity_minted);
            require(
                token.transferFrom(msg.sender, address(this), token_amount),
                "transfer from unsuccessful"
            );

            emit onAddLiquidity(msg.sender, msg.value, token_amount);
            emit onLiquidity(msg.sender, balanceOf(msg.sender));
            emit Transfer(address(0), msg.sender, liquidity_minted);
            return liquidity_minted;
        } else {
            require(msg.value >= 1 ether, "INVALID_VALUE");
            token_amount = max_tokens;
            uint256 initial_liquidity = address(this).balance;
            super._mint(msg.sender, initial_liquidity);
            require(
                token.transferFrom(msg.sender, address(this), token_amount),
                "TransferFrom unsuccessful"
            );

            emit onAddLiquidity(msg.sender, msg.value, token_amount);
            emit onLiquidity(msg.sender, balanceOf(msg.sender));
            emit Transfer(address(0), msg.sender, initial_liquidity);
            return initial_liquidity;
        }
    }

    /**
     * @dev Burn SWAP tokens to withdraw BNB && Tokens at current ratio.
     * @param amount Amount of SWAP burned.
     * @param min_bnb Minimum BNB withdrawn.
     * @param min_tokens Minimum Tokens withdrawn.
     * @return The amount of BNB && Tokens withdrawn.
     */
    function removeLiquidity(
        uint256 amount,
        uint256 min_bnb,
        uint256 min_tokens
    ) public returns (uint256, uint256) {
        require(amount > 0 && min_bnb > 0 && min_tokens > 0);
        uint256 total_liquidity = totalSupply();
        require(total_liquidity > 0);
        uint256 token_reserve = token.balanceOf(address(this));
        uint256 bnb_amount = (amount * address(this).balance) / total_liquidity;
        uint256 token_amount = (amount * (token_reserve)) / total_liquidity;
        require(bnb_amount >= min_bnb && token_amount >= min_tokens);
        super._burn(msg.sender, amount);
        (bool succ, ) = payable(msg.sender).call{value: bnb_amount}("");
        require(succ, "Fail tranfer BNB");
        require(token.transfer(msg.sender, token_amount));
        emit onRemoveLiquidity(msg.sender, bnb_amount, token_amount);
        emit onLiquidity(msg.sender, balanceOf(msg.sender));
        emit Transfer(msg.sender, address(0), amount);
        return (bnb_amount, token_amount);
    }
}
