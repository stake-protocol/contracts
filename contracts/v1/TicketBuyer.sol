// SPDX-License-Identifier: MIT

pragma solidity ^0.8.16;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../../interfaces/ILottery.sol";
import "../../interfaces/IFountain.sol";
import "../../interfaces/IPancakePair.sol";
import "../../interfaces/IPancakeSwapRouter.sol";

contract TicketBuyer {
    ERC20 public immutable token;
    ERC20 public immutable busd =
        ERC20(0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56);
    ILottery public immutable lottery;
    IFountain public pol;

    //BABY - USDT
    ERC20 public immutable usdt =
        ERC20(0x55d398326f99059fF775485246999027B3197955);
    IPancakePair public pairWithStable =
        IPancakePair(0xE730C7B7470447AD4886c763247012DfD233bAfF);
    IPancakeSwapRouter public router =
        IPancakeSwapRouter(0x325E343f1dE602396E256B67eFd1F61C3A6B38Bd);

    constructor(address _token, address _lottery) {
        token = ERC20(_token);
        lottery = ILottery(_lottery);
    }

    function buyTickets(uint32[] calldata _ticketNumbers, uint256 _partnerId)
        external
    {
        // get token price based on tickets to buy
        // transfer token to this contract
        getTokensFromUser(_ticketNumbers.length);
        // swap token for BUSD
        swapTokenToBUSD();
        // TRANSFER TO Treasury and partners
        distributePartnerFee(_partnerId);
        // swap BUSD to STAKE

        // send STAKE to pool using addToPool

        // buyWithPartnerToken
    }

    function getTokensFromUser(uint256 _ticketAmount) internal {
        //GET PRICE OF TOKEN RELATIVE TO BUSD
        (uint256 baby, uint256 stable, ) = pairWithStable.getReserves();
        uint256 price = getCurrentLotteryPrice();
        // BABY SPECIFIC - we wont do the extra conversion into bUSD because im lazy
        baby = (price * _ticketAmount * baby) / stable;
        bool succ = token.transferFrom(msg.sender, address(this), baby);
        require(succ, "Token tx fail");
    }

    function swapTokenToBUSD() internal {
        uint256 balance = token.balanceOf(address(this));
        token.approve(address(router), balance);
        address[] memory path = new address[](3);
        path[0] = address(token);
        path[1] = address(usdt);
        path[2] = address(busd);
        //swap to BUSD
        router.swapExactTokensForTokens(
            balance,
            1,
            path,
            address(this),
            block.timestamp
        );
    }

    function swapBUSDToSTAKE() internal {}

    function getCurrentLotteryPrice() internal returns (uint256 _price) {
        uint256 currentRound = lottery.currentRound();
        (, , , , , , , _price) = lottery.roundInfo(currentRound);
    }

    function distributePartnerFee(uint256 _id) internal {
        uint256 balance = busd.balanceOf(address(this));
        uint256 devCut = lottery.devAmount();
        balance = (balance * devCut) / 1e12; // 1e12 is PERCENT_BASE IN LOTTERY

        try lottery.partners(_id) returns (address pAddress) {} catch {}
    }
}
