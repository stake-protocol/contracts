// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../../interfaces/IVault.sol";
import "../../interfaces/IPinkAntiBot.sol";
import "../../interfaces/IPancakeSwapPair.sol";

contract StakeToken is ERC20, Ownable {
    uint256 public transferTax;
    uint256 public sellTax;
    uint256 public buyTax;

    mapping(address => uint256) public customTaxes;
    mapping(address => bool) public hasCustomTaxes; // Custom taxes have an extra 2 zeroes.
    mapping(address => bool) public exclusions; //Send
    mapping(address => bool) public receiveExclusions;
    mapping(address => bool) public liquidityPairs;
    address public vault;
    address public pool;
    bool private vaultIsContract = false;
    uint256 public lastMint;

    uint256 public constant INIT_SUPPLY = 750000 ether;
    uint256 public constant CUSTOM_TAX_BASE = 10000;
    uint256 public constant MAX_SUPPLY = 5000000 ether;

    // ANTI BOT
    IPinkAntiBot public pinkAntiBot;
    bool public antiBotEnabled = true;

    modifier onlyPool() {
        require(pool != address(0) && msg.sender == pool, "Not the pool");
        _;
    }

    constructor(address _vault) ERC20("Stake Token", "STAKE") {
        require(_vault != address(0), "Incorrect address");
        transferTax = 10;
        sellTax = 18;
        buyTax = 11;
        vault = _vault;
        _mint(owner(), INIT_SUPPLY);
        // This conditional is not on mainnet since it's only used for testing
        if (block.chainid == 56) {
            pinkAntiBot = IPinkAntiBot(
                0x8EFDb3b642eb2a20607ffe0A56CFefF6a95Df002
            );
            pinkAntiBot.setTokenOwner(owner());
        } else {
            antiBotEnabled = false;
        }
        pool = address(0);
        lastMint = block.timestamp;
    }

    function setEnableAntiBot(bool _enable) external onlyOwner {
        antiBotEnabled = _enable;
    }

    function setPool(address _pool) external onlyOwner {
        require(_pool != address(0) && pool == address(0), "zero address");
        pool = _pool;
    }

    function setAllTaxes(
        uint256 _transferTax,
        uint256 _sell,
        uint256 _buy
    ) external onlyOwner {
        require(
            _buy <= 25 && _sell <= 25 && _transferTax <= 20,
            "Invalid Tax Amount"
        );
        transferTax = _transferTax;
        sellTax = _sell;
        buyTax = _buy;
    }

    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        require(from != address(0), "BEP20: transfer from the zero address");
        require(to != address(0), "BEP20: transfer to the zero address");
        if (antiBotEnabled) pinkAntiBot.onPreTransferCheck(from, to, amount);

        (uint256 taxAmount, uint8 taxType) = calculateTransferTax(
            from,
            to,
            amount
        );
        uint256 realizedAmount = amount - taxAmount;
        address _taxUser = taxType == 4 ? from : address(0);
        if (taxAmount > 0 && taxType > 0) {
            super._transfer(from, vault, taxAmount);
            if (taxType > 0 && vaultIsContract) {
                try
                    IVault(vault).spread(taxAmount, taxType, _taxUser)
                {} catch {}
            }
        }

        super._transfer(from, to, realizedAmount);
    }

    function calculateTransferTax(
        address from,
        address to,
        uint256 amount
    ) public view returns (uint256, uint8 _type) {
        // BUY 1, SELL 2, Transfer 3, Custom 4
        // Protocol Owned Liquidity and exclusions will handle taxes themselves.
        if (exclusions[from] || receiveExclusions[to]) return (0, 0);
        if (hasCustomTaxes[from])
            return ((customTaxes[from] * amount) / CUSTOM_TAX_BASE, 4);
        if (liquidityPairs[from]) return ((buyTax * amount) / 100, 1);
        if (liquidityPairs[to]) return ((sellTax * amount) / 100, 2);
        return ((transferTax * amount) / 100, 3);
    }

    function setCustomTax(address _contract, uint256 _tax) external onlyOwner {
        require(_tax <= CUSTOM_TAX_BASE / 4, "Tax too large");
        customTaxes[_contract] = _tax;
    }

    function setCustomTaxStatus(address _contract, bool _status)
        external
        onlyOwner
    {
        hasCustomTaxes[_contract] = _status;
    }

    function excludeAddress(
        address _address,
        bool _all,
        bool _isReceive
    ) public onlyOwner {
        if (_all) {
            receiveExclusions[_address] = true;
            exclusions[_address] = true;
            return;
        }
        if (_isReceive) {
            receiveExclusions[_address] = true;
            return;
        }
        exclusions[_address] = true;
    }

    function excludeMultiple(
        address[] calldata _addresses,
        bool _all,
        bool _isReceive
    ) external onlyOwner {
        uint256 totalAddresses = _addresses.length;
        require(totalAddresses > 0, "Empty");
        for (uint256 i = 0; i < totalAddresses; i++) {
            excludeAddress(_addresses[i], _all, _isReceive);
        }
    }

    function removeExclusions(
        address _address,
        bool _all,
        bool _isReceive
    ) public onlyOwner {
        if (_all) {
            receiveExclusions[_address] = false;
            exclusions[_address] = false;
            return;
        }
        if (_isReceive) {
            receiveExclusions[_address] = false;
            return;
        }
        exclusions[_address] = false;
    }

    function removeMultiple(
        address[] calldata _addresses,
        bool _all,
        bool _isReceive
    ) external onlyOwner {
        uint256 totalAddresses = _addresses.length;
        require(totalAddresses > 0, "Empty");
        for (uint256 i = 0; i < totalAddresses; i++) {
            removeExclusions(_addresses[i], _all, _isReceive);
        }
    }

    function updateVault(address _vault, bool _isContract) external onlyOwner {
        require(_vault != address(0) && !vaultIsContract, "Invalid vault");
        vault = _vault;
        vaultIsContract = _isContract;
    }

    function burn(uint256 amount) external {
        _burn(msg.sender, amount);
    }

    // We don't stop execution if mint is unsuccessful.
    function mint(uint256 amount) public onlyPool returns (bool) {
        if (
            pool != address(0) &&
            amount > 0 &&
            amount <= (totalSupply() / 960) && // max mint of 5% of total Supply daily every 30 min
            amount + totalSupply() <= MAX_SUPPLY &&
            block.timestamp - lastMint > 30 minutes // mint available only every 30 min.
        ) {
            _mint(pool, amount);
            lastMint = block.timestamp;
            return true;
        }
        return false;
    }

    function addLiquidityPair(address _pair) external onlyOwner {
        require(_pair != address(0), "zero address");
        require(
            IPancakeSwapPair(_pair).factory() != address(0),
            "Invalid pair"
        );
        liquidityPairs[_pair] = true;
    }
}
