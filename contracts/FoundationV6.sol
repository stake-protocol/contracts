//SPDX-License-Identifier:MIT
pragma solidity 0.8.17;

import "../interfaces/IFoundation.sol";
import "../interfaces/IToken.sol";
import "../interfaces/IPOL.sol";

contract FoundationV6 is IFoundation {
    struct User {
        uint256 shares;
        uint256 debt;
        uint256 deposits;
        uint256 claims;
        uint256 perm_shares;
        uint256 all_earnings;
    }

    IToken public BUSD;
    IToken public STAKE;
    IPOL public pol;
    address public locker;

    uint256 public accRewardsPerShare;
    uint256 public totalShares;
    mapping(address => User) public shares;

    //-----------------------------------------
    //            Stats
    //-----------------------------------------
    uint256 public totalClaimed;
    uint256 public totalLocked;
    uint256 public totalRewarded;
    //-----------------------------------------
    //            Fees
    //-----------------------------------------
    uint256 public depositFee = 3;
    uint256 public withdrawFee = 10;
    uint256 public claimFee = 10;

    //-----------------------------------------
    //            EVENTS
    //-----------------------------------------
    event Deposit(
        address indexed _user,
        uint256 _amount,
        uint256 _shares_created,
        uint256 _shares_total
    );
    event Withdraw(
        address indexed _user,
        uint256 _busd_received,
        uint256 _shares_withdrawn
    );
    event Claim(address indexed _user, uint256 _amount, uint256 _tax);
    event Compound(
        address indexed _user,
        uint256 _busd,
        uint256 _shares_created,
        uint256 _shares_total
    );
    //-----------------------------------------
    //            Ownable.sol
    //-----------------------------------------
    address public owner;

    modifier onlyOwner() {
        require(msg.sender == owner, "Not Owner");
        _;
    }

    //-----------------------------------------
    //         ReentrancyGuard.sol
    //-----------------------------------------
    bool private lock = false;
    modifier reentrancyGuard() {
        require(!lock, "Reentrant");
        lock = true;
        _;
        lock = false;
    }

    //-----------------------------------------
    //         ACTUAL FOUNDATION
    //-----------------------------------------
    constructor(
        address _busd,
        address _stake,
        address _pol,
        address _locker
    ) {
        BUSD = IToken(_busd);
        STAKE = IToken(_stake);
        pol = IPOL(_pol);
        owner = msg.sender;
        locker = _locker;
    }

    function distribute(uint256 amount) public {
        BUSD.transferFrom(msg.sender, address(this), amount);
        totalRewarded += amount;
        if (totalShares > 0)
            accRewardsPerShare += (amount * 1 ether) / (totalShares);
    }

    function deposit(uint256 _deposit) public reentrancyGuard {
        require(BUSD.transferFrom(msg.sender, address(this), _deposit), "DP1"); // dev: couldn't transfer tokens
        // EZ SHARES
        User storage user = shares[msg.sender];
        if ((user.shares + user.perm_shares) > 0) {
            compound();
        }
        // Approve to create liquidity
        BUSD.approve(address(pol), _deposit);
        // USE POL NEW FUNCTION FOR ADDING.
        uint256 min_liq = pol.addLiquidityFromBase(_deposit);
        //---
        // 3% is locked as liquidity
        uint256 fee = (min_liq * depositFee) / 100;
        min_liq -= fee;
        totalLocked += fee;
        pol.transfer(locker, fee);
        // user shares are based on liquidity provided
        user.deposits += _deposit;
        user.shares += min_liq;
        user.debt = (user.shares + user.perm_shares) * accRewardsPerShare;
        totalShares += min_liq;
        //
        emit Deposit(msg.sender, _deposit, min_liq, user.shares);
    }

    function withdraw(uint256 amount) public {
        User storage user = shares[msg.sender];
        // TODO this function gets the liquidity USER provided, removes it and then sends it to USER
        require(amount < user.shares + 1, "WTH1"); // dev: Insufficient Balance
        // first thing, claim pending rewards
        claim();
        // withdrawfee of Liquidity is locked
        user.shares -= amount;
        // we are only using half of the withdrawFee
        uint256 tax = (amount * withdrawFee) / 200;
        totalLocked += tax;
        pol.transfer(locker, tax);
        pol.approve(address(pol), amount);
        uint256 baseAmount = pol.removeLiquidityToBase(amount - tax, tax);
        user.debt = (user.shares + user.perm_shares) * accRewardsPerShare;
        BUSD.transfer(msg.sender, baseAmount);
        emit Withdraw(msg.sender, baseAmount, amount);
        // If user withdraws all of their shares, they lose their permanent Shares
        if (user.shares == 0) {
            totalShares -= user.perm_shares;
            user.perm_shares = 0;
        }
    }

    function compound() public {
        // this function is FREE OF TAX
        User storage user = shares[msg.sender];
        // whatever BUSD is earned by USER gets converted to liquidity
        uint256 reward = getPendingRewards(msg.sender);
        user.all_earnings == reward;
        BUSD.approve(address(pol), reward);
        uint256 liq = pol.addLiquidityFromBase(reward);
        // update USER values
        // add the new liquidity to user's SHARES values
        user.shares += liq;
        user.debt = (user.shares + user.perm_shares) * accRewardsPerShare;
        // Update global stat trackers
        totalShares += liq;
        totalClaimed += reward;
        emit Compound(msg.sender, reward, liq, user.shares);
    }

    function claim() public {
        User storage user = shares[msg.sender];
        //get BUSD amount earned
        uint256 claim_reward = getPendingRewards(msg.sender);
        if (claim_reward == 0) return;
        //claimfee is distributed to other users
        uint256 tax = (claim_reward * claimFee) / 100;
        totalClaimed += claim_reward;
        // We remove this user's shares in order to calc the tax given to the others
        accRewardsPerShare +=
            (tax * 1 ether) /
            (totalShares - (user.shares + user.perm_shares));

        //update USER values
        user.debt = (user.shares + user.perm_shares) * accRewardsPerShare;
        user.claims += claim_reward;
        user.all_earnings += claim_reward;

        emit Claim(msg.sender, claim_reward, tax);
        claim_reward -= tax;
        BUSD.transfer(msg.sender, claim_reward);
    }

    function getPendingRewards(address _user)
        public
        view
        returns (uint256 _pending)
    {
        // here we calculate the pending BUSD that _user is owed
        User storage user = shares[_user];
        return
            ((user.shares + user.perm_shares) *
                accRewardsPerShare -
                user.debt) / 1 ether;
    }

    /// @notice Give a user "permanent" shares, these shares are mainly used for people who participated in the previous Foundation iteration
    /// @param _user The user that will be credited
    /// @param _perm_shares Number of shares to give
    /// @dev Please note that these shares will be lost if user ever withdraws his regular shares.
    function setPermShares(address _user, uint256 _perm_shares)
        public
        onlyOwner
    {
        User storage user = shares[_user];
        if (user.perm_shares > 0) {
            totalShares -= user.perm_shares;
        }
        shares[_user].perm_shares += _perm_shares;
        totalShares += _perm_shares;
    }

    function editFees(
        uint256 _deposit,
        uint256 _withdraw,
        uint256 _claims
    ) external onlyOwner {
        depositFee = _deposit;
        withdrawFee = _withdraw;
        claimFee = _claims;
    }
}
