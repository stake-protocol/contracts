// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract TestToken is ERC20Burnable, Ownable {
    constructor(string memory symbol, string memory name) ERC20(symbol, name) {
        _mint(msg.sender, 1_000_000 ether);
    }
}
